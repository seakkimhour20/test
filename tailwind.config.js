/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
    './app/**/*.{js,ts,jsx,tsx}'
  ],
  theme: {
    extend: {
      fontFamily: {
        openSans: ['Open Sans', 'sans-serif']
      },
      backgroundImage: {
        my_bg_image: "url('../public/images/login_background.png')"
      },
      colors: {
        primary: {
          normal: '#0e6431',
          hover: '#0d5a2c',
          active: '#0b5027'
        },
        red: {
          normal: '#ff4444',
          hover: '#e63d3d',
          active: '#cc3636'
        },
        white: {
          normal: '#ffff',
          hover: '#f3f7f5',
          active: '#d8e6de'
        },
        black: {
          normal: '#052311'
        },
        light: {
          normal: '#e7f0ea',
          hover: '#dbe8e0',
          active: '#b4cfbf'
        },
        dark: {
          normal: '#052311',
          hover: '#083c1d',
          active: '#062d16'
        },
        grey: {
          normal: '#808080',
          dark: '#302E2E',
          light: '#898686'
        }
      }
    }
  },
  plugins: []
};
