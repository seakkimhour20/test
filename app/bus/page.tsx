"use client";
import * as React from "react";
import { BusModel } from "../(components)/bus_model";
import { SearchButton } from "../(components)/search_botton";
import { deleteBus } from "../pages/api/bus";
import { getAllBus } from "../pages/api/bus";
import { updateBus } from "../pages/api/bus";
import { useEffect, useState } from "react";
import withAuth from "../../services/withAuth";
import "react-toastify/dist/ReactToastify.css";
import { BusResponse } from "@/types/bus";
import { MdSearchOff } from "react-icons/md";
import type { PaginationProps } from "antd";
const BusManagement = () => {
  const [data, setData] = useState([]);
  const [display, setDisplay] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [rowsPerPage, setRowsPerPage] = useState(25);

  // Calculate pagination
  const indexOfLastRow = currentPage * rowsPerPage;
  const indexOfFirstRow = indexOfLastRow - rowsPerPage;
  const currentRows = display.slice(indexOfFirstRow, indexOfLastRow);
  const [query, setQuery] = useState("");
  const handlePageChange = (pageNumber: any) => {
    setCurrentPage(pageNumber);
  };

  useEffect(() => {
    const fetchData = async () => {
      const responseData = await getAllBus();
      setData(responseData.data);
      setDisplay(responseData.data);
    };
    fetchData();
  }, []);

  const handleUpdateStatus = async (
    bus_id: any,
    model: string,
    plateNumber: string,
    seats: number,
    driverName: string,
    driverNumber: string,
    enable: boolean
  ) => {
    try {
      const res = await updateBus(
        bus_id,
        model,
        plateNumber,
        seats,
        driverName,
        driverNumber,
        enable
      );
      if (res.status === 200) {
        window.location.reload();
      } else {
        console.error("Failed to delete the bus.");
      }
    } catch (error) {
      console.error(error);
    }
  };
  const search = (search: string) => {
    const result = data.filter((bus: any) => {
      return (
        bus.model.toLowerCase().includes(search.toLowerCase()) ||
        bus.plateNumber.toLowerCase().includes(search.toLowerCase()) ||
        bus.driverName.toLowerCase().includes(search.toLowerCase()) ||
        bus.driverContact.toLowerCase().includes(search.toLowerCase())
      );
    });
    setDisplay(result);
  };

  const handleDelete = async (id: any) => {
    try {
      const res = await deleteBus(id);
      if (res.status === 200) {
        window.location.reload();
      } else {
        console.error("Failed to delete the bus.");
      }
    } catch (error) {
      console.error(error);
    }
  };

  const toggleStatus = (status: boolean) => {
    return !status;
  };

  const filterBus = data.filter((bus: any) => {
    return (
      bus.model.toLowerCase().includes(query.toLowerCase()) ||
      bus.plateNumber.toLowerCase().includes(query.toLowerCase()) ||
      bus.driverName.toLowerCase().includes(query.toLowerCase()) ||
      bus.driverContact.toLowerCase().includes(query.toLowerCase())
    );
  });

  const handleNext = () => {
    if (currentPage < Math.ceil(filterBus.length / rowsPerPage)) {
      setCurrentPage(currentPage + 1);
    }
  };
  const handlePrev = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };
  return (
    <div className="flex flex-col w-full p-[50px] min-h-screen bg-light-normal">
      <div className="ml-[13%]">
        <div className="flex flex-row items-center justify-between w-full align-middle">
          <h4 className="font-bold text-[36px]">BUS MANAGEMENT</h4>
          <BusModel
            isEdit={false}
            isIcon={true}
            isTitle={true}
            isDisabled={false}
            className={`text-white-normal h-11 bg-primary-normal hover:bg-primary-hover`}
          />
        </div>
        <div className="w-full mt-16">
          <div className="flex items-center p-1 align-middle border-2 rounded-md bg-white-active w-80 h-11">
            <SearchButton searchFunction={search} />
          </div>
          <div className="m-20 overflow-x-auto">
            <table className="w-full border-0 border-collapse table-auto">
              <thead>
                <tr className="text-sm font-normal text-gray-500">
                  <th className="px-4 py-2 text-left">No</th>
                  <th className="px-4 py-2 text-left">Driver Name</th>
                  <th className="px-4 py-2 text-left">Driver Number</th>
                  <th className="px-4 py-2 text-left">Model</th>
                  <th className="px-4 py-2 text-left">Plate Number</th>
                  <th className="px-4 py-2 text-left">Number of Seats</th>
                  <th className="px-4 py-2 text-left">Active</th>
                  <th className="px-4 py-2 text-left"></th>
                </tr>
              </thead>
              {display.length == 0 || !display ? (
                <div className="flex flex-col items-center justify-center ">
                  <MdSearchOff className="w-24 h-24" />
                  <h1 className="text-2xl">No data</h1>
                </div>
              ) : (
                <tbody className="text-green-950">
                  {/* Loop over the busData array and generate a table row for each object */}
                  {currentRows.map((bus: BusResponse, index: any) => (
                    <tr
                      key={index}
                      className={
                        bus.enable ? "" : "text-gray-400 bg-opacity-60"
                      }
                    >
                      <td className="px-4 py-2">
                        {(currentPage - 1) * rowsPerPage + index + 1}
                      </td>
                      <td className="px-4 py-2">{bus.driverName}</td>
                      <td className="px-4 py-2">{bus.driverContact}</td>
                      <td className="px-4 py-2">{bus.model}</td>
                      <td className="px-4 py-2">{bus.plateNumber}</td>
                      <td className="px-4 py-2">{bus.numOfSeat}</td>
                      <td className="px-4 py-2">
                        <td className="px-4 py-2">
                          {/* {bus.enable ? "Yes" : "No"} */}
                          <button
                            className={`relative inline-block w-10 h-6 rounded-full shadow-inner transition duration-300 ease-in-out ${
                              bus.enable ? "bg-green-800" : "bg-gray-200"
                            }`}
                            onClick={() =>
                              handleUpdateStatus(
                                bus.id,
                                bus.model,
                                bus.plateNumber,
                                bus.numOfSeat,
                                bus.driverName,
                                bus.driverContact,
                                toggleStatus(bus.enable)
                              )
                            }
                          >
                            <span
                              className={`absolute inset-0 w-6 h-6 flex items-center justify-center ${
                                bus.enable ? "ml-4" : ""
                              }`}
                            >
                              <span
                                className={`w-5 h-5 rounded-full shadow-md ${
                                  bus.enable ? "bg-gray-300" : "bg-white"
                                }`}
                              ></span>
                            </span>
                          </button>
                        </td>
                      </td>
                      <td className="px-4 py-2">
                        <button className="px-4 py-2 font-bold text-white rounded">
                          <BusModel
                            bus_model={bus.model}
                            bus_plateNumber={bus.plateNumber}
                            bus_numOfSeat={bus.numOfSeat}
                            bus_driverName={bus.driverName}
                            bus_driverContact={bus.driverContact}
                            bus_id={bus.id}
                            //i don't know why but put this --> it turn to work
                            bus_enable={bus.enable}
                            isEdit={true}
                            isIcon={true}
                            isTitle={false}
                            isDisabled={!bus.enable ? true : false}
                            className={""}
                          />
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              )}
            </table>
          </div>

          <br />
          {display.length == 0 || !display ? null : (
            <div className="flex justify-end mt-4">
              <div className="flex items-center">
                <span className="mr-2">Rows per page:</span>
                <select
                  value={rowsPerPage}
                  onChange={(e) => {
                    if (Number(e.target.value) * currentPage > data.length) {
                      setCurrentPage(1);
                      setRowsPerPage(Number(e.target.value));
                    } else {
                      setRowsPerPage(Number(e.target.value));
                    }
                  }}
                  className="px-2 py-1 border border-gray-300 rounded-md"
                >
                  <option value={5}>5</option>
                  <option value={10}>10</option>
                  <option value={15}>15</option>
                  <option value={25}>25</option>
                  <option value={50}>50</option>
                  <option value={100}>100</option>
                  <option value={200}>200</option>
                </select>
              </div>
              <div className="flex items-center ml-4">
                <span className="mr-2">Page:</span>
                {
                  <button
                    key={">"}
                    onClick={() => handlePrev()}
                    className={`border border-gray-300  rounded-md px-5 py-1 ml-1 focus:outline-none ${
                      currentPage == 1 ? "bg-gray-200" : ""
                    }`}
                  >
                    {"<"}
                  </button>
                }
                {Array.from(
                  { length: Math.ceil(display.length / rowsPerPage) },
                  (_, i) => i + 1
                ).map((page) => {
                  return (
                    <button
                      key={page}
                      onClick={() => handlePageChange(page)}
                      className={`border border-gray-300 rounded-md px-2 py-1 ml-1 focus:outline-none ${
                        currentPage === page ? "bg-gray-300" : ""
                      }`}
                    >
                      {page}
                    </button>
                  );
                })}
                {
                  <button
                    key={">"}
                    onClick={() => handleNext()}
                    className={`border border-gray-300 rounded-md px-5 py-1 ml-1 focus:outline-none ${
                      currentPage == Math.ceil(display.length / rowsPerPage)
                        ? "bg-gray-200"
                        : ""
                    }`}
                  >
                    {">"}
                  </button>
                }
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default withAuth(BusManagement);
