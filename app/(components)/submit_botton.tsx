"use client";
export const SubmitButton: React.FunctionComponent<any> = ({
  title,
  onClick,
  className,
  isDisabled,
}) => {
  return (
    <button
      type="submit"
      disabled={isDisabled === undefined ? false : isDisabled}
      // className={`px-5 mt-2 p-2.5 flex-1 text-white bg-primary-normal rounded-md outline-none text-white-normal  ${className}`}
      className={
        isDisabled === true
          ? `px-5 mt-2 p-2.5 flex-1 text-white rounded-md outline-none text-white-normal bg-gray-500 ${className}`
          : `px-5 mt-2 p-2.5 flex-1 text-white bg-primary-normal rounded-md outline-none text-white-normal hover:bg-primary-hover ${className}`
      }
      onClick={onClick}
    >
      {title}
    </button>
  );
};
