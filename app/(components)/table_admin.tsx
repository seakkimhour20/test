"use client";
import * as React from "react";
import { Table } from "@nextui-org/react";
const TableAdmin
 = () => {
  return (
    <Table
      aria-label="Example table with static content"
      className="bg-green-100 w-96"
      // css={{
      //   height: "auto",
      //   // width: "auto"
      //   // width: "fit-content",
      //   // minWidth: "100%",
      // }}
    >
      <Table.Header>
        <Table.Column>No</Table.Column>
        <Table.Column>Username</Table.Column>
        <Table.Column>Gender</Table.Column>
        <Table.Column>Email</Table.Column>
        <Table.Column>Current Location Status</Table.Column>
        <Table.Column>Active</Table.Column>
        <Table.Column>Ticket</Table.Column>
      </Table.Header>
      <Table.Body>
        <Table.Row key="1">
          <Table.Cell>1</Table.Cell>
          <Table.Cell>Hai Seanghor</Table.Cell>
          <Table.Cell>Male</Table.Cell>
          <Table.Cell>seanghor@kit.edu.kh</Table.Cell>
          <Table.Cell>Not In KRR</Table.Cell>
          <Table.Cell>True</Table.Cell>
          <Table.Cell>34/36</Table.Cell>
        </Table.Row>
        <Table.Row key="2">
          <Table.Cell>2</Table.Cell>
          <Table.Cell>Srun China</Table.Cell>
          <Table.Cell>Male</Table.Cell>
          <Table.Cell>seanghor@kit.edu.kh</Table.Cell>
          <Table.Cell>Not In KRR</Table.Cell>
          <Table.Cell>True</Table.Cell>
          <Table.Cell>34/36</Table.Cell>
        </Table.Row>
        <Table.Row key="3">
          <Table.Cell>3</Table.Cell>
          <Table.Cell>Seak Kimhour</Table.Cell>
          <Table.Cell>Male</Table.Cell>
          <Table.Cell>seanghor@kit.edu.kh</Table.Cell>
          <Table.Cell>Not In KRR</Table.Cell>
          <Table.Cell>True</Table.Cell>
          <Table.Cell>34/36</Table.Cell>
        </Table.Row>
        <Table.Row key="4">
          <Table.Cell>4</Table.Cell>
          <Table.Cell>Huot Monirith</Table.Cell>
          <Table.Cell>Male</Table.Cell>
          <Table.Cell>seanghor@kit.edu.kh</Table.Cell>
          <Table.Cell>Not In KRR</Table.Cell>
          <Table.Cell>True</Table.Cell>
          <Table.Cell>34/36</Table.Cell>
        </Table.Row>
      </Table.Body>
      <Table.Pagination
        shadow
        noMargin
        align="center"
        rowsPerPage={3}

      />
    </Table>
  );
};

export default TableAdmin
;
