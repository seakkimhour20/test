'use client';
import React, { useState } from 'react';
import { OutLineButton } from './out_botton';
import { IconButton } from './Icon_botton';
import { delete_sublocation } from '../pages/api/location';
import Button from './Button';

const DeleteModel: React.FunctionComponent<any> = ({ item, id }) => {
  const [showModal, setShowModal] = useState(false);
  console.log('id');

  return (
    <>
      <OutLineButton
        className={'ml-2'}
        icon='/delete.svg'
        onClick={() => setShowModal(true)}
      />
      {showModal ? (
        <>
          <div className='fixed inset-0 z-10 overflow-y-auto backdrop-blur-sm'>
            <div
              className='fixed inset-0 w-full h-full bg-black'
              onClick={() => setShowModal(false)}></div>
            <div className='flex items-center min-h-screen px-4 py-8'>
              <div className='relative w-full max-w-lg p-4 mx-auto bg-black rounded-md shadow-2xl bg-light-hover'>
                <div className='mt-3'>
                  <div className='mt-2 text-center sm:ml-4 sm:text-center sm:mr-4'>
                    <h4 className='text-[20px] font-bold text-gray-800'>
                      Are you sure that you want to delete {item} ?
                    </h4>
                    <div className='flex pt-10'>
                      <div className='flex-1 pr-10 '>
                        <Button
                          title='Yes'
                          onClick={() => delete_sublocation(id)}
                          color='red'
                          isPrimary={true}
                        />
                      </div>
                      <div className='flex-1'>
                        <Button
                          title='No'
                          onClick={() => setShowModal(false)}
                          color='red'
                          isPrimary={false}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : null}
    </>
  );
};

export default DeleteModel;
