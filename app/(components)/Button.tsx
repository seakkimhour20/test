import React from 'react';

interface IconProps {
  isPrimary: boolean;
  color?: string;
  onClick: () => void;
  title: string;
}

const colors: { [key: string]: string } = {
  red: 'bg-red-normal hover:bg-red-hover text-white',
  primary: 'bg-primary-normal hover:bg-primary-hover text-white'
};

export default function Button(props: IconProps) {
  const { isPrimary, color, onClick, title } = props;
  let colorClass = colors[color != undefined ? color : 'primary'];
  return (
    <div>
      {isPrimary ? (
        <div
          className={`w-full ${colorClass} cursor-pointer font-bold sm:text-lg md:text-lg lg:text-xl text-white-normal text-center rounded-xl h-16 justify-center items-center flex p-5 mr-5`}
          onClick={onClick}>
          {title}
        </div>
      ) : (
        <div
          className='cursor-pointer font-bold sm:text-lg md:text-xl lg:text-2xl text-grey-normal text-center rounded-xl h-16 justify-center items-center flex border-grey-normal border-2 p-5'
          onClick={onClick}>
          {title}
        </div>
      )}
    </div>
  );
}
