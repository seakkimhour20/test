"use client";
import * as React from "react";
import { Table } from "@nextui-org/react";
const TableScheduleDetail = () => {
    return (     <Table
        aria-label="Example table with static content"
        className="bg-green-100 w-96"
        // css={{
        //   height: "auto",
        //   // width: "auto"
        //   // width: "fit-content",
        //   // minWidth: "100%",
        // }}
      >
        <Table.Header>
          <Table.Column>No</Table.Column>
          <Table.Column>Username</Table.Column>
            <Table.Column>From</Table.Column>
            <Table.Column>To</Table.Column>
          <Table.Column>Departure Date</Table.Column>
          <Table.Column>Departure Time</Table.Column>
          <Table.Column>Ticket Status</Table.Column>
        </Table.Header>
        <Table.Body>
          <Table.Row key="1">
            <Table.Cell>1</Table.Cell>
            <Table.Cell>Tony Reichert</Table.Cell>
            <Table.Cell>Active</Table.Cell>
            <Table.Cell>Tony Reichert</Table.Cell>
            <Table.Cell>CEO</Table.Cell>
            <Table.Cell>Tony Reichert</Table.Cell>
            <Table.Cell>Active</Table.Cell>
          </Table.Row>
          <Table.Row key="2">
            <Table.Cell>2</Table.Cell>
            <Table.Cell>Technical Lead</Table.Cell>
            <Table.Cell>Paused</Table.Cell>
            <Table.Cell>Tony Reichert</Table.Cell>
            <Table.Cell>CEO</Table.Cell>
            <Table.Cell>Tony Reichert</Table.Cell>
            <Table.Cell>Active</Table.Cell>
          </Table.Row>
          <Table.Row key="3">
            <Table.Cell>3</Table.Cell>
            <Table.Cell>Jane Fisher</Table.Cell>
            <Table.Cell>Active</Table.Cell>
            <Table.Cell>Tony Reichert</Table.Cell>
            <Table.Cell>CEO</Table.Cell>
            <Table.Cell>Tony Reichert</Table.Cell>
            <Table.Cell>Active</Table.Cell>
          </Table.Row>
          <Table.Row key="4">
            <Table.Cell>4</Table.Cell>
            <Table.Cell>William Howard</Table.Cell>
            <Table.Cell>Vacation</Table.Cell>
            <Table.Cell>Tony Reichert</Table.Cell>
            <Table.Cell>CEO</Table.Cell>
            <Table.Cell>Tony Reichert</Table.Cell>
            <Table.Cell>Active</Table.Cell>
          </Table.Row>
        </Table.Body>
        <Table.Pagination
          shadow
          noMargin
          align="center"
          rowsPerPage={3}
    
        />
      </Table> );
}
 
export default TableScheduleDetail;