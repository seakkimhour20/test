"use client";
import Link from "next/link";
import { useRouter, usePathname } from "next/navigation";
import { logOut } from "../pages/api/auth";
import Image from "next/image";
import Logo from "../../public/images/logo.png";

export default function Sidebar() {
  const router = useRouter();
  const currentRoute = usePathname();
  if (currentRoute === "/login") {
    return null;
  }
  const userLogOut = () => {
    logOut();
    router.push("/");
  };
  return (
    <div>
      <button
        data-drawer-target="default-sidebar"
        data-drawer-toggle="default-sidebar"
        aria-controls="default-sidebar"
        type="button"
        className="inline-flex items-center p-2 mt-2 ml-3 text-sm "
      >
        <Image src="/nav_burger.svg" alt={""} width={32} height={32} />

      </button>

      <aside
        id="default-sidebar"
        className="fixed top-0 left-0 z-40 w-64 h-screen transition-transform -translate-x-full sm:translate-x-0"
        aria-label="Sidebar"
      >
        <div className="flex flex-col justify-between h-full py-4 overflow-y-auto bg-green-800">
          <div className="flex flex-col items-center justify-center text-white-normal">
            SHUTTLE BUS <br /> <span className="">SYSTEM</span>
            <Link href="/schedule">
              <Image src={Logo} alt="logo" width={100} />
            </Link>
          </div>
          <ul className="space-y-2 font-medium">
            <li
              className={`${
                currentRoute === "/schedule" ||
                currentRoute.slice(0, 10) == "/schedule/"
                  ? "active"
                  : ""
              }`}
            >
              <Link
                href="/schedule"
                className="flex items-center p-3 text-white-normal dark:text-white-normal hover:bg-green-900 dark:hover:bg-green-900"
              >
                <span className="ml-3">Schedules</span>
              </Link>
            </li>
            <li className={`${currentRoute === "/bus" ? "active" : ""}`}>
              <Link
                href="/bus"
                className="flex items-center p-3 text-white-normal dark:text-white-normal hover:bg-green-900 dark:hover:bg-green-900"
              >
                <span className="ml-3">Bus Management</span>
              </Link>
            </li>
            <li className={`${currentRoute === "/location" ? "active" : ""}`}>
              <Link
                href="/location"
                className="flex items-center p-3 text-white-normal dark:text-white-normal hover:bg-green-900 dark:hover:bg-green-900"
              >
                <span className="ml-3">Location Management</span>
              </Link>
            </li>
            <li
              className={`${
                currentRoute === "/user" || currentRoute === "/dashboard"
                  ? "active"
                  : ""
              }`}
            >
              <Link
                href="/user"
                className="flex items-center p-3 text-white-normal dark:text-white-normal hover:bg-green-900 dark:hover:bg-green-900"
              >
                <span className="ml-3">User Management</span>
              </Link>
            </li>
            <li className={`${currentRoute === "/ticket" ? "active" : ""}`}>
              <Link
                href="/ticket"
                className="flex items-center p-3 text-white-normal dark:text-white-normal hover:bg-green-900 dark:hover:bg-green-900"
              >
                <span className="ml-3">Ticket Management</span>
              </Link>
            </li>
          </ul>
          <ul>
            <li>
              <a
                href="#"
                className="flex items-center p-2 rounded-lg text-white-normal dark:text-white-normal hover:bg-green-900 dark:hover:bg-green-900"
              >
                <Image src="/side_bar_admin.svg" alt={""} width={24} height={24} />
                <span className="ml-3">Admin</span>
              </a>
            </li>
            <li onClick={() => userLogOut()}>
              <a
                href="#"
                className="flex items-center p-2 rounded-lg text-white-normal dark:text-white-normal hover:bg-green-900 dark:hover:bg-green-900"
              >
                <Image src="/logout.svg" alt={""} width={24} height={24} />

                <span className="ml-3">Logout</span>
              </a>
            </li>
          </ul>
        </div>
      </aside>
    </div>
  );
}
