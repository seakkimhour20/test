'use client';
import Image from 'next/image';

interface Props {
  items: {
    photo: string;
    name: string;
    email: string;
    department: string;
    batchNum: number;
    gender: string;
    status: boolean;
    remainTicket: number;
    role: string;
    turnOn: boolean
  };
}
const convertName = (location: string) => {
  return location
    // .split(" ")
    // .map((word) => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase())
    // .join(" ");
};
const UserProfileCard: React.FC<Props> = ({ items }) => {
  return (
    <div>
      {
        items.turnOn === true ? (
          <div className='fixed inset-0 z-10 overflow-y-auto backdrop-blur-sm'>
            <div className='flex items-center min-h-screen px-4 py-8'>
              <div className='relative w-full max-w-lg  mx-auto bg-black rounded-md shadow-2xl '>
                <div className="backdrop-blur-sm bg-light-active justify-center grid grid-rows-3 grid-flow-col gap-4 items-center min-w-screen px-6 pb-6 pt-2  bg-black rounded-md shadow-2xl">
                  <div className="row-span-3 flex items-center mx-10">
                    <div className="flex flex-col items-center">
                      <div className="w-16 h-16 rounded-full overflow-hidden">
                        <Image
                          src={!items.photo ? '/default.svg' : items.photo}
                          alt=""
                          width={64}
                          height={64}
                        />
                      </div>
                      <div className="text-center">
                        <h2 className="text-base font-semibold text-gray-800">{convertName(items.name)}</h2>
                        <p className={`${items.role === "ADMIN" ? "text-red-600" : items.role === "STAFF" ? "text-green-800" : "text-blue-800"} font-mono`}>{items.role}</p>
                      </div>
                    </div>
                  </div>
                  <div className="col-span-2 pt-5">
                    <div className="font-bold text-3xl text-gray-700">
                      <h1>User Information</h1>
                    </div>
                  </div>
                  <div className="row-span-2 col-span-2">
                    <div className="mt-4">
                      <p className="text-gray-700">
                        <span className="font-semibold text-gray-900">Email:</span> {items.email}
                      </p>
                      <p className="text-gray-700">
                        <span className="font-semibold text-gray-900">Gender:</span> {convertName(items.gender)}
                      </p>
                      {
                        items.role !== "STUDENT"
                          ? (null)
                          : (
                            <p className="text-gray-700">
                              <span className="font-semibold text-gray-900">Department:</span> {items.department ===
                                "SOFTWAREENGINEERING"
                                ? "Software Engineering"
                                : items.department ===
                                  "TOURISMANDMANAGEMENT"
                                  ? "Tourisum and Management"
                                  : items.department ===
                                    "ARCHITECTURE"
                                    ? "Architecture"
                                    : "N/A"}
                            </p>
                          )
                      }
                      {
                        items.role !== "STUDENT"
                          ? (null)
                          : (
                            <p className="text-gray-700">
                              <span className="font-semibold text-gray-900">Batch:</span> {!items.batchNum ? 0 : items.batchNum}
                            </p>
                          )
                      }
                      <p className="text-gray-700">
                        <span className="font-semibold text-gray-900">Status:</span>
                        {
                          items.status
                            ? "In Kirirom"
                            : "Out of Kirirom"
                        }
                      </p>
                      {
                        items.role !== "ADMIN"
                          ? (
                            <p className="text-gray-700">
                              <span className="font-semibold text-gray-900">Remaining Tickets:</span> {items.remainTicket}
                            </p>
                          )
                          : (
                            null
                          )
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>) : (null)
      }
    </div>
  );
};

export default UserProfileCard;