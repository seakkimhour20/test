"use client";
import * as React from "react";
import { FC } from "react";
import Image from "next/image";
import { useState } from "react";
import { SubmitButton } from "./submit_botton";
import { IconButton } from "./Icon_botton";
import { swapBooking } from "../pages/api/booking";
import { SwapBookingDto } from "@/types/booking";
import ShowTerminal from "./show_terminal";

interface pageProps {
  bookingUserOptions: {
    id: string;
    value: string;
  }[];
  waitingUserOptions: {
    id: string;
    value: string;
  }[];
  scheduleId: string;
}

export const SwapTicketModel: FC<pageProps> = ({
  bookingUserOptions,
  waitingUserOptions,
  scheduleId,
}) => {
  const [showModal, setShowModal] = useState(false);
  const [userBooking, setUserBooking] = useState("");
  const [userWaiting, setUserWaiting] = useState("");

  // for terminal
  const [success, setSuccess] = useState(false);
  const [showTerminal, setShowTerminal] = useState(false);
  const [description, setDiscription] = useState("");
  const [content, setContent] = useState("");

  const handleSelectBookingUser = async (event: any) => {
    
    setUserBooking(event.target.value);
  };
  const handleSelectWaitingUser = async (event: any) => {

    setUserWaiting(event.target.value);
  };
  const changeTheme = () => {
    return setShowModal(!showModal);
  };

  async function swap() {
    const swapData = {
      fromBookedId: userBooking,
      withWaitingId: userWaiting,
    } as SwapBookingDto;
    const res = await swapBooking(scheduleId, swapData);
    if (res?.status == 200) {
      setSuccess(true);
      setContent("Swapped Success");
      setDiscription("User Has been Swapped Successfull");
      setShowModal(false);
      setShowTerminal(true);
      setTimeout(() => {
        // window.location.reload();
        setShowTerminal(false);
      }, 2000);
    } else {

      setSuccess(false);
      setShowModal(false);
      setContent("Error 500");
      setDiscription("Something Went Wrong");
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
    }
  }
  return (
    <>
      <div>
        {/* <IconButton onClick={() => changeTheme()} /> */}
        <div
          className="flex items-center justify-center p-2 px-5 align-middle rounded-md cursor-pointer w-50 text-white-normal h-11 bg-primary-normal hover:bg-primary-hover"
          onClick={() => changeTheme()}
        >
          <Image src="/swapWhite.svg" alt="" width={20} height={20} />
          <h4 className="text-[16px] mx-3">Swap Ticket</h4>
        </div>
      </div>
      {showModal ? (
        <>
          <div className="fixed inset-0 z-10 overflow-y-auto backdrop-blur-sm">
            <div
              className="fixed inset-0 w-full h-full bg-black"
              onClick={() => setShowModal(false)}
            ></div>
            <div className="flex items-center min-h-screen px-4 py-8">
              <div className="relative w-full max-w-lg p-4 mx-auto bg-black rounded-md shadow-2xl bg-light-hover">
                <div className="mt-3">
                  <div className="mt-2 text-center sm:ml-4 sm:text-center sm:mr-4">
                    <h4 className="text-[24px] font-bold text-gray-800">
                      Swap Ticket
                    </h4>

                    <div className="mt-5">
                      <p className="text-left text-[16] font-normal">
                        Username as Booking
                      </p>
                      <select
                        value={userBooking}
                        onChange={handleSelectBookingUser}
                        className="items-start w-full p-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                      >
                        {bookingUserOptions.map((option: any) => (
                          <option key={option.value} value={option.id}>
                            {option.value}
                          </option>
                        ))}
                      </select>
                    </div>
                    <div className="flex justify-center my-3">
                      {" "}
                      <Image src="/swap.svg" alt="" width={20} height={20} />
                    </div>
                    <div className="">
                      <p className="text-left text-[16] font-normal">
                        Username as Waiting
                      </p>
                      <select
                        value={userWaiting}
                        onChange={handleSelectWaitingUser}
                        className="items-start w-full p-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                      >
                        {waitingUserOptions.map((option: any) => (
                          <option key={option.value} value={option.id}>
                            {option.value}
                          </option>
                        ))}
                      </select>
                    </div>
                    <div className="flex justify-between">
                      <div className="flex-1 pr-5">
                        <SubmitButton
                          onClick={() => {
                            setShowModal(false);
                          }}
                          title={"Cancel"}
                          className="w-full mt-2  bg-red-active hover:bg-red-normal px-5  p-2.5"
                        />
                      </div>
                      <div className="flex-1">
                        <button
                          disabled={
                            userWaiting == "" || userBooking == ""
                              ? true
                              : false
                          }
                          type="submit"
                          className={
                            userWaiting !== "" && userBooking !== ""
                              ? "px-5 mt-2 p-2.5 flex-1 text-white bg-primary-normal rounded-md outline-none text-white-normal w-full hover:bg-primary-hover"
                              : "px-5 mt-2 p-2.5 flex-1 text-white bg-gray-500 rounded-md outline-none text-white-normal w-full "
                          }
                          onClick={() => {
                            swap();
                          }}
                        >
                          Swap
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : showTerminal ? (
        <ShowTerminal
          content={content}
          description={description}
          show={true}
          success={success}
          onClick={() => {
            setShowTerminal(false);
          }}
        />
      ) : null}
    </>
  );
};

export default SwapTicketModel;
