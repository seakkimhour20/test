import React, { useState } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { forgetPassword } from "../pages/api/auth";
import { Button, Input, Modal } from "antd";

import { OutLineButton } from "./out_botton";
import { SubmitButton } from "./submit_botton";
import { log } from "console";
const modalStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    padding: "30px",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    maxWidth: "90%", // Width for mobile
    width: "500px", // Width for laptop
    backgroundColor: "#dbe8e0",
    border: "none",
  },
};
function ForgetPasswordModal() {
  const [email, setEmail] = useState("");
  const userNameHanddle = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value);
  };
  const showToastMessage = (message: string, type: keyof typeof toast) => {
    (toast as any)[type](message, {
      position: toast.POSITION.TOP_RIGHT,
      autoClose: 1000,
    });
  };
  const forgetPasswordRequest = async () => {
    setConfirmLoading(true);
    const res = await forgetPassword(email).then(async (res) => {
      console.log("res", res.status);

      if (res.status == 200) {
        setConfirmLoading(false);
        showToastMessage(
          'Reset password has been sent to your Gmail. Please check your inbox or spam folder for instructions. If you encounter any issues, please feel free to contact our team."',
          "success"
        );
        setOpen(false);
      } else if (res.status == 400) {
        setConfirmLoading(false);
        showToastMessage(
          "The request was malformed or missing required parameters.",
          "error"
        );
      } else if (res.status == 401) {
        showToastMessage(
          "The user is not authenticated or does not have the necessary permissions to request a password reset.",
          "error"
        );
        setConfirmLoading(false);
      } else if (res.status == 404) {
        showToastMessage(
          "The user account associated with the provided email address was not found.",
          "error"
        );
        setConfirmLoading(false);
      } else if (res.status == 429) {
        showToastMessage(
          "The user has exceeded the rate limit for requesting password resets. Retry after a certain period of time.",
          "error"
        );
        setConfirmLoading(false);
      } else if (res.status == 500) {
        showToastMessage(
          "An unexpected error occurred on the server while processing the request.",
          "error"
        );
        setConfirmLoading(false);
      } else if (res.status == 409) {
        const errorMessage = await res.json();
        showToastMessage(errorMessage.error, "error");
        setConfirmLoading(false);
      } else {
        showToastMessage(
          "There is something wrong, please contact our team",
          "error"
        );
        setConfirmLoading(false);
      }
    });
  };

  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);

  const showModal = () => {
    setEmail("");
    setOpen(true);
  };

  const handleCancel = () => {
    console.log("Clicked cancel button");
    setOpen(false);
  };

  return (
    <>
      <OutLineButton onClick={showModal} title={"Forget Password"} />
      <Modal
        title="Forget Password"
        visible={open}
        onOk={forgetPasswordRequest}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        centered
        closable
        okText="Send"
        bodyStyle={{
          height: "80px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
        okButtonProps={{
          className: "bg-primary-normal text-white hover:bg-primary-normal ",
          type: "default",
          style: { width: "49%", color: "white", height: "40px" },
        }}
        cancelButtonProps={{
          className:
            "bg-red-normal text-white hover:text-white hover:bg-red-normal",
          type: "default",
          style: { width: "49%", color: "white", height: "40px" },
        }}
      >
        <Input
          className="p-2 mt-3 bg-gray-100"
          value={email}
          placeholder="Email"
          onChange={userNameHanddle}
        />
      </Modal>
    </>
  );
}
export default ForgetPasswordModal;
