"use client";
import { useEffect, useState } from "react";
import { SubmitButton } from "./submit_botton";
import { addNewUser, deleteUser, updateUser } from "../pages/api/user";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { IconButton } from "./Icon_botton";
import ShowTerminal from "./show_terminal";
import { getAllDeparture } from "../pages/api/departure";
import { getAllBatch } from "../pages/api/batch";
import { BatchResponse } from "@/types/batch";
import { EnumType } from "typescript";
import Image from "next/image";
import { UserError } from "../../types/user_error";


export const UserModel: React.FunctionComponent<any> = ({
  isEdit,
  isDelate,
  iconOnly,
  user_id,
  user_role,
  user_email,
  user_username,
  user_department,
  user_phone,
  user_batchNum,
  user_inKRR,
  user_gender,
  user_enable,
  isIcon,
  isTitle,
  isDisabled,
  className,
}) => {
  const RoleOptions = [
    { value: "", label: "Select a Role" },
    { value: "STUDENT", label: "Student" },
    { value: "ADMIN", label: "Admin" },
    { value: "STAFF", label: "Staff" },
  ];
  const RoleOptionsForEdit = [
    { value: "", label: "Select a Role" },
    { value: "STUDENT", label: "Student" },
    { value: "STAFF", label: "Staff" },
  ];

  const GenderOptions = [
    { value: "", label: "Select Gender" },
    { value: "MALE", label: "Male" },
    { value: "FEMALE", label: "Female" },
  ];
  const DepartmentOptions = [{ value: "", label: "Select Department" }];

  const BatchSEOptions = [{ value: "", label: "Select Batch Number" }];

  const BatchTMOptions = [{ value: "", label: "Select Batch Number" }];

  const BatchARCOptions = [{ value: "", label: "Select Batch Number" }];
  const [includePassword, setIncludePassword] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [userId, setUserId] = useState(user_id == "" ? "" : user_id);
  const [userRole, setUserRole] = useState(user_role == "" ? "" : user_role);
  const [userEmail, setUserEmail] = useState(
    user_email == "" ? "" : user_email
  );
  const [userUsername, setUserUsername] = useState(
    user_username == "" ? "" : user_username
  );
  const [userPassword, setUserPassword] = useState(
    includePassword && isEdit ? null : ""
  );
  const [userConfirmPassword, setUserConfirmPassword] = useState("");
  const [userDepartment, setUserDepartment] = useState(
    user_department == "" ? "" : user_department
  );
  const [userPhone, setUserPhone] = useState(
    user_phone == "" ? "" : user_phone
  );
  const [userBatchNum, setUserBatchNum] = useState(
    user_batchNum == "" ? "" : user_batchNum
  );
  const [userInKRR, setUserInKRR] = useState(
    user_inKRR == "" ? "" : user_inKRR
  );
  const [userGenders, setUserGenders] = useState(user_gender);
  const [userEnable, setUserEnable] = useState(
    user_enable == "" ? "" : user_enable
  );

  // for terminal
  const [success, setSuccess] = useState(false);
  const [showTerminal, setShowTerminal] = useState(false);
  const [description, setDiscription] = useState("");
  const [content, setContent] = useState("");
  const [data, setData] = useState<BatchResponse[]>();

  const [type, setType] = useState("password");
  const [icon, setIcon] = useState("/hide_password.svg");

  const [type2, setType2] = useState("password");
  const [icon2, setIcon2] = useState("/hide_password.svg");
  const [UserError, setUserError] = useState<UserError>({
    username: { isError: false, message: "" },
    email: { isError: false, message: "" },
    password: { isError: false, message: "" },
    confirmPassword: { isError: false, message: "" },
  });

  // useEffect
  useEffect(() => {
    const fetchData = async () => {
      const responseData = await getAllBatch();
      setData(responseData.data);
    };
    fetchData();
    if (userRole === "STAFF") {
      setUserDepartment(null);
      setUserBatchNum("");
    } else if (userRole === "STUDENT") {
      setUserDepartment(user_department == "" ? "" : user_department);
      setUserBatchNum(user_batchNum == "" ? "" : user_batchNum);
    }
  }, [userRole, user_department, user_batchNum]);

  // habdleSet vissible
  const handleToggle = () => {
    if (type === "password") {
      setIcon("/un_hide_password.svg");
      setType("text");
    } else {
      setIcon("/hide_password.svg");
      setType("password");
    }
  };

  const handleToggle2 = () => {
    if (type2 === "password") {
      setIcon2("/un_hide_password.svg");
      setType2("text");
    } else {
      setIcon2("/hide_password.svg");
      setType2("password");
    }
  };

  // get distrinct department
  data?.forEach((item) => {
    // console.log(item.department);

    const departmentName = item.department;
    const existingDepartment = DepartmentOptions.find(
      (dept) => dept.value === departmentName
    );
    if (!existingDepartment) {
      DepartmentOptions.push({
        value: departmentName,
        label:
          departmentName === "SOFTWAREENGINEERING"
            ? "Software Engineering"
            : departmentName === "TOURISMANDMANAGEMENT"
              ? "Tourism and Management"
              : departmentName === "ARCHITECTURE"
                ? "Architecture"
                : "N/A",
      } as any);
    }

    if (departmentName === "SOFTWAREENGINEERING") {
      const batchNum = item.batchNum;
      const existingBatch = BatchSEOptions.find(
        (batch: any) => batch.value === batchNum
      );
      if (!existingBatch) {
        BatchSEOptions.push({
          value: batchNum,
          label: `SE-B${batchNum}`,
        } as any);
      }
    }
    else if (departmentName === "TOURISMANDMANAGEMENT") {
      const batchNum = item.batchNum;
      const existingBatch = BatchTMOptions.find(
        (batch: any) => batch.value === batchNum
      );
      if (!existingBatch) {
        BatchTMOptions.push({
          value: batchNum,
          label: `TM-B${batchNum}`,
        } as any);
      }
    }
    else if (departmentName === "ARCHITECTURE") {
      const batchNum = item.batchNum;
      const existingBatch = BatchARCOptions.find(
        (batch: any) => batch.value === batchNum
      );
      if (!existingBatch) {
        BatchARCOptions.push({
          value: batchNum,
          label: `ARC-B${batchNum}`,
        } as any);
      }
    }
  });
  console.log(userId);
  // sort batchNum
  BatchSEOptions.sort((a: any, b: any) => a.batchNum - b.batchNum);
  BatchTMOptions.sort((a: any, b: any) => a.batchNum - b.batchNum);
  BatchARCOptions.sort((a: any, b: any) => a.batchNum - b.batchNum);

  const handleSelectRoleChange = (event: any) => {
    console.log(event.target.value);

    setUserRole(event.target.value);
  };

  // const handle
  const handleSelectGenderChange = (event: any) => {
    setUserGenders(event.target.value);
    console.log(userGenders);
    // console.log(event.target.value);
  };


  const handleSelectBatchNumChange = (event: any) => {
    setUserBatchNum(event.target.value);
  };

  const handleSelectDepartmentChange = (event: any) => {
    setUserDepartment(event.target.value);
  };

  const title = isEdit ? "Save Change" : "Create";
  const button_title = isEdit ? "Edit" : "Create New User";
  const changeTheme = () => {
    setUserRole(isEdit ? userRole : ""), setUserEmail(isEdit ? userEmail : "");
    setUserUsername(isEdit ? userUsername : "");
    setUserPassword(userPassword);
    setUserConfirmPassword(userConfirmPassword);
    setUserDepartment(isEdit ? userDepartment : "");
    setUserPhone(isEdit ? userPhone : "");
    setUserBatchNum(isEdit ? userBatchNum : "");
    setUserGenders(isEdit ? userGenders : "");
    setUserError({
      username: { isError: false, message: "" },
      email: { isError: false, message: "" },
      password: { isError: false, message: "" },
      confirmPassword: { isError: false, message: "" },
    });
    return setShowModal(!showModal);
  };

  const matchPassword = (password: string, confirmPassword: string) => {
    return password === confirmPassword ? true : false;
  };

  async function handleCreate() {
    const match = userPassword
      ? matchPassword(userPassword, userConfirmPassword)
      : false;
    if (match == false) {
      setSuccess(false);
      setShowModal(false);
      setContent("Passwords do NOT match");
      setDiscription("Your confirm password must be match with your password");
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
      return;
    }

    const newData = {
      userEmail,
      userUsername,
      userPassword,
      userPhone,
      userRole,
      userInKRR,
      userGenders,
      userDepartment,
      userBatchNum,
    };
    const res: any = await addNewUser({
      email: newData.userEmail,
      username: newData.userUsername,
      password: newData.userPassword,
      phone: newData.userPhone || null,
      role: newData.userRole,
      inKRR: newData.userInKRR,
      gender: newData.userGenders || null,
      department: newData.userDepartment,
      batchNum: Number(newData.userBatchNum),
      enable: true,
    });
    if (res?.status == 200 || res?.status == 201) {
      const data = await res.json();

      setSuccess(true);
      setContent("Created");
      setDiscription(data.message);
      setShowModal(false);
      setShowTerminal(true);
      setTimeout(() => {
        setShowTerminal(false);
        window.location.reload();
      }, 2000);

      // Set it to null value for every field after create
      setUserRole(""), setUserEmail("");
      setUserUsername("");
      setUserPassword("");
      setUserConfirmPassword("");
      setUserDepartment("");
      setUserPhone("");
      setUserBatchNum("");
      setUserGenders("");
      return;
    } else if (res?.status === 400) {
      const data = await res.json();

      setSuccess(false);
      setShowModal(false);
      setContent("Required");
      setDiscription(data.message);
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
      return;
    } else if (res?.status === 404) {
      const data = await res.json();

      setSuccess(false);
      setShowModal(false);
      setContent("Required");
      setDiscription(data.error);
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
      return;
    } else if (res?.status === 409) {
      const data = await res.json();

      setSuccess(false);
      setShowModal(false);
      setContent("Fail to Create");
      setDiscription(data.error);
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
      return;
    } else {
      setSuccess(false);
      setShowModal(false);
      setContent("Error");
      setDiscription("Something Went Wrong");
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
      return;
    }
  }

  async function handleUpdateUser() {

    if (includePassword && userPassword) {
      const match = matchPassword(userPassword, userConfirmPassword);
      if (match == false) {
        setSuccess(false);
        setShowModal(false);
        setContent("Passwords do NOT match");
        setDiscription(
          "Your confirm password must be match with your password"
        );
        setTimeout(() => {
          setShowTerminal(true);
        }, 400);
        return;
      }
    }


    const res = await updateUser(
      userId,
      userRole,
      userEmail,
      userUsername,
      userPassword,
      userDepartment,
      userPhone,
      userBatchNum,
      userInKRR,
      userGenders,
      userEnable
    );


    if (res?.status == 200 || res?.status == 201) {
      const data = await res.json();
      setSuccess(true);
      setContent("Updated");
      setDiscription(data.message);
      setShowModal(false);
      setShowTerminal(true);
      setTimeout(() => {
        setShowTerminal(false);
        window.location.reload();
      }, 2000);

      // Set it to null value for every field after create
      setUserRole(isEdit ? userRole : ""),
        setUserEmail(isEdit ? userEmail : "");
      setUserUsername(isEdit ? userUsername : "");
      setUserConfirmPassword(isEdit ? userConfirmPassword : "");
      setUserPassword(isEdit ? userPassword : "");
      setUserDepartment(isEdit ? userDepartment : "");
      setUserPhone(isEdit ? userPhone : "");
      setUserBatchNum(isEdit ? userBatchNum : "");
      setUserGenders(isEdit ? userGenders : "");
      return;
    } else if (res?.status === 400) {
      const data = await res.json();
      setSuccess(false);
      setShowModal(false);
      setContent("Required");
      setDiscription(data.message);
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
      return;
    } else if (res?.status === 404) {
      const data = await res.json();
      setSuccess(false);
      setShowModal(false);
      setContent("Required");
      setDiscription(data.error);
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
      return;
    } else if (res?.status === 409) {
      const data = await res.json();
      setSuccess(false);
      setShowModal(false);
      setContent("Fail to Update");
      setDiscription(data.error);
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
      return;
    } else {
      setSuccess(false);
      setShowModal(false);
      setContent("Error");
      setDiscription("Something Went Wrong");
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
      return;
    }
  }

  const handleValidateDisableButton = async () => {
    const validate =
      (!isEdit &&
        (!userEmail ||
          !userUsername ||
          !userGenders ||
          !userRole ||
          !userPassword ||
          !userConfirmPassword)) ||
        (userRole === "STUDENT" && (!userDepartment || !userBatchNum))
        ? true
        : // update && include password
        (isEdit &&
          includePassword &&
          (!userEmail ||
            !userUsername ||
            !userGenders ||
            !userRole ||
            !userPassword ||
            !userConfirmPassword)) ||
          (userRole === "STUDENT" && (!userDepartment || !userBatchNum))
          ? true
          : // update && not include password
          (isEdit &&
            !includePassword &&
            (!userEmail || !userUsername || !userGenders || !userRole)) ||
            (userRole === "STUDENT" && (!userDepartment || !userBatchNum))
            ? true
            : false;

    return validate;
  };

  return (
    <>
      <IconButton
        title={button_title}
        onClick={() => changeTheme()}
        img={isEdit ? "/edit.svg" : isDelate ? "/delete.svg" : "/add.svg"}
        isIconOnly={isIcon}
        isTitleOnly={isTitle}
        className={`${className}`}
        isDisabled={false}
        w={22}
        h={22}
      />
      {showModal ? (
        <>
          <div className="fixed inset-0 z-10 overflow-y-auto backdrop-blur-sm">
            <div
              className="fixed inset-0 w-full h-full bg-black"
              onClick={() => setShowModal(false)}
            ></div>

            <div className="flex items-center min-h-screen px-4 py-8">
              <div className="relative w-full max-w-lg p-4 mx-auto bg-black rounded-md shadow-2xl bg-light-hover">
                <div className="mt-3">
                  <div className="mt-2 text-center sm:ml-4 sm:text-center sm:mr-4">
                    <h4 className="text-[24px] font-bold text-gray-800">
                      {button_title}
                    </h4>
                    <div className="flex flex-row justify-between mt-3">
                      <div>
                        <p className="text-left text-[16] font-normal">
                          Username
                        </p>

                        <input
                          defaultValue={isEdit ? user_username : userUsername}
                          onSubmit={(e) => {
                            e.preventDefault();
                          }}
                          onKeyDown={(event) => {
                            const isUppercaseLetter =
                              event.key.toUpperCase() !==
                              event.key.toLowerCase();
                            const specialCharPattern =
                              /[!@#$%^&*()/-_+,.?"':{}|<>-]/;
                            if (
                              event.key === "Delete" ||
                              event.key === "Backspace" ||
                              event.key === "ArrowLeft" ||
                              event.key === "ArrowRight" ||
                              isUppercaseLetter
                            ) {
                              return;
                            }
                            if (specialCharPattern.test(event.key)) {
                              event.preventDefault();
                            }
                          }}
                          onChange={(e) => {
                            const value = e.target.value.trim();
                            if (value.length === 0) {
                              const newError = { ...UserError };
                              newError.username.isError = true;
                              newError.username.message = "Username is required";
                              setUserUsername(value);
                              setUserError(newError);
                            } else if (value.length < 6) {
                              const newError = { ...UserError };
                              newError.username.isError = true;
                              newError.username.message = "Username must be at least 6 characters";
                              setUserError(newError);
                            } else if (value.length > 20) {
                              const newError = { ...UserError };
                              newError.username.isError = true;
                              newError.username.message = "Username must be less than 20 characters";
                              setUserError(newError);
                            } else {
                              const newError = { ...UserError };
                              newError.username.isError = false;
                              newError.username.message = "";
                              setUserError(newError);
                              setUserUsername(value);
                            }
                          }}
                          className={`${UserError?.username.isError == true
                            ? "items-start w-full p-3 mt-2  rounded-lg outline-red-normal border-red-normal outline-offset-2 mb-1"
                            : "items-start w-full p-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                            }`}
                          type="text"
                          placeholder="Username"
                        />
                        {UserError?.username.isError && (
                          <p className="text-red-normal text-[12px] start-0 flex">
                            {UserError?.username.message}
                          </p>
                        )}
                      </div>
                      <div className="ml-5">
                        <p className="text-left text-[16] font-normal">Email</p>

                        <input
                          defaultValue={isEdit ? user_email : userEmail}
                          onSubmit={(e) => {
                            e.preventDefault();
                          }}
                          onChange={(val) => {
                            const emailPattern =
                              /^[a-zA-Z0-9._%+-]+@(gmail\.com|kit\.edu\.kh)$/;
                            const isValidEmail = emailPattern.test(
                              val.target.value
                            );
                            if (val.target.value.length > 0) {
                              if (isValidEmail == false) {
                                const newError = { ...UserError };
                                newError.email.isError = true;
                                newError.email.message =
                                  "must be a valid email address";
                                setUserError(newError);
                              } else if (isValidEmail == true) {
                                const newError = { ...UserError };
                                newError.email.isError = false;
                                newError.email.message = "";
                                setUserError(newError);
                                setUserEmail(val.target.value);
                              }
                            }
                            else if (val.target.value.length === 0) {
                              const newError = { ...UserError };
                              newError.email.isError = true;
                              newError.email.message =
                                "email is required";
                              setUserEmail(val.target.value);
                              setUserError(newError);
                            }
                            else {
                              const newError = { ...UserError };
                              newError.email.isError = false;
                              newError.email.message = "";
                              setUserError(newError);
                            }
                          }}
                          className={`${UserError?.email.isError == true
                            ? "items-start w-full p-3 mt-2  rounded-lg outline-red-normal border-red-normal outline-offset-2 mb-1"
                            : "items-start w-full p-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                            }`}
                          type="text"
                          placeholder="Email"
                        />
                        {UserError?.email.isError && (
                          <p className="text-red-normal text-[12px] start-0 flex">
                            {UserError?.email.message}
                          </p>
                        )}
                      </div>
                    </div>


                    <div className="flex flex-col text-start">
                      Gender
                      <select
                        defaultValue={userGenders}
                        onChange={handleSelectGenderChange}
                        value={userGenders}
                        className="items-start w-full p-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                      >
                        {GenderOptions.map((option) => (
                          <option key={option.value} value={option.value}>
                            {option.label}
                          </option>
                        ))}
                      </select>
                    </div>
                    {isEdit ? userRole !== "ADMIN" ? (<div className="flex flex-col text-start">
                      Role
                      <select
                        defaultValue={userRole}
                        onChange={handleSelectRoleChange}
                        value={userRole}
                        className="items-start w-full p-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                      >
                        {RoleOptionsForEdit.map((option) => (
                          <option key={option.value} value={option.value}>
                            {option.label}
                          </option>
                        ))}
                      </select>
                    </div>) : null
                      : (
                        <div className="flex flex-col text-start">
                          Role
                          <select
                            defaultValue={userRole}
                            value={userRole}
                            onChange={handleSelectRoleChange}
                            className="items-start w-full p-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                          >
                            {RoleOptions.map((option) => (
                              <option key={option.value} value={option.value}>
                                {option.label}
                              </option>
                            ))}
                          </select>
                        </div>
                      )}

                    {((userRole === "STUDENT" || userRole === "STAFF") && userDepartment !== null) || userRole === "STUDENT" ? (
                      <div>
                        <div className="flex flex-row text-start">
                          <div className="flex flex-col mr-4">
                            Department
                            <select
                              value={userDepartment}
                              onChange={handleSelectDepartmentChange}
                              className="items-start w-56 p-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                            >
                              {/* <option value="">Select Department</option> */}
                              {DepartmentOptions.map((option) => (
                                <option key={option.value} value={option.value}>
                                  {option.label}
                                </option>
                              ))}
                            </select>
                          </div>

                          {userDepartment == "SOFTWAREENGINEERING" ? (
                            <div>
                              Batch
                              <select
                                value={userBatchNum}
                                onChange={handleSelectBatchNumChange}
                                className="items-start p-3 mt-2 mb-5 rounded-lg w-52 outline-primary-normal outline-offset-2"
                              >
                                {BatchSEOptions.map((option) => (
                                  <option
                                    key={option.value}
                                    value={option.value}
                                  >
                                    {option.label}
                                  </option>
                                ))}
                              </select>
                            </div>
                          ) : null}

                          {userDepartment == "TOURISMANDMANAGEMENT" ? (
                            <div>
                              Batch
                              <select
                                value={userBatchNum}
                                onChange={handleSelectBatchNumChange}
                                className="items-start p-3 mt-2 mb-5 rounded-lg w-52 outline-primary-normal outline-offset-2"
                              >
                                {BatchTMOptions.map((option) => (
                                  <option
                                    key={option.value}
                                    value={option.value}
                                  >
                                    {option.label}
                                  </option>
                                ))}
                              </select>
                            </div>
                          ) : null}

                          {userDepartment == "ARCHITECTURE" ? (
                            <div>
                              Batch
                              <select
                                value={userBatchNum}
                                onChange={handleSelectBatchNumChange}
                                className="items-start p-3 mt-2 mb-5 rounded-lg w-52 outline-primary-normal outline-offset-2"
                              >
                                {BatchARCOptions.map((option) => (
                                  <option
                                    key={option.value}
                                    value={option.value}
                                  >
                                    {option.label}
                                  </option>
                                ))}
                              </select>
                            </div>
                          ) : null}
                        </div>
                      </div>
                    ) : null}
                    {/* Include password */}
                    {(isEdit && includePassword) || !isEdit ? (
                      <div className="flex flex-row justify-between">
                        <div>
                          <p className="text-left text-[16] font-normal">
                            New Password
                          </p>

                          <div
                            className={
                              UserError?.password.isError == true
                                ? "flex flex-row mt-2 mb-1"
                                : "flex flex-row mt-2 mb-5"
                            }
                          >
                            <input
                              className="items-start w-full p-3 rounded-lg outline-primary-normal outline-offset-2"
                              defaultValue={""}
                              onSubmit={(e) => {
                                e.preventDefault();
                              }}
                              onChange={(val) => {
                                if (
                                  val.target.value.length < 5 &&
                                  val.target.value.length > 0
                                ) {
                                  const newError = { ...UserError };
                                  newError.password.isError = true;
                                  newError.password.message =
                                    "Password  at least 5 characters";
                                  setUserError(newError);
                                } else {
                                  const newError = { ...UserError };
                                  newError.password.isError = false;
                                  newError.password.message = "";
                                  setUserError(newError);
                                  setUserPassword(
                                    !includePassword && isEdit
                                      ? null
                                      : val.target.value
                                  );
                                }
                              }}
                              type={type}
                              placeholder="New Password"
                              autoComplete="current-password"
                            />
                            <span
                              className="flex items-center justify-around "
                              onClick={handleToggle}
                            >
                              <Image
                                className="absolute flex items-center mr-10"
                                src={icon}
                                alt=""
                                width={16}
                                height={16}
                              />
                            </span>
                          </div>
                          {UserError?.password.isError && (
                            <p className="text-red-normal text-[12px] start-0 flex">
                              {UserError?.password.message}
                            </p>
                          )}
                        </div>
                        <div className="ml-5">
                          <p className="text-left text-[16] font-normal">
                            Confirm New Password
                          </p>

                          <div
                            className={
                              UserError?.confirmPassword.isError == true
                                ? "flex flex-row mt-2 mb-1"
                                : "flex flex-row mt-2 mb-5"
                            }
                          >
                            <input
                              defaultValue={isEdit ? "" : userConfirmPassword}
                              onSubmit={(e) => {
                                e.preventDefault();
                              }}
                              onChange={(val) => {
                                if (
                                  val.target.value.length < 5 &&
                                  val.target.value.length > 0
                                ) {
                                  const newError = { ...UserError };
                                  newError.confirmPassword.isError = true;
                                  newError.confirmPassword.message =
                                    "Password  at least 5 characters";
                                  setUserError(newError);
                                } else if (userPassword != val.target.value) {
                                  const newError = { ...UserError };
                                  newError.confirmPassword.isError = true;
                                  newError.confirmPassword.message =
                                    "Password does not match";
                                  setUserError(newError);
                                } else if (
                                  userPassword == val.target.value &&
                                  val.target.value.length > 0 &&
                                  val.target.value.length > 4
                                ) {
                                  const newError = { ...UserError };
                                  newError.confirmPassword.isError = false;
                                  newError.confirmPassword.message = "";
                                  setUserError(newError);
                                  setUserConfirmPassword(val.target.value);
                                }
                              }}
                              className={`${UserError?.confirmPassword.isError == true
                                ? "items-start w-full p-3   rounded-lg outline-red-normal border-red-normal outline-offset-2"
                                : "items-start w-full p-3  rounded-lg outline-primary-normal outline-offset-2"
                                }`}
                              type={type2}
                              autoComplete="current-password"
                              placeholder="Confirm Password"
                            />

                            <span
                              className="flex items-center justify-around "
                              onClick={handleToggle2}
                            >
                              <Image
                                className="absolute flex items-center mr-10"
                                src={icon2}
                                alt=""
                                width={16}
                                height={16}
                              />
                            </span>
                          </div>
                          {UserError?.confirmPassword.isError && (
                            <p className="text-red-normal text-[12px] start-0 flex">
                              {UserError?.confirmPassword.message}
                            </p>
                          )}
                        </div>
                      </div>
                    ) : null}
                    {isEdit ? (
                      <div
                        className="flex flex-row p-2 cursor-pointer"
                        onClick={() => {
                          setIncludePassword(!includePassword);
                        }}
                      >
                        <div
                          className={`w-[25px] h-[25px] rounded-full border border-black-normal mr-2 ${includePassword
                            ? "bg-primary-normal"
                            : "bg-white-normal border border-black-normal"
                            }`}
                        ></div>

                        <div className="flex items-center ">
                          <p className="text-base font-normal">
                            Include Password
                          </p>
                        </div>
                      </div>
                    ) : null}

                    <div className="flex justify-between">
                      <div className="flex-1 pr-5">
                        <SubmitButton
                          onClick={() => {
                            setShowModal(false);
                          }}
                          title={"Cancel"}
                          className="w-full mt-5  bg-red-active hover:bg-red-normal px-5  p-2.5"
                        />
                      </div>
                      <div className="flex-1">
                        <SubmitButton
                          onClick={() => {
                            if (isEdit === true) {
                              handleUpdateUser();
                            } else {
                              handleCreate();
                            }
                          }}
                          title={title}
                          isDisabled={
                            // create
                            (!isEdit &&
                              (!userEmail ||
                                !userUsername ||
                                !userGenders ||
                                !userRole ||
                                !userPassword ||
                                !userConfirmPassword)) ||
                            (userRole === "STUDENT" &&
                              (!userDepartment || !userBatchNum)) ||
                            // update && include password
                            (isEdit &&
                              includePassword &&
                              (!userEmail ||
                                !userUsername ||
                                !userGenders ||
                                !userRole ||
                                !userPassword ||
                                !userConfirmPassword)) ||
                            (userRole === "STUDENT" &&
                              (!userDepartment || !userBatchNum)) ||
                            // update && not include password
                            (isEdit &&
                              !includePassword &&
                              (!userEmail ||
                                !userUsername ||
                                !userGenders ||
                                !userRole)) ||
                            (userRole === "STUDENT" &&
                              (!userDepartment || !userBatchNum))
                          }
                          className={
                            `${handleValidateDisableButton}` ? "w-full mt-5" : "w-full mt-5 hover:bg-primary-hover"
                          }
                        />

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : showTerminal ? (
        <ShowTerminal
          content={content}
          description={description}
          show={true}
          success={success}
          onClick={() => {
            setShowTerminal(false);
          }}
        />
      ) : null}
    </>
  );
};
