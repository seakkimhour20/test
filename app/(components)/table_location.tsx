"use client";
import { MainLocation } from "@/types/main_location";
import { useEffect, useState } from "react";
import { SubLocation } from "./sub_location_model";
import { SubLocationModel } from "@/types/sub_location";
import DeleteModel from "./delete_model";
import { get_all } from "../pages/api/location";
import { LocationModel } from "./location_model";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import DeleteSubLocationModal from "./delete_subLocation_model";


function TableComponent() {
  const [tableData, setTableData] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const response = await get_all();
      setTableData(response.data);
    }
    fetchData();
  }, []);

  const convertName = (location: string) => {
    return location
      .split(" ")
      .map((word) => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase())
      .join(" ");
  };
  return (
    <>
      {tableData.map((data: MainLocation) => (
        <div className="flex flex-col mt-[40px] ml-5 " key={data.id}>
          {/* header */}
          <div className="flex flex-row">
            <div className="w-1/2">
              <p className="text-[25px] font-bold">
                {convertName(data.mainLocationName)}
              </p>
            </div>
            <div className="w-1/2">
              <div className="flex">
                <div className="w-1/4">
                  {/* Create */}
                  <SubLocation
                    isEdit={false}
                    main_location_id={data.id}
                    isIcon={true}
                    isTitle={false}
                    isDisabled={false}
                    className={``}
                  />
                </div>
                <div className="flex items-center justify-center w-1/2">
                  {/* Edit */}
                  <LocationModel
                    isEdit={true}
                    name={data.mainLocationName}
                    id={data.id}
                    isIcon={false}
                    isTitle={true}
                    isDisabled={false}
                    className={`font-bold text-[25px] text-gray-800 hover:text-gray-400`}
                  />
                </div>
              </div>
            </div>
          </div>
          {/* table */}
          <div className="flex flex-row mt-3 ml-2">
            <div className="w-1/2 ">
              <div className="flex font-bold text-gray-400">
                <div className="w-1/6">No</div>
                <div className="">Location</div>
              </div>
            </div>
            <div />
          </div>
          {data.SubLocation.map((sub: SubLocationModel, index: number) => (
            <div className="flex flex-row p-2 " key={sub.id}>
              <div className="w-1/2 ">
                <div className="flex">
                  <div className="w-1/6 pl-1">{index + 1}</div>
                  <div className="w-35%">
                    {convertName(sub.subLocationName)}
                  </div>
                </div>
              </div>

              <div className="flex w-1/2">
                <div className="w-1/4"></div>

                <div className="flex items-center justify-center w-1/2">
                  <SubLocation
                    isEdit={true}
                    main_location_id={data.id}
                    sub_location_id={sub.id}
                    name={sub.subLocationName}
                    isIcon={true}
                    isTitle={false}
                    isDisabled={false}
                    className={``}
                  />
                  {/* <DeleteModel item={convertName(sub.subLocationName)} id={sub.id} /> */}
                  <DeleteSubLocationModal
                    item={`${sub.subLocationName}`}
                    id={sub.id}
                    enable={false}
                    className={``}
                  />
                </div>
              </div>

              <div />
            </div>
          ))}
        </div>
      ))}
      <ToastContainer />
    </>
  );
}
export { TableComponent };
