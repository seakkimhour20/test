"use client";
import { useState } from "react";
import { SubmitButton } from "./submit_botton";
import { IconButton } from "./Icon_botton";
import { createBus, updateBus } from "../pages/api/bus";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ShowTerminal from "./show_terminal";
import { BusError } from "../../types/bus_error";
export const BusModel: React.FunctionComponent<any> = ({
  isEdit,
  bus_model,
  bus_plateNumber,
  bus_numOfSeat,
  bus_driverName,
  bus_driverContact,
  bus_enable,
  bus_id,
  isIcon,
  isTitle,
  className,
  isDisabled,
}) => {
  const [showModal, setShowModal] = useState(false);
  const [driverName, setDriverName] = useState(
    bus_driverName == "" ? "" : bus_driverName
  );
  const [driverNumber, setDriverNumber] = useState(
    bus_driverContact == "" ? "" : bus_driverContact
  );
  const [model, setModel] = useState(bus_model == "" ? "" : bus_model);
  const [seats, setSeats] = useState(bus_numOfSeat == "" ? "" : bus_numOfSeat);
  const [plateNumber, setPlateNumber] = useState(
    bus_plateNumber == "" ? "" : bus_plateNumber
  );
  const [enable, setEnable] = useState(bus_enable == "" ? "" : bus_enable);

  // for terminal
  const [success, setSuccess] = useState(false);
  const [showTerminal, setShowTerminal] = useState(false);
  const [description, setDiscription] = useState("");
  const [content, setContent] = useState("");
  const [validate, setValidate] = useState<BusError>({
    phoneNumber: {
      isError: false,
      message: "",
    },
    plateNumber: {
      isError: false,
      message: "",
    },
  } as BusError);
  const title = isEdit ? "Save Change" : "Create";
  const button_title = isEdit ? "Edit" : "Add New Bus";
  const changeTheme = () => {
    return setShowModal(!showModal);
  };

  async function handleCreateBus() {
    const newData = {
      driverName,
      driverNumber,
      model,
      seats,
      plateNumber,
      enable,
    };
    const handlePlateNumberChange = (e: any) => {
      const inputValue = e.target.value;

      // Validate the input against the pattern
      const pattern = /^2[A-Z]{2}-[0-9]{4}$/;
      if (pattern.test(inputValue)) {
        setPlateNumber(inputValue);
      }
    };
    const res = await createBus({
      model: newData.model,
      plateNumber: newData.plateNumber,
      numOfSeat: Number(newData.seats),
      driverName: newData.driverName,
      driverContact: newData.driverNumber,
      enable: true,
    });
    if (res?.status == 200 || res?.status == 201) {
      const data = await res.json();

      setSuccess(true);
      setContent("Created");
      setDiscription(data.message);
      setShowModal(false);
      setShowTerminal(true);
      setTimeout(() => {
        setShowTerminal(false);
        window.location.reload();
      }, 2000);

      // Set it to null value for every field after create
      setDriverName(""), setDriverNumber("");
      setModel("");
      setSeats("");
      setPlateNumber("");
    } else if (res?.status === 409) {
      const data = await res.json();

      setSuccess(false);
      setShowModal(false);
      setContent("Fail to Update");
      setDiscription(data.error);
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
    } else {
      setSuccess(false);
      setShowModal(false);
      setContent("Error");
      setDiscription("Something Went Wrong");
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
    }
  }

  async function handleUpdateBus() {
    const res = await updateBus(
      bus_id,
      model,
      plateNumber,
      seats,
      driverName,
      driverNumber,
      enable
    );
    if (res?.status == 200 || res?.status == 201) {
      const data = await res.json();

      setSuccess(true);
      setContent("Updated");
      setDiscription(data.message);
      setShowModal(false);
      setShowTerminal(true);
      setTimeout(() => {
        setShowTerminal(false);
        window.location.reload();
      }, 2000);

      // Set it to null value for every field after create
      setDriverName(""), setDriverNumber("");
      setModel("");
      setSeats("");
      setPlateNumber("");
    } else if (res?.status === 409) {
      const data = await res.json();

      setSuccess(false);
      setShowModal(false);
      setContent("Fail to Update");
      setDiscription(data.error);
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
    } else {
      setSuccess(false);
      setShowModal(false);
      setContent("Error");
      setDiscription("Something Went Wrong");
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
    }
  }
  return (
    <>
      <IconButton
        title={button_title}
        onClick={() => changeTheme()}
        img={
          isEdit
            ? isDisabled === false
              ? "/edit.svg"
              : "/edit_disable.svg"
            : "/add.svg"
        }
        w={22}
        h={22}
        isIconOnly={isIcon}
        isTitleOnly={isTitle}
        className={`${className}`}
        isDisabled={isDisabled}
      />
      {(showModal && bus_enable) ||
      (showModal && button_title == "Add New Bus") ? (
        <>
          <div className="fixed inset-0 z-10 overflow-y-auto backdrop-blur-sm">
            <div
              className="fixed inset-0 w-full h-full bg-black"
              onClick={() => setShowModal(false)}
            ></div>
            <div className="flex items-center min-h-screen px-4 py-8">
              <div className="relative w-full max-w-lg p-4 mx-auto bg-black rounded-md shadow-2xl bg-light-hover">
                <div className="mt-3">
                  <div className="mt-2 text-center sm:ml-4 sm:text-center sm:mr-4">
                    <h4 className="text-[24px] font-bold text-gray-800">
                      {button_title}
                    </h4>
                    <p className="flex font-bold flex-start">
                      Driver Information
                    </p>
                    <div className="flex flex-row justify-between mt-3">
                      <div>
                        <p className="text-left text-[16] font-normal">
                          Driver Name
                        </p>

                        <input
                          defaultValue={bus_driverName}
                          onSubmit={(e) => {
                            e.preventDefault();
                          }}
                          onChange={(val) => setDriverName(val.target.value)}
                          className="items-start w-full p-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                          type="text"
                          placeholder="Enter Driver Name"
                        />
                      </div>
                      <div className="ml-5">
                        <p className="text-left text-[16] font-normal">
                          Driver Number
                        </p>

                        <input
                          defaultValue={bus_driverContact}
                          onSubmit={(e) => {
                            e.preventDefault();
                          }}
                          onChange={(val) => {
                            const cambodianPhoneNumberRegex =
                              /^(?:\+855|0)[1-9]\d{7,8}$/;
                            if (
                              cambodianPhoneNumberRegex.test(val.target.value)
                            ) {
                              setValidate({
                                ...validate,
                                phoneNumber: {
                                  isError: false,
                                  message: "",
                                },
                              });
                            } else {
                              setValidate({
                                ...validate,
                                phoneNumber: {
                                  isError: true,
                                  message: "Phone number is invalid",
                                },
                              });
                            }

                            setDriverNumber(
                              val.target.value.replace(/[^0-9+]/g, "")
                            );
                          }}
                          className={`${
                            validate?.phoneNumber.isError == true
                              ? "items-start w-full p-3 mt-2  rounded-lg outline-red-normal border-red-normal outline-offset-2 mb-1"
                              : "items-start w-full p-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                          }`}
                          onKeyDown={(e) => {
                            if (
                              e.key === "Delete" ||
                              e.key === "Backspace" ||
                              e.key === "ArrowLeft" ||
                              e.key === "ArrowRight"
                            ) {
                              return;
                            }
                            const keyValue = e.key;
                            const numericRegex = /^[0-9]+$/;
                            const plusRegex = /^\+$/;
                            if (
                              !numericRegex.test(keyValue) &&
                              !plusRegex.test(keyValue)
                            ) {
                              e.preventDefault();
                            }
                          }}
                          type="text"
                          placeholder="Enter Contact Number"
                        />
                        {validate.phoneNumber.isError ? (
                          <p className="text-red-normal text-[12px] start-0 flex">
                            Phone number is invalid
                          </p>
                        ) : null}
                      </div>
                    </div>

                    <p className="flex font-bold flex-start">Bus Information</p>
                    <div className="mt-2">
                      <p className="text-left text-[16] font-normal">Model</p>

                      <input
                        defaultValue={bus_model}
                        onSubmit={(e) => {
                          e.preventDefault();
                        }}
                        onChange={(val) => setModel(val.target.value)}
                        className="items-start w-full p-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                        type="text"
                        placeholder="Enter the Model"
                      />
                    </div>
                    <div className="flex flex-row justify-between">
                      <div>
                        <p className="text-left text-[16] font-normal">Seat</p>

                        <input
                          defaultValue={bus_numOfSeat}
                          onSubmit={(e) => e.preventDefault()}
                          onChange={(val) => setSeats(val.target.value)}
                          className="items-start w-full p-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                          type="number"
                          pattern="[0-9]*"
                          placeholder="Enter the Seats"
                        />
                      </div>
                      <div className="ml-5">
                        <p className="text-left text-[16] font-normal">
                          Plate Number
                        </p>

                        <input
                          defaultValue={bus_plateNumber}
                          onSubmit={(e) => {
                            e.preventDefault();
                          }}
                          onChange={(val) => {
                            const cambodianPlateNumberRegex =
                              /^\d{1}[A-Z]{2}-\d{4}$/;
                            if (val.target.value == "") {
                              setValidate({
                                ...validate,
                                plateNumber: {
                                  isError: false,
                                  message: "",
                                },
                              });
                            } else if (
                              cambodianPlateNumberRegex.test(val.target.value)
                            ) {
                              setValidate({
                                ...validate,
                                plateNumber: {
                                  isError: false,
                                  message: "",
                                },
                              });
                            } else {
                              setValidate({
                                ...validate,
                                plateNumber: {
                                  isError: true,
                                  message: "Plate number is invalid",
                                },
                              });
                            }

                            setPlateNumber(val.target.value);
                          }}
                          className={`${
                            validate?.plateNumber.isError == true
                              ? "items-start w-full p-3 mt-2  rounded-lg outline-red-normal border-red-normal outline-offset-2 mb-1"
                              : "items-start w-full p-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                          }`}
                          type="text"
                          maxLength={9}
                          placeholder="(e.g., 2BB-0000)"
                          required
                        />
                        {validate.plateNumber.isError ? (
                          <p className="text-red-normal text-[12px] start-0 flex">
                            plate number is invalid
                          </p>
                        ) : null}
                      </div>
                    </div>
                    <div className="flex justify-between">
                      <div className="flex-1 pr-5">
                        <SubmitButton
                          onClick={() => {
                            setShowModal(false);
                          }}
                          title={"Cancel"}
                          className="w-full mt-5  bg-red-active hover:bg-red-normal px-5  p-2.5"
                        />
                      </div>
                      <div className="flex-1">
                        <SubmitButton
                          onClick={() => {
                            if (isEdit === true) {
                              handleUpdateBus();
                            } else {
                              handleCreateBus();
                            }
                          }}
                          className={"w-full mt-5"}
                          title={title}
                          isDisabled={
                            !driverName ||
                            !driverNumber ||
                            !model! ||
                            !seats ||
                            !plateNumber ||
                            validate?.phoneNumber.isError ||
                            validate?.plateNumber.isError ||
                            driverName.trim() === ""
                              ? true
                              : false
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : showTerminal ? (
        <ShowTerminal
          content={content}
          description={description}
          show={true}
          success={success}
          onClick={() => {
            setShowTerminal(false);
          }}
        />
      ) : null}
    </>
  );
};
