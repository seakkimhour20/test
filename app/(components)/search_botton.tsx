"use client";
import { useState } from "react";
import { AiOutlineSearch } from "react-icons/ai";
export const SearchButton: React.FunctionComponent<any> = ({
  placeholder,
  searchFunction,
}) => {
  return (
    <div className="flex items-center p-1 align-middle border-2 rounded-md bg-white-active w-80 h-11">
      <AiOutlineSearch className="mx-1" />
      <input
        type="text"
        onChange={(e) => searchFunction(e.target.value)}
        className="w-full bg-transparent border-collapse outline-none text-grey-normal" placeholder={!placeholder ? "Search by name, gender, email" : placeholder}
        
      />
    </div>
  );
};
