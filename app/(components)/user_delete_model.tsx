'use client';
import React, { useState } from 'react';
import { OutLineButton } from './out_botton';
import { IconButton } from './Icon_botton';
import { deleteUser } from '../pages/api/user';
import { ToastContainer, toast } from 'react-toastify';
import Button from '../(components)/Button';
import 'react-toastify/dist/ReactToastify.css';
import ShowTerminal from './show_terminal';
const DeleteUserModel: React.FunctionComponent<any> = ({ item, id }) => {
  const [showModal, setShowModal] = useState(false);
  // for terminal
  const [success, setSuccess] = useState(false)
  const [showTerminal, setShowTerminal] = useState(false)
  const [description, setDiscription] = useState("")
  const [content, setContent] = useState("")


  const handleDeleteUser = async () => {
    const res = await deleteUser(id)
    if (res?.status == 200 || res?.status == 201) {
      setSuccess(true)
      setContent('Delete')
      setDiscription('1 user deleted successful.')
      setShowModal(false)
      setShowTerminal(true)
      setTimeout(() => {
        setShowTerminal(false);
        window.location.reload();
      }, 2000)
      return
    } else {
      setShowModal(false)
      setSuccess(false)
      setContent('Faile to Delete')
      setDiscription("Something Went Wrong, Can't Delete")
      setShowTerminal(true)
    }
    return
  }

  return (
    <>
      <IconButton
        className={'ml-2'}
        isIconOnly={true}
        isTitleOnly={false}
        img={'/delete.svg'}
        isDisabled={false}
                w={22}
        h={22}
        onClick={() => setShowModal(true)} />
      {showModal ? (
        <>
          <div className='fixed inset-0 z-10 overflow-y-auto backdrop-blur-sm'>
            <div
              className='fixed inset-0 w-full h-full bg-black'
              onClick={() => setShowModal(false)}></div>
            <div className='flex items-center min-h-screen px-4 py-8'>
              <div className='relative w-full max-w-lg p-4 mx-auto bg-black rounded-md shadow-2xl bg-light-hover'>
                <div className='mt-3'>
                  <div className='mt-2 text-center sm:ml-4 sm:text-center sm:mr-4'>
                    <h4 className='text-2xl font-bold text-gray-800'>
                      Are you sure that you want to delete {item} ?
                    </h4>

                    <div className='flex pt-10'>
                      <div className='flex-1 pr-10 '>
                        <Button
                          title='Yes'
                          onClick={() => handleDeleteUser()}
                          color='red'
                          isPrimary={true}
                        />
                      </div>
                      <div className='flex-1'>
                        <Button
                          title='No'
                          onClick={() => setShowModal(false)}
                          color='red'
                          isPrimary={false}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : showTerminal ? (<ShowTerminal content={content} description={description} show={true} success={success} onClick={() => { setShowTerminal(false) }} />)
        : null}
      {/* <ToastContainer /> */}
    </>
  );
};

export default DeleteUserModel;
