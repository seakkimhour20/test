"use client";
import { Chart as ChartJS, ArcElement, Tooltip, Legend, ChartOptions } from "chart.js";
import { Doughnut } from "react-chartjs-2";
import { getAllUser } from "../pages/api/user";
import { useEffect, useState } from "react";

ChartJS.register(ArcElement, Tooltip, Legend);

export const DoughnutChart: React.FunctionComponent<any> = () => {
  const [user, setUser] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const responseData = await getAllUser();
      setUser(responseData.data);
    };
    fetchData();
  }, []);


  const data = {
    labels: [`In Krr ${(user.filter((u: any) => u.inKRR === true)).length}`, `Out Krr ${(user.filter((u: any) => u.inKRR !== true)).length}`],
    datasets: [
      {
        data: [(user.filter((u: any) => u.inKRR === true)).length, (user.filter((u: any) => u.inKRR !== true)).length],
        backgroundColor: ['#0E6431', '#ADCAB9'],
        hoverBackgroundColor: ['#0E6431', '#ADCAB9'],

      },
    ],
  };

  const options = {
    plugins: {
      legend: {
        position: 'right',
        align: 'center', // Align the legend items to the center
        labels: {
          boxWidth: 14,
          padding: 20,
          usePointStyle: true,
        },
      },

    },
  } as ChartOptions<'doughnut'>;


  return (
    <div className="w-52">
      <div className="relative">
        <Doughnut data={data} options={options} />
        <div className="absolute inset-0 flex items-center justify-center">
          <div className="flex flex-col text-sm">
            <span className="w-7 text-center">{user.length}</span>
            <span className="mr-20"><span className="mr-4">Total</span></span>
          </div>
        </div>
      </div>
    </div>
  );
};

