'use client';
import Image from 'next/image';

interface IconButtonProps {
  title: any;
  onClick: any;
  isDisabled: any;
  img: any;
  className: any;
  isIconOnly: any;
  isTitleOnly: any;
  w: any;
  h: any;
  buttonWidth?: any;
}
export const IconButton: React.FunctionComponent<any> = (
  props: IconButtonProps
) => {
  const {
    title,
    onClick,
    isDisabled,
    img,
    className,
    isIconOnly,
    isTitleOnly,
    w,
    h,
    buttonWidth
  } = props;
  return (
    <button
      type='submit'
      disabled={isDisabled === false ? false : true}
      onClick={onClick}
      // className={`flex items-center justify-center p-2 px-5 align-middle rounded-md cursor-pointer  text-white-normal h-11 bg-primary-normal hover:bg-primary-hover`}>
      className={
        !className
          ? `flex items-center justify-center p-2 px-5 align-middle rounded-md cursor-pointer`
          : `flex items-center justify-center p-2 px-5 align-middle rounded-md cursor-pointer ${className} ${buttonWidth}`
      }>
      {isIconOnly ? (
        <Image
          src={!img ? '/default.svg' : img}
          alt=''
          width={!w ? 20 : w}
          height={!h ? 20 : h}
        />
      ) : null}
      {isTitleOnly ? (
        <p className='px-5 text-white outline-none items-center text-center text-[16px] '>
          {title}
        </p>
      ) : null}
    </button>
  );
};
