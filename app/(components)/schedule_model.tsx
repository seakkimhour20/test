"use client";
import React, { useRef } from "react";
import { SubmitButton } from "./submit_botton";
import { IconButton } from "./Icon_botton";
import { getAllBus, getBus } from "../pages/api/bus";
import {
  addNewSchedule,
  getSchedule,
  updateSchedule,
} from "../pages/api/schedule";
import { useEffect, useState } from "react";
import { getAllDeparture, getDeparture } from "../pages/api/departure";
import { DepartureResponse } from "@/types/departure";
import { BusResponse } from "@/types/bus";
import { CreateScheduleDto, UpdateScheduleDto } from "@/types/schedule";
import ShowTerminal from "./show_terminal";
import { formatTime } from "@/helper/format_time";
export const ScheduleModel: React.FunctionComponent<any> = ({
  isEdit,
  isCreate,
  isImport,
  departure_id,
  departure_date,
  schedule_id,
  isIcon,
  isTitle,
  className,
  isDisabled,
  bus,
  departure,
}) => {
  const departureListFormat = [
    {
      id: "",
      value: "Select Departure",
    },
  ];
  const busListFormat = [
    {
      id: "",
      value: "Select Bus",
      seat: 0,
    },
  ];

  const [showModal, setShowModal] = useState(false);
  const [listDeparture, setListDeparture] = useState([]);
  const [listBus, setListBus] = useState([]);
  const fileInputRef = useRef<any>();

  // for terminal
  const [success, setSuccess] = useState(false);
  const [showTerminal, setShowTerminal] = useState(false);
  const [description, setDiscription] = useState("");
  const [content, setContent] = useState("");

  // Bus
  const [busIds, setBusIds] = useState<string>("");
  const [busModel, setBusModel] = useState("");
  const [busPlateNumber, setBusPlateNumber] = useState("");
  const [busDriverName, setBusDriverName] = useState("");
  const [busDriverContact, setBusDriverContact] = useState("");

  const [selectedFile, setSelectedFile] = useState<any>();
  // Departure
  const [departureIds, setDepartureIds] = useState<string>(
    departure_id == " " ? "" : departure_id
  );
  const [from, setFrom] = useState("");
  const [destination, setDestination] = useState("");
  const [pickupLocation, setPickupLocation] = useState("");
  const [dropOffLocation, setDropOffLocation] = useState("");
  const [date, setDate] = useState<string>(
    departure_date == "" ? "" : departure_date
  );
  const [numBookings, setNumBookings] = useState<number>(0);
  const [time, setTime] = useState("");

  const title =
    !isCreate && !isImport && isEdit
      ? "Save Change"
      : isCreate && !isImport && !isEdit
      ? "Create"
      : "Import";

  const button_title =
    !isCreate && !isImport && isEdit
      ? "Edit"
      : isCreate && !isImport && !isEdit
      ? "Create Schedule"
      : "Import Schedule";

  const changeTheme = () => {
    return setShowModal(!showModal);
  };

  useEffect(() => {
    if (isEdit == true) {
      setTime(formatTime(departure?.departureTime));
      setDepartureIds(departure?.id);
      // setDate(new Date(departure_date).toLocaleDateString());
      setDate(new Date(departure_date).toISOString().substring(0, 10));

      setBusIds(bus?.id);
      setDropOffLocation(departure?.dropLocation?.subLocationName);
      setBusDriverName(bus?.driverName);
      setBusDriverContact(bus?.driverContact);
      setBusModel(bus?.model);
      setBusPlateNumber(bus?.plateNumber);
      setPickupLocation(departure?.pickupLocation?.subLocationName);
      setFrom(departure?.from?.mainLocationName);
      setDestination(departure?.destination?.mainLocationName);
    }
    const fetchDataDeparture = async () => {
      const resDeparture = await getAllDeparture();
      const responseDeparture = await resDeparture.data;

      responseDeparture.forEach(
        (element: any) =>
          (element.departureTime = formatTime(element.departureTime))
      );
      setListDeparture(responseDeparture);
      return responseDeparture;
    };

    const fectDataBus = async () => {
      const resBus = await getAllBus();
      const responseBus = await resBus.data;
      setListBus(responseBus);
      return responseBus;
    };

    const fetchDataSchedule = async () => {
      const resSchedule = await getSchedule(schedule_id);
      const responseSchedule = await resSchedule.data;
      setNumBookings(responseSchedule?.booking.length);
      return responseSchedule;
    };

    listDeparture.length == 0 ? fetchDataDeparture() : null;
    listBus.length == 0 ? fectDataBus() : null;
    schedule_id != "" ? fetchDataSchedule() : null;
  }, [listBus.length, listDeparture.length, schedule_id]);

  // DepartureList
  listDeparture.map((item: DepartureResponse) => {
    departureListFormat.push({
      id: item.id,
      value: `${item?.from?.mainLocationName?.toLocaleUpperCase()} - ${item?.destination?.mainLocationName?.toLocaleUpperCase()}  |  Time: ${
        item?.departureTime
      }`,
    });
  }, []);

  // BusList
  listBus
    .filter((item: BusResponse) => item?.enable === true)
    .map((item: BusResponse) => {
      busListFormat.push({
        id: item.id,
        value: `${item?.model?.toLocaleUpperCase()} - Seat:${item?.numOfSeat}`,
        seat: item?.numOfSeat,
      });
    }, []);

  const handleSelectDepartureChange = async (event: any) => {
    if (isEdit == true) {
      setDepartureIds(event.target.value);
      const departure = await getDeparture(event.target.value);
      const responseDeparture = (await departure?.data) as DepartureResponse;
      setFrom(responseDeparture?.from?.mainLocationName);
      setDestination(responseDeparture?.destination?.mainLocationName);
      setPickupLocation(responseDeparture?.pickupLocation?.subLocationName);
      setDropOffLocation(responseDeparture?.dropLocation?.subLocationName);
      setTime(formatTime(responseDeparture?.departureTime as string));
    } else {
      setDepartureIds(event.target.value);
      const departure = await getDeparture(event.target.value);
      const responseDeparture = (await departure?.data) as DepartureResponse;
      setFrom(responseDeparture?.from?.mainLocationName);
      setDestination(responseDeparture?.destination?.mainLocationName);
      setPickupLocation(responseDeparture?.pickupLocation?.subLocationName);
      setDropOffLocation(responseDeparture?.dropLocation?.subLocationName);
      setTime(formatTime(responseDeparture?.departureTime as string));
    }
  };

  const handleSelectBusChange = async (event: any) => {
    setBusIds(event.target.value);
    const bus = await getBus(event.target.value);
    const responseBus = (await bus?.data) as BusResponse;
    setBusModel(responseBus?.model);
    setBusPlateNumber(responseBus?.plateNumber);
    setBusDriverContact(
      responseBus?.driverContact === null ? "N/A" : responseBus?.driverContact
    );
    setBusDriverName(responseBus?.driverName);
  };

  const handleSelectDateChange = (event: any) => {
    setDate(event.target.value);
  };

  const handleFileChange = (event: any) => {
    setSelectedFile(event.target.files[0]);
  };
  const isExcelFile = (fileName: string) => {
    return fileName.endsWith(".xlsx") || fileName.endsWith(".xls");
  };

  const handleUpload = async (e: any) => {
    e.preventDefault();

    if (selectedFile && isExcelFile(selectedFile.name)) {
      const formData = new FormData();
      formData.append("file", selectedFile);
      const res = await fetch(`${process.env.base}/schedule/import`, {
        method: "POST",
        headers: {
          // "Content-Type": "multipart/form-data;",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: formData,
      });
      const data = await res.json();
      // Handle the response
      if (res.status == 200) {
        setSuccess(true);
        setContent("Import Success");
        setDiscription(data.message);
        setShowModal(false);
        setShowTerminal(true);
        setTimeout(() => {
          setShowTerminal(false);
          window.location.reload();
        }, 2000);

        setSelectedFile("");
        return;
      }
    } else {
      setSuccess(false);
      setShowModal(false);
      setContent("Error");
      setDiscription("Invalid file format. Please select an Excel file.");
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
      return;
    }
  };

  // Handle create schedule
  async function create() {
    const newData = {
      departureId: departureIds,
      busId: busIds || null || "",
      date: date,
    } as CreateScheduleDto;

    const res = await addNewSchedule(newData);
    // const data = await res.json()
    if (res?.status == 200) {
      const data = await res.json();

      setSuccess(true);
      setContent("Created");
      setDiscription(data.message);
      setShowModal(false);
      setShowTerminal(true);
      setTimeout(() => {
        setShowTerminal(false);
        window.location.reload();
      }, 2000);

      // Set it to null value for every field after create
      setDepartureIds(""), setBusIds("");
      setFrom("");
      setDestination("");
      setPickupLocation("");
      setDropOffLocation("");
      setTime("");
      setDate("");
      setBusDriverName("");
      setBusDriverContact("");
      setBusModel("");
      setBusPlateNumber("");

      return;
    } else if (res?.status == 409) {
      const data = await res.json();

      setSuccess(false);
      setShowModal(false);
      setContent("Duplicate Schedule");
      setDiscription(data.error);
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
      return;
    } else {
      setSuccess(false);
      setShowModal(false);
      setContent("Error");
      setDiscription("Something Went Wrong");
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
      return;
    }
  }

  // Handle update schedule
  async function update() {
    const newData = {
      departureId: departureIds,
      busId: busIds,
      date: date,
    } as UpdateScheduleDto;

    const res: any = await updateSchedule(schedule_id, newData);
    if (res?.status == 200) {
      setSuccess(true);
      setContent("Update Success");
      setDiscription("Schedule Has Updated");
      setShowModal(false);
      setShowTerminal(true);
      setTimeout(() => {
        setShowTerminal(false);
        window.location.reload();
      }, 2000);
      return;
    } else if (res?.status == 409) {
      const data = await res.json();
      setSuccess(false);
      setShowModal(false);
      setContent("Duplicate");
      setDiscription(data.error);
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
      return;
    } else {
      setSuccess(false);
      setShowModal(false);
      setContent("Error");
      setDiscription("Something went wrong");
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
      return;
    }
  }

  // for format date to : dd-mm-yy
  function formatDate(dateString: string): string {
    const date = new Date(dateString);
    const day = date.getDate();
    const month = date.toLocaleString("default", { month: "short" });
    const year = date.getFullYear();
    return `${day}/${month}/${year}`;
  }

  // set min Date
  const minDate = new Date().toISOString().split("T")[0];
  return (
    <>
      <IconButton
        title={button_title}
        onClick={() => changeTheme()}
        img={
          isCreate && !isImport
            ? "/add.svg"
            : isCreate && isImport
            ? "/file.svg"
            : isEdit && isDisabled
            ? "./edit_disable.svg"
            : isEdit
            ? "./edit.svg"
            : ""
        }
        isIconOnly={isIcon}
        isTitleOnly={isTitle}
        className={`${className}`}
        isDisabled={isDisabled}
      />
      {showModal ? (
        (isCreate === true && isImport === false && isEdit === false) ||
        (isCreate === false && isImport === false && isEdit === true) ? (
          <>
            <div className="fixed inset-0 z-10 overflow-y-auto backdrop-blur-sm">
              <div
                className="fixed inset-0 w-full h-full bg-black"
                onClick={() => {
                  setShowModal(false);
                }}
              ></div>
              <div className="flex items-center min-h-screen px-4 py-2">
                <div className="relative w-full max-w-lg p-4 mx-auto bg-black rounded-md shadow-2xl bg-light-hover">
                  <div className="mt-1">
                    <div className="mt-1 text-center sm:ml-4 sm:text-center sm:mr-4">
                      <h4 className="text-[24px] font-bold text-gray-800">
                        {button_title}
                      </h4>
                      <div className="flex flex-col w-full mt-7">
                        {" "}
                        <p className="flex text-xl font-bold flex-start">
                          Schedule Information
                        </p>
                        <div className="flex flex-row mt-2">
                          <div className="flex flex-col text-start">
                            <p className="text-left text-[16] font-normal">
                              Departure
                            </p>
                            <select
                              value={departureIds}
                              onChange={handleSelectDepartureChange}
                              className="items-start w-full p-3 mt-1 rounded-lg outline-primary-normal outline-offset-2"
                            >
                              {departureListFormat.map((option) => (
                                <option key={option.value} value={option.id}>
                                  {option.value}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                        <div className="flex flex-row justify-between mt-3 ">
                          <div>
                            <p className="text-left text-[16] font-normal">
                              Bus
                            </p>

                            <select
                              value={busIds}
                              onChange={handleSelectBusChange}
                              className="items-start w-full p-3 mt-1 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                            >
                              {isEdit
                                ? busListFormat
                                    .filter((bus) => bus.seat >= numBookings)
                                    .map((option) => (
                                      <option
                                        key={option.value}
                                        value={option.id}
                                      >
                                        {option.value}
                                      </option>
                                    ))
                                : busListFormat.map((option) => (
                                    <option
                                      key={option.value}
                                      value={option.id}
                                    >
                                      {option.value}
                                    </option>
                                  ))}
                            </select>
                          </div>
                          <div className="ml-5">
                            <p className="text-left text-[16] font-normal">
                              Departure Date
                            </p>
                            <input
                              className="items-start w-full py-2 pl-3 mt-1 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                              min={minDate}
                              type="date"
                              value={date}
                              onChange={handleSelectDateChange}
                            />
                          </div>
                        </div>
                      </div>
                      {/* Destination Infor */}
                      <div className="flex flex-col mt-3">
                        {" "}
                        <p className="flex text-xl font-bold flex-start">
                          Destination Information
                        </p>
                        <div className="flex flex-row justify-between mt-2">
                          <div>
                            <p className="text-left text-[16] font-normal">
                              From
                            </p>

                            <input
                              disabled={true}
                              value={from}
                              onSubmit={(e) => {
                                e.preventDefault();
                              }}
                              onChange={(val) => setFrom(val.target.value)}
                              className="items-start w-full py-1 pl-3 mt-1 mb-5 rounded-lg bg-white-normal outline-primary-normal outline-offset-2"
                              type="text"
                              placeholder=" "
                            />
                          </div>
                          <div className="ml-5">
                            <p className="text-left text-[16] font-normal">
                              To
                            </p>

                            <input
                              disabled={true}
                              value={destination}
                              onSubmit={(e) => {
                                e.preventDefault();
                              }}
                              onChange={(val) =>
                                setDestination(val.target.value)
                              }
                              className="items-start w-full py-1 pl-3 mt-1 mb-5 rounded-lg bg-white-normal outline-primary-normal outline-offset-2"
                              type="text"
                              placeholder=" "
                            />
                          </div>
                        </div>
                        <div className="flex flex-row justify-between">
                          <div>
                            <p className="text-left text-[16] font-normal">
                              Pick Up Location
                            </p>

                            <input
                              disabled={true}
                              className="items-start w-full py-1 pl-3 mt-1 mb-5 rounded-lg bg-white-normal outline-primary-normal outline-offset-2"
                              value={pickupLocation}
                              onSubmit={(e) => {
                                e.preventDefault();
                              }}
                              onChange={(val) =>
                                setPickupLocation(val.target.value)
                              }
                              type="text"
                              placeholder=" "
                            />
                          </div>
                          <div className="ml-5">
                            <p className="text-left text-[16] font-normal">
                              Drop Off Location
                            </p>

                            <input
                              disabled={true}
                              value={dropOffLocation}
                              onSubmit={(e) => {
                                e.preventDefault();
                              }}
                              onChange={(val) =>
                                setDropOffLocation(val.target.value)
                              }
                              className="items-start w-full py-1 pl-3 mt-1 mb-5 rounded-lg bg-white-normal outline-primary-normal outline-offset-2"
                              type="text"
                              placeholder=" "
                            />
                          </div>
                        </div>
                      </div>

                      {/* Departure Infor */}
                      <div className="flex flex-col mt-3">
                        <p className="flex text-xl font-bold flex-start">
                          Departure Information
                        </p>
                        <div className="flex flex-row justify-between mt-2">
                          <div className="">
                            <p className="text-left text-[16] font-normal">
                              Departure Time
                            </p>

                            <input
                              disabled={true}
                              value={time}
                              onSubmit={(e) => {
                                e.preventDefault();
                              }}
                              onChange={(val) => setTime(val.target.value)}
                              className="items-start w-full py-1 pl-3 mt-1 mb-5 rounded-lg bg-white-normal outline-primary-normal outline-offset-2"
                              type="text"
                              placeholder=" "
                            />
                          </div>
                          <div>
                            <p className="text-left text-[16] font-normal">
                              Departure Date
                            </p>
                            <input
                              disabled={true}
                              // value={date}
                              value={formatDate(date)}
                              onSubmit={(e) => {
                                e.preventDefault();
                              }}
                              onChange={(val) => setDate(val.target.value)}
                              className="items-start w-full py-1 pl-3 mt-1 mb-5 rounded-lg bg-white-normal outline-primary-normal outline-offset-2"
                              type="text"
                              placeholder=" "
                            />
                          </div>
                        </div>
                      </div>
                      {/* Bus Infor */}
                      <div className="flex flex-col mt-2">
                        {" "}
                        <p className="flex text-xl font-bold flex-start">
                          Bus Information
                        </p>
                        <div className="flex flex-row justify-between mt-2">
                          <div>
                            <p className="text-left text-[16] font-normal">
                              Drive Name
                            </p>

                            <input
                              value={busDriverName}
                              disabled={true}
                              onSubmit={(e) => {
                                e.preventDefault();
                              }}
                              onChange={(val) =>
                                setBusDriverName(val.target.value)
                              }
                              className="items-start w-full py-1 pl-3 mt-1 mb-5 rounded-lg bg-white-normal outline-primary-normal outline-offset-2"
                              type="text"
                              placeholder=" "
                            />
                          </div>
                          <div className="ml-5">
                            <p className="text-left text-[16] font-normal">
                              Driver Contact
                            </p>

                            <input
                              disabled={true}
                              value={busDriverContact}
                              onSubmit={(e) => {
                                e.preventDefault();
                              }}
                              onChange={(val) =>
                                setBusDriverContact(val.target.value)
                              }
                              className="items-start w-full py-1 pl-3 mt-1 mb-5 rounded-lg bg-white-normal outline-primary-normal outline-offset-2"
                              type="text"
                              placeholder=" "
                            />
                          </div>
                        </div>
                        <div className="flex flex-row justify-between">
                          <div className="">
                            <p className="text-left text-[16] font-normal">
                              Model
                            </p>
                            <input
                              disabled={true}
                              value={busModel?.toUpperCase()}
                              onSubmit={(e) => {
                                e.preventDefault();
                              }}
                              onChange={(val) => setBusModel(val.target.value)}
                              className="items-start w-full py-1 pl-3 mt-1 mb-5 rounded-lg bg-white-normal outline-primary-normal outline-offset-2"
                              type="text"
                              placeholder=" "
                            />
                          </div>
                          <div className="ml-5">
                            <p className="text-left text-[16] font-normal">
                              Plate Number
                            </p>

                            <input
                              disabled={true}
                              value={busPlateNumber}
                              onSubmit={(e) => {
                                e.preventDefault();
                              }}
                              onChange={(val) =>
                                setBusPlateNumber(val.target.value)
                              }
                              className="items-start w-full py-1 pl-3 mt-1 mb-5 rounded-lg bg-white-normal outline-primary-normal outline-offset-2"
                              // className='items-start w-full py-2 pl-3 mt-1 mb-5 rounded-lg bg-white-normal outline-primary-normal outline-offset-2'
                              type="text"
                              placeholder=" "
                            />
                          </div>
                        </div>
                      </div>
                      <div className="flex justify-between">
                        <div className="flex-1 pr-5">
                          <SubmitButton
                            onClick={() => {
                              setShowModal(false);
                            }}
                            title={"Cancel"}
                            className="w-full mt-5 bg-red-active hover:bg-red-normal"
                          />
                        </div>
                        <div className="flex-1">
                          <SubmitButton
                            onClick={() => {
                              if (!isCreate && !isImport && isEdit) {
                                update();
                              } else if (isCreate && !isImport && !isEdit) {
                                create();
                              }
                            }}
                            isDisabled={
                              !departureIds ||
                              !date ||
                              departureIds === null ||
                              date === null
                                ? true
                                : false
                            }
                            title={title}
                            // className='w-full mt-5 hover:bg-primary-hover '
                            className="w-full mt-5 "
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        ) : (
          // Import file
          <>
            <div className="fixed inset-0 z-10 overflow-y-auto backdrop-blur-sm">
              <div
                className="fixed inset-0 w-full h-full bg-black"
                onClick={() => {
                  setShowModal(false);
                  setSelectedFile(null);
                }}
              ></div>
              <div className="flex items-center min-h-screen px-4 py-8">
                <div className="relative w-full max-w-lg p-4 mx-auto bg-black rounded-md shadow-2xl bg-light-hover">
                  <div className="mt-3">
                    <div className="mt-2 text-center sm:ml-4 sm:text-center sm:mr-4">
                      <h4 className="text-[24px] font-bold text-gray-800">
                        {button_title}
                      </h4>
                      <div className="flex flex-col mt-7">
                        <p className="flex text-xl font-bold flex-start">
                          Location File
                        </p>
                        <div className="flex flex-row mt-3">
                          <input
                            className="items-start w-full py-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                            onChange={handleFileChange}
                            type="file"
                            name="file"
                            accept=".xlsx, .xls"
                          />
                        </div>
                      </div>
                      <div className="flex justify-between">
                        <div className="flex-1 pr-5">
                          <SubmitButton
                            onClick={() => {
                              setShowModal(false);
                            }}
                            title={"Cancel"}
                            className="w-full mt-5 bg-red-active hover:bg-red-normal"
                          />
                        </div>
                        <div className="flex-1">
                          <SubmitButton
                            onClick={handleUpload}
                            isDisabled={selectedFile == null ? true : false}
                            title={title}
                            className="w-full mt-5"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        )
      ) : showTerminal ? (
        <ShowTerminal
          content={content}
          description={description}
          show={true}
          success={success}
          onClick={() => {
            setShowTerminal(false);
          }}
        />
      ) : null}
    </>
  );
};
