"use client";
import { useState } from "react";
import { SubmitButton } from "./submit_botton";
import { OutLineButton } from "./out_botton";
import { create_sublocation, edit_sublocation } from "../pages/api/location";
import { IconButton } from "./Icon_botton";
import ShowTerminal from "./show_terminal";
export const SubLocation: React.FunctionComponent<any> = ({
  isEdit,
  main_location_id,
  sub_location_id,
  name,
  isIcon,
  isTitle,
  className,
  isDisabled
}) => {
  // for terminal
  const [success, setSuccess] = useState(false);
  const [showTerminal, setShowTerminal] = useState(false);
  const [description, setDiscription] = useState("");
  const [content, setContent] = useState("");

  const [showModal, setShowModal] = useState(false);
  const [subLocation, setSubLocation] = useState(name != "" ? name : "");
  const title = isEdit ? "Edit Point" : "Add New Point";
  const sub_title = isEdit ? "Edit Point Location" : "New Point Location";
  const button_title = isEdit ? "Save Change" : "Add";
  const model_button_title = isEdit ? "Edit" : "Add New Point";
  const changeTheme = () => {
    // setSubLocation("")
    return setShowModal(!showModal);
  };

  // Handle Create
  const handleCreate_subLocation = async () => {
    const res = await create_sublocation(subLocation, main_location_id);
    if (res?.status == 200 || res?.status == 201) {
      const data = await res.json();

      setSuccess(true);
      setContent("Created");
      setDiscription(data.message);
      setShowModal(false);
      setShowTerminal(true);
      setTimeout(() => {
        setShowTerminal(false);
        window.location.reload();
      }, 2000);

      // Set it to null value for every field after create
      subLocation("");
    } else if (res?.status === 409) {
      const data = await res.json();

      setSuccess(false);
      setShowModal(false);
      setContent("Fail to Create");
      setDiscription(data.error);
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
    } else {

      setSuccess(false);
      setShowModal(false);
      setContent("Error");
      setDiscription("Something Went Wrong");
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
    }
  };

  // Handle Update:
  const handleUpdate_subLocation = async () => {
    const res = await edit_sublocation(
      sub_location_id,
      subLocation,
      main_location_id
    );
    if (res?.status == 200 || res?.status == 201) {
      const data = await res.json();
  
      setSuccess(true);
      setContent("Updated");
      setDiscription(data.message);
      setShowModal(false);
      setShowTerminal(true);
      setTimeout(() => {
        setShowTerminal(false);
        window.location.reload();
      }, 2000);

      // Set it to null value for every field after create
      subLocation("");
    } else if (res?.status === 409) {
      const data = await res.json();
  
      setSuccess(false);
      setShowModal(false);
      setContent("Fail to Update");
      setDiscription(data.error);
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
    } else {
      
      setSuccess(false);
      setShowModal(false);
      setContent("Error");
      setDiscription("Something Went Wrong");
      setTimeout(() => {
        setShowTerminal(true);
      }, 400);
    }
  };
  return (
    <>
      {isEdit ? (
        <IconButton
          title={button_title}
          onClick={() => changeTheme()}
          img={isEdit ? "/edit.svg" : "/add.svg"}
          isIconOnly={isIcon}
          isTitleOnly={isTitle}
          className={`${className}`}
          isDisabled={isDisabled}
          w={23}
          h={23}
        />
      ) : (
        <OutLineButton
          onClick={() => changeTheme()}
          title={model_button_title}
          className={`${className}`}
        />
      )}
      {showModal ? (
        <>
          <div className="fixed inset-0 z-10 overflow-y-auto backdrop-blur-sm">
            <div
              className="fixed inset-0 w-full h-full bg-black"
              onClick={() => setShowModal(false)}
            ></div>
            <div className="flex items-center min-h-screen px-4 py-8">
              <div className="relative w-full max-w-lg p-4 mx-auto bg-black rounded-md shadow-2xl bg-light-hover">
                <div className="mt-3">
                  <div className="mt-2 text-center sm:ml-4 sm:text-center sm:mr-4">
                    <h4 className="text-[24px] font-bold text-gray-800">
                      {title}
                    </h4>
                    <p className="text-left text-[16] font-normal mt-5">
                      {sub_title}
                    </p>

                    <input
                      defaultValue={name}
                      onSubmit={(e) => {
                        e.preventDefault();
                      }}
                      onChange={(val) => setSubLocation(val.target.value)}
                      className="items-start w-full p-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                      type="text"
                      placeholder="Enter New Point Location"
                    />
                    <div className="flex justify-between">
                      <div className="flex-1 pr-5">
                        <SubmitButton
                          onClick={() => {
                            setShowModal(false);
                          }}
                          title={"Cancel"}
                          className="w-full mt-5  bg-red-active hover:bg-red-normal px-5  p-2.5"
                        />
                      </div>
                      <div className="flex-1">
                        <SubmitButton
                          onClick={() => {
                            if (isEdit == true) {
                              handleUpdate_subLocation();
                            } else {
                              handleCreate_subLocation();
                            }
                          }}
                          title={button_title}
                          isDisabled={
                            subLocation == "" || !subLocation ? true : false
                          }
                          className={`${
                            subLocation == "" || !subLocation
                              ? "w-full mt-5 bg-gray-500"
                              : "w-full mt-5 hover:bg-primary-hover"
                          }`}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : showTerminal ? (
        <ShowTerminal
          content={content}
          description={description}
          show={true}
          success={success}
          onClick={() => {
            setShowTerminal(false);
          }}
        />
      ) : null}
    </>
  );
};
