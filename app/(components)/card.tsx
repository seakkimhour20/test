"use client";
import Image from "next/image";

export const Card: React.FunctionComponent<any> = ({
  icon,
  number,
  title,
}) => {
  return (
    <div className="overflow-hidden bg-white rounded-lg shadow-md w-52 h-52">
        <div>
          <div className="flex flex-row justify-center pt-3 bg-green-800 h-14">
          <Image src={icon === null || icon === undefined ? "/default.svg" : icon} alt="" width={32} height={32} />
          </div>
          <div className="flex flex-col items-center justify-center h-full p-4 text-green-900 bg-green-100 font-inter">
            <h2 className="mb-4 font-semibold text-7xl">{number}</h2>
            <h2 className="text-xl font-semibold mb-9">{title}</h2>
          </div>
        </div>
    </div>
  );
};
