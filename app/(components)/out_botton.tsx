"use client";
import Image from "next/image";
export const OutLineButton: React.FunctionComponent<any> = ({
  title,
  onClick,
  icon,
  className,
}) => {
  if (icon == undefined) {
    return (
      <div
        onClick={onClick}
        className={`flex items-center align-middle rounded-md cursor-pointer text-black-normal ${className}`}
      >
        <p
          className={`text-white outline-none items-center text-center text-[1em]`}
        >
          {title}
        </p>
      </div>
    );
  }
  return (
    <div
      onClick={onClick}
      className={`flex items-center align-middle rounded-md cursor-pointer text-black-normal ${className}`}
    >
      <Image src={icon} alt="" className="w-6 h-6 text-black-normal" width={14} height={14}/>
      <p
        className={`text-white outline-none items-center text-center text-[1em]`}
      >
        {title}
      </p>
    </div>
  );
};
