"use client";
import * as React from "react";
import { Table, changeTheme } from "@nextui-org/react";
import { BiPencil } from "react-icons/bi";
import { BusModel } from "./bus_model";
import { ScheduleModel } from "./schedule_model";
import { Button } from "@nextui-org/react";
import { MdOutlineEdit } from "react-icons/md";

interface Props {
  columnList: {
    key: string;
    label: string;
  }[];
  rowList: {
    key: string | number;
    driverName: string;
    driverNumber: string;
    from: string;
    to: string;
    departureDate: string | Date;
    departureTime: string;
    seat: number;
    booked: number;
    waiting: number;
    operation: string;
    [key: string]: any; // add index signature here
  }[];
}

const TableSchedule: React.FC<Props> = ({ columnList, rowList }) => {
  const changeTheme = () => {};
  return (
    <table className="w-full border-0 border-collapse table-auto">
      <thead>
        <tr className="text-sm font-normal text-gray-500">
          {columnList.map((colName: any, index: number) => (
            <th className="px-4 py-2 text-left" key={index}>{colName.label}</th>
          ))}
        </tr>
      </thead>
      <tbody className="text-green-950">
        {/* Loop over the busData array and generate a table row for each object */}
        {rowList.map((sheduleDt: any, index: any) => (
          <tr key={index}>
            <td className="px-4 py-2">{index + 1}</td>
            <td className="px-4 py-2">{sheduleDt.driverName}</td>
            <td className="px-4 py-2">{sheduleDt.driverNumber}</td>
            <td className="px-4 py-2">{sheduleDt.from}</td>
            <td className="px-4 py-2">{sheduleDt.to}</td>
            <td className="px-4 py-2">{sheduleDt.departureDate}</td>
            <td className="px-4 py-2">{sheduleDt.departureTime}</td>
            <td className="px-4 py-2">{sheduleDt.seat}</td>
            <td className="px-4 py-2">{sheduleDt.booked}</td>
            <td className="px-4 py-2">{sheduleDt.waiting}</td>
            <td className="flex items-center justify-center px-4 py-2">
              <ScheduleModel 
                isEdit={true}
                bus_id={"default"}
                departureId={"default"}
                scheduleId={"default"}/>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default TableSchedule;
