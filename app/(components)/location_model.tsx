"use client";
import { useState } from "react";
import { SubmitButton } from "./submit_botton";
import { IconButton } from "./Icon_botton";
import { create_mainlocation, edit_mainlocation } from "../pages/api/location";
import { OutLineButton } from "./out_botton";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
export const LocationModel: React.FunctionComponent<any> = ({
  isEdit,
  name,
  id,
  isIcon,
  isTitle,
  className,
  isDisabled,
  width,
  height,
}) => {
  const [showModal, setShowModal] = useState(false);
  const [location, setLocation] = useState(name != "" ? name : "");
  const title = isEdit ? "Save Change" : "Add";
  const button_title = isEdit == true ? "Edit" : "Add New Location";

  const changeTheme = () => {
    return setShowModal(!showModal);
  };

  return (
    <>
      <IconButton
        title={button_title}
        onClick={() => changeTheme()}
        img={isEdit ? "/edit.svg" : "/add.svg"}
        isIconOnly={isIcon}
        isTitleOnly={isTitle}
        className={`${className}`}
        isDisabled={isDisabled}
        w={width}
        h={height}
      />
      {showModal ? (
        <>
          <div className="fixed inset-0 z-10 overflow-y-auto backdrop-blur-sm">
            <div
              className="fixed inset-0 w-full h-full bg-black"
              onClick={() => setShowModal(false)}
            ></div>
            <div className="flex items-center min-h-screen px-4 py-8">
              <div className="relative w-full max-w-lg p-4 mx-auto bg-black rounded-md shadow-2xl bg-light-hover">
                <div className="mt-3">
                  <div className="mt-2 text-center sm:ml-4 sm:text-center sm:mr-4">
                    <h4 className="text-[24px] font-bold text-gray-800">
                      {button_title}
                    </h4>
                    <p className="text-left text-[16] font-normal mt-5">
                      New Location
                    </p>

                    <input
                      onSubmit={(e) => {
                        e.preventDefault();
                      }}
                      defaultValue={name}
                      onChange={(val) => setLocation(val.target.value)}
                      className="items-start w-full p-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2"
                      type="text"
                      placeholder="Enter New Location"
                    />
                    <div className="flex justify-between">
                      <div className="flex-1 pr-5">
                        <SubmitButton
                          onClick={() => {
                            setShowModal(false);
                          }}
                          title={"Cancel"}
                          className="w-full mt-5  bg-red-active hover:bg-red-normal px-5  p-2.5"
                        />
                      </div>
                      <div className="flex-1">
                        <SubmitButton
                          onClick={async () => {
                            if (isEdit) {
                              await edit_mainlocation(location, id);
                            } else {
                              await create_mainlocation(location);
                            }
                          }}
                          isDisabled={
                            location == "" || !location ? true : false
                          }
                          title={title}
                          className={`${
                            location == "" || !location
                              ? "w-full mt-5 bg-gray-500"
                              : "w-full mt-5 hover:bg-primary-hover"
                          }`}
                        />
                      </div>

                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : null}
      <ToastContainer />
    </>
  );
};
