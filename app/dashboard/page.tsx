"use client";
import * as React from "react";
import { UserModel } from "../(components)/user_model";
import { SearchButton } from "../(components)/search_botton";
import { deleteUser, getAllUser } from "../pages/api/user";
import { useEffect, useState } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { updateUser } from "../pages/api/user";
import withAuth from "../../services/withAuth";

import "react-tabs/style/react-tabs.css";
import DeleteUserModel from "../(components)/user_delete_model";
import { StudentChart } from "../(components)/student_chart";
import { UserResponse } from "@/types/user";
import { getAllBatch } from "../pages/api/batch";
import { MdSearchOff } from "react-icons/md";

const Dashboard = () => {
  const DepartmentOptions = [
    { value: "", label: "All Department" },
    { value: "SOFTWAREENGINEERING", label: "Software Engineering" },
    { value: "TOURISMANDMANAGEMENT", label: "Tourism Management" },
    { value: "ARCHITECTURE", label: "Architecture" },
  ];

  const BatchSEOptions = [{ value: "", label: "All Batch" }];

  const BatchTMOptions = [{ value: "", label: "All Batch" }];

  const BatchARCOptions = [{ value: "", label: "All Batch" }];

  var [data, setData] = useState([]);
  const [userDepartment, setUserDepartment] = useState("");
  const [userBatchNum, setUserBatchNum] = useState("");
  var [display, setDisplay] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [rowsPerPage, setRowsPerPage] = useState(25);
  const [allBatch, setAllBatch] = useState<any>([]);
  const handlePageChange = (pageNumber: any) => {
    setCurrentPage(pageNumber);
  }; // Set the number of rows per page

  useEffect(() => {
    const fetchData = async () => {
      const responseData = await getAllUser();
      setData(responseData.data.filter((user: any) => user.role === "STUDENT"));
      setDisplay(
        responseData.data.filter((user: any) => user.role === "STUDENT")
      );
    };

    fetchData();
  }, []);

  useEffect(() => {
    const fetchBatchData = async () => {
      const responseBatchData = await getAllBatch();
      setAllBatch(responseBatchData.data);
    };

    fetchBatchData();
  }, []);

  for (let i = 0; i < allBatch.length; i++) {
    if (allBatch[i].department === "SOFTWAREENGINEERING") {
      BatchSEOptions.push({
        value: allBatch[i].batchNum,
        label: `SE-B${allBatch[i].batchNum}`
      });
    } else if (allBatch[i].department === "TOURISMANDMANAGEMENT") {
      BatchTMOptions.push({
        value: allBatch[i].batchNum,
        label: `TM-B${allBatch[i].batchNum}`,
      });
    } else if (allBatch[i].department === "ARCHITECTURE") {
      BatchARCOptions.push({
        value: allBatch[i].batchNum,
        label: `ARC-B${allBatch[i].batchNum}`
      });
    }
  }

  if (userDepartment && userBatchNum) {
    display = data.filter(
      (user: any) =>
        user.role === "STUDENT" &&
        user.studentInfo.batch.department === userDepartment &&
        user.studentInfo.batch.batchNum === Number(userBatchNum)
    );
  } else if (userDepartment) {
    display = data.filter(
      (user: any) =>
        user.role === "STUDENT" &&
        user.studentInfo.batch.department === userDepartment
    );
  }

  const indexOfLastRow = currentPage * rowsPerPage;
  const indexOfFirstRow = indexOfLastRow - rowsPerPage;
  const currentRows = display.slice(indexOfFirstRow, indexOfLastRow);

  const search = (search: string) => {
    const result = data.filter((user: any) => {
      return (
        user.username.toLowerCase().includes(search.toLowerCase()) ||
        user.email.toLowerCase().includes(search.toLowerCase())
      );
    });
    setDisplay(result);
  };

  const handleUpdateStatus = async (
    user_id: any,
    role: string,
    email: string,
    username: string,
    password: string,
    department: string,
    phone: string,
    batchNum: number,
    inKrr: boolean,
    gender: string,
    enable: boolean
  ) => {
    try {
      const res = await updateUser(
        user_id,
        role,
        email,
        username,
        password,
        department,
        phone,
        batchNum,
        inKrr,
        gender,
        enable
      );
      if (res.status === 200) {
        window.location.reload();
      } else {
        console.error("Failed to edit user.");
      }
    } catch (error) {
      console.error(error);
    }
  };

  const toggleStatus = (status: boolean) => {

    return !status;
  };
  const handleSelectDepartmentChange = (event: any) => {
    setUserDepartment(event.target.value);
  };
  const handleSelectBatchNumChange = (event: any) => {
    setUserBatchNum(event.target.value);
  };
  const handleNext = () => {
    if (currentPage < Math.ceil(data.length / rowsPerPage)) {
      setCurrentPage(currentPage + 1);
    }
  };
  const handlePrev = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };
  return (
    <div className="ml-[13%]">
      <div className="flex flex-col w-[100%] p-[50px] bg-gray-100 h-full">
        <div className="flex flex-row items-center justify-between w-full">
          <div className="flex flex-col">
            <h4 className="font-bold text-[30px]">User Dashboard</h4>
            <div className="flex justify-center mt-10">
              <StudentChart
                department={userDepartment}
                batchNum={userBatchNum}
              />
            </div>
          </div>
          <div className="flex flex-row text-start">
            <div className="flex flex-col mr-4">
              <select
                value={userDepartment}
                onChange={handleSelectDepartmentChange}
                className="items-start w-56 p-3 mt-2 mb-5 rounded-lg outline-primary-normal outline-offset-2 bg-green-50"
              >
                {DepartmentOptions.map((option) => (
                  <option key={option.value} value={option.value}>
                    {option.label}
                  </option>
                ))}
              </select>
            </div>

            {userDepartment == "SOFTWAREENGINEERING" ? (
              <div>
                <select
                  value={userBatchNum}
                  onChange={handleSelectBatchNumChange}
                  className="items-start p-3 mt-2 mb-5 rounded-lg w-52 outline-primary-normal outline-offset-2 bg-green-50"
                >
                  {BatchSEOptions.map((option) => (
                    <option key={option.value} value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </select>
              </div>
            ) : null}

            {userDepartment == "TOURISMANDMANAGEMENT" ? (
              <div>
                <select
                  value={userBatchNum}
                  onChange={handleSelectBatchNumChange}
                  className="items-start p-3 mt-2 mb-5 rounded-lg w-52 outline-primary-normal outline-offset-2 bg-green-50"
                >
                  {BatchTMOptions.map((option) => (
                    <option key={option.value} value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </select>
              </div>
            ) : null}

            {userDepartment == "ARCHITECTURE" ? (
              <div>
                <select
                  value={userBatchNum}
                  onChange={handleSelectBatchNumChange}
                  className="items-start p-3 mt-2 mb-5 rounded-lg w-52 outline-primary-normal outline-offset-2"
                >
                  {BatchARCOptions.map((option) => (
                    <option key={option.value} value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </select>
              </div>
            ) : null}
          </div>
        </div>

        <div className="w-full mr-5">
          <Tabs className="mt-7">
            <TabList className="flex justify-between">
              <div>
                <Tab>Students</Tab>
              </div>
              <SearchButton searchFunction={search} />
            </TabList>
            {currentRows.length === 0 ? (
              <div className="flex flex-col items-center justify-center ">
                <MdSearchOff className="w-24 h-24" />
                <h1 className="text-2xl">No data</h1>
              </div>
            ) : (
              <TabPanel>
                <div className="mt-8 first-letter:overflow-x-auto">
                  <table className="w-full border-0 border-collapse table-auto">
                    <thead>
                      <tr className="text-sm font-normal text-gray-500">
                        <th className="px-4 py-2 text-left">No</th>
                        <th className="px-4 py-2 text-left">Username</th>
                        <th className="px-4 py-2 text-left">Gender</th>
                        <th className="px-4 py-2 text-left">Department</th>
                        <th className="px-4 py-2 text-left">Batch</th>
                        <th className="px-4 py-2 text-left">Gmail</th>
                        <th className="px-4 py-2 text-left">Status</th>
                        <th className="px-4 py-2 text-left">Active</th>
                        <th className="px-4 py-2 text-left">Tickets</th>
                        <th className="px-4 py-2 text-left"></th>
                      </tr>
                    </thead>
                    <tbody className="text-green-950">
                      {currentRows.map((user: UserResponse, index: any) => {
                        return (
                          <tr
                            key={user.id}
                            className={
                              user.enable ? "" : "text-gray-400 bg-opacity-60"
                            }
                          >
                            <td className="px-4 py-2">{(currentPage - 1) * rowsPerPage + index + 1}</td>
                            <td className="px-4 py-2">{user.username}</td>
                            <td className="px-4 py-2 text-center">
                              {user.gender == "MALE"
                                ? "M"
                                : "" || user.gender == "FEMALE"
                                  ? "F"
                                  : ""}
                            </td>
                            <td className="px-4 py-2">
                              {user.studentInfo.batch.department}
                            </td>
                            <td className="px-4 py-2 text-center">
                              {user.studentInfo.batch.batchNum}
                            </td>
                            <td className="px-4 py-2">{user.email}</td>
                            <td className="px-4 py-2">
                              {user.inKRR ? "In KRR" : "Out KRR"}
                            </td>
                            <td className="px-4 py-2">
                              <button
                                className={`relative inline-block w-10 h-6 rounded-full shadow-inner transition duration-300 ease-in-out ${user.enable ? "bg-green-800" : "bg-gray-200"
                                  }`}
                                onClick={() =>
                                  handleUpdateStatus(
                                    user.id,
                                    user.role,
                                    user.email,
                                    user.username,
                                    user.password,
                                    user.studentInfo.batch.department,
                                    user.phone,
                                    user.studentInfo.batch.batchNum,
                                    user.inKRR,
                                    user.gender,
                                    toggleStatus(user.enable)
                                  )
                                }
                              >
                                <span
                                  className={`absolute inset-0 w-6 h-6 flex items-center justify-center ${user.enable ? "ml-4" : ""
                                    }`}
                                >
                                  <span
                                    className={`w-5 h-5 rounded-full shadow-md ${user.enable ? "bg-gray-300" : "bg-white"
                                      }`}
                                  ></span>
                                </span>
                              </button>
                            </td>
                            <td className="px-4 py-2 text-center">
                              {user.ticket.remainTicket}/36
                            </td>
                            <td className="flex flex-row">
                              <button className="bg-transparent">
                                <UserModel
                                  isEdit={true}
                                  isIcon={true}
                                  user_id={user.id}
                                  user_username={user.username}
                                  user_email={user.email}
                                  user_password={user.password}
                                  user_gender={user.gender}
                                  user_role={user.role}
                                  user_department={
                                    user.studentInfo.batch.department
                                  }
                                  user_batchNum={user.studentInfo.batch.batchNum}
                                  userEnable={user.enable}
                                />
                              </button>
                              <DeleteUserModel
                                item={user.username}
                                id={user.id}
                                className=""
                              />
                            </td>
                          </tr>
                        )
                      }

                      )}
                    </tbody>
                  </table>
                </div>
                <div className="flex justify-end mt-4">
                  <div className="flex items-center">
                    <span className="mr-2">Rows per page:</span>
                    <select
                      value={rowsPerPage}
                      onChange={(e) => {
                        if (
                          Number(e.target.value) * currentPage >
                          data.length
                        ) {
                          setCurrentPage(1);
                          setRowsPerPage(Number(e.target.value));
                        } else {
                          setRowsPerPage(Number(e.target.value));
                        }
                      }}
                      className="px-2 py-1 border border-gray-300 rounded-md"
                    >
                      <option value={5}>5</option>
                      <option value={10}>10</option>
                      <option value={15}>15</option>
                      <option value={25}>25</option>
                      <option value={50}>50</option>
                      <option value={100}>100</option>
                      <option value={200}>200</option>
                    </select>
                  </div>
                  <div className="flex items-center ml-4">
                    <span className="mr-2">Page:</span>
                    {
                      <button
                        key={">"}
                        onClick={() => handlePrev()}
                        className={`border border-gray-300  rounded-md px-5 py-1 ml-1 focus:outline-none ${currentPage == 1 ? "bg-gray-200" : ""
                          }`}
                      >
                        {"<"}
                      </button>
                    }
                    {Array.from(
                      { length: Math.ceil(display.length / rowsPerPage) },
                      (_, i) => i + 1
                    ).map((page) => (
                      <button
                        key={page}
                        onClick={() => handlePageChange(page)}
                        className={`border border-gray-300 rounded-md px-2 py-1 ml-1 focus:outline-none ${currentPage === page ? "bg-gray-300" : ""
                          }`}
                      >
                        {page}
                      </button>
                    ))}
                    {
                      <button
                        key={">"}
                        onClick={() => handleNext()}
                        className={`border border-gray-300 rounded-md px-5 py-1 ml-1 focus:outline-none ${currentPage == Math.ceil(data.length / rowsPerPage)
                          ? "bg-gray-200"
                          : ""
                          }`}
                      >
                        {">"}
                      </button>
                    }
                  </div>
                </div>
              </TabPanel>
            )}
          </Tabs>
        </div>
      </div>
    </div>
  );
};

// export default Dashboard;
export default withAuth(Dashboard);
