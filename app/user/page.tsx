"use client";
import * as React from "react";
import { UserModel } from "../(components)/user_model";
import { DoughnutChart } from "../(components)/chart";
import { getAllUser } from "../pages/api/user";
import { useEffect, useState } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { updateUser } from "../pages/api/user";
import withAuth from "../../services/withAuth";
import { AiOutlineSearch } from "react-icons/ai";
import "react-tabs/style/react-tabs.css";
import DeleteUserModel from "../(components)/user_delete_model";
import { Card } from "../(components)/card";
import Link from "next/link";
import { MdSearchOff } from "react-icons/md";
import UserProfileCard from "../(components)/userProfile_card";
import { parse_jwt_token } from "@/helper/parse_jwt";


const UserManagement = () => {
  const [data, setData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [rowsPerPage, setRowsPerPage] = useState(25);
  const [query, setQuery] = useState("");
  const [tap, setTab] = useState(0);
  const [filter, setFilter] = useState("");
  const [loggedInUserRole, setLoggedInUserRole] = useState("");

  // profile
  const [turnOn, setTurnOn] = useState(false);
  const [items, setItem] = useState<any>({
    photo: "",
    name: "",
    email: "",
    department: "",
    batchNum: "",
    gender: "",
    status: "",
    remainTicket: 0,
    role: "",
    turnOn: turnOn,
  });

  const indexOfLastRow = currentPage * rowsPerPage;
  const indexOfFirstRow = indexOfLastRow - rowsPerPage;

  const handlePageChange = (pageNumber: any) => {
    setCurrentPage(pageNumber);
  }; // Set the number of rows per page

  useEffect(() => {
    const fetchData = async () => {
      const responseData = await getAllUser();
      setData(responseData.data);

      const token = localStorage.getItem("token");
      const userRole = parse_jwt_token(token) || ""; // Provide a default value for null case
      setLoggedInUserRole(userRole);
    };

    fetchData();
  }, []);



  const handleUpdateStatus = async (
    user_id: any,
    role: string,
    email: string,
    username: string,
    password: string | null,
    department: string,
    phone: string,
    batchNum: number,
    inKrr: boolean,
    gender: string,
    enable: boolean
  ) => {
    try {
      const res = await updateUser(
        user_id,
        role,
        email,
        username,
        password,
        department,
        phone,
        batchNum,
        inKrr,
        gender,
        enable
      );
      if (res.status === 200) {
        window.location.reload();
      } else {
        console.error("Failed to edit user.");
      }
    } catch (error) {
      console.error(error);
    }
  };

  const toggleStatus = (status: boolean) => {
    return !status;
  };

  const handleInputChange = (event: any) => {
    setCurrentPage(1);
    setQuery(event.target.value);
  };
  const clearSearchValue = () => {
    setQuery("");
  };


  const filterUsers = data.filter((item: any) => {

    if (tap === 0) {
      return (
        item.role === "STUDENT" &&
        (item.username.toLowerCase().includes(query.toLowerCase()) ||
          item.email.toLowerCase().includes(query.toLowerCase()))
      );
    } else if (tap === 1) {
      return (
        item.role === "ADMIN" &&
        (item.username.toLowerCase().includes(query.toLowerCase()) ||
          item.email.toLowerCase().includes(query.toLowerCase()))
      );
    } else {
      return (
        item.role === "STAFF" &&
        (item.username.toLowerCase().includes(query.toLowerCase()) ||
          item.email.toLowerCase().includes(query.toLowerCase()))
      );
    }

  });
  const handleNext = () => {
    if (tap === 0) {
      const totalPages = Math.ceil(
        filterUsers
          .filter((user: any) => user.role === "STUDENT" && (
            (filter === "" && user.inKRR) ||
            (filter === "inKrr" && user.inKRR) ||
            (filter === "outsideKrr" && !user.inKRR)
          ))
          .length / rowsPerPage
      );

      if (currentPage < totalPages) {
        setCurrentPage(currentPage + 1);
      }
    } else if (tap === 1) {
      const totalPages = Math.ceil(
        filterUsers
          .filter((user: any) => user.role === "ADMIN" && (
            (filter === "" && user.inKRR) ||
            (filter === "inKrr" && user.inKRR) ||
            (filter === "outsideKrr" && !user.inKRR)
          ))
          .length / rowsPerPage
      );

      if (currentPage < totalPages) {
        setCurrentPage(currentPage + 1);
      }
    } else {
      const totalPages = Math.ceil(
        filterUsers
          .filter((user: any) => user.role === "STAFF" && (
            (filter === "" && user.inKRR) ||
            (filter === "inKrr" && user.inKRR) ||
            (filter === "outsideKrr" && !user.inKRR)
          ))
          .length / rowsPerPage
      );

      if (currentPage < totalPages) {
        setCurrentPage(currentPage + 1);
      }
    }

  };

  const handlePrev = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const handleFilterChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    console.log(event.target.value);
    const selectedFilter = event.target.value;
    setFilter(selectedFilter);
  };



  const handlePopupProfile = (
    name: string,
    email: string,
    department: string,
    batchNum: number,
    gender: string,
    status: string,
    remainTicket: number,
    role: string,
    turnOn: boolean
  ) => {
    setTurnOn(true);
    return setItem({
      photo: gender == "MALE" ? "/male.svg" : "/female.svg",
      name: name,
      email: email,
      department: department,
      batchNum: batchNum,
      gender: gender,
      status: status,
      remainTicket: remainTicket,
      role: role,
      turnOn: turnOn,
    });
  };

  return (
    <div className="flex flex-col w-full p-[50px] min-h-screen bg-light-normal">
      <div className="ml-[13%]">
        <div
          onClick={() => {
            setTurnOn(false);
          }}
        >
          {turnOn ? <UserProfileCard items={items} /> : null}
        </div>
        <div className="flex flex-row items-center justify-between w-full align-middle">
          <div className="flex flex-col">
            <h4 className="font-bold text-[30px]">USER MANAGEMENT</h4>
            <div className="flex justify-center mt-10">
              <DoughnutChart />
            </div>
          </div>
          <div className="flex flex-col items-end">
            <UserModel
              isCreate={true}
              isImport={true}
              isEdit={false}
              isIcon={true}
              isTitle={true}

              className={`text-white-normal h-11 bg-primary-normal hover:bg-primary-hover`}
            />
            <div className="flex flex-row mt-5 mr-28 space-x-7">
              <Link href="/dashboard">
                <Card
                  title={"Students"}
                  number={
                    data.filter((item: any) => item.role === "STUDENT").length
                  }
                  icon={"/student.svg"}
                />
              </Link>

              <Card
                title={"Admins"}
                number={
                  data.filter((item: any) => item.role === "ADMIN").length
                }
                icon={"/admin.svg"}
              />
              <Card
                title={"Staffs"}
                number={
                  data.filter((item: any) => item.role === "STAFF").length
                }
                icon={"/staff.svg"}
              />
            </div>
          </div>
        </div>

        <div className="w-full mr-5">
          <Tabs
            className="mt-7"
            defaultIndex={0}
            onSelect={(index) => {
              setTab(index);
              clearSearchValue();
            }}
          >
            <TabList className="flex justify-between">
              <div className="flex">
                <Tab
                  onClick={() => {
                    setCurrentPage(1);
                  }}
                >
                  Students
                </Tab>
                <Tab
                  onClick={() => {
                    setCurrentPage(1);
                  }}
                >
                  Admins
                </Tab>
                <Tab
                  onClick={() => {
                    setCurrentPage(1);
                  }}
                >
                  Staff
                </Tab>
              </div>
              <div className="flex items-center">
                <div className="flex items-center mr-2">
                  <AiOutlineSearch className="mx-1" />
                  <input
                    value={query}
                    type="text"
                    onChange={handleInputChange}
                    className="bg-transparent border-collapse outline-none text-grey-normal"
                    placeholder="Search by name, email"
                  />
                </div>
                <div className="flex items-center">
                  <label className="mr-1">Filter:</label>
                  <select
                    id="filter"
                    value={filter}
                    onChange={handleFilterChange}
                    className="border-collapse outline-none bg-white px-2 py-1 rounded-md shadow-sm"
                  >
                    <option value="">All</option>
                    <option value="inKrr">In Krr</option>
                    <option value="outsideKrr">Outside Krr</option>
                  </select>
                </div>

              </div>
            </TabList>

            <TabPanel>
              <div className="mt-8 first-letter:overflow-x-auto">
                <table className="w-full border-0 border-collapse table-auto">
                  <thead>
                    <tr className="text-sm font-normal text-gray-500">
                      <th className="px-4 py-2 text-left">No</th>
                      <th className="px-4 py-2 text-left">Username</th>
                      <th className="px-4 py-2 text-center">Gender</th>
                      <th className="px-4 py-2 text-center">Department</th>
                      <th className="px-4 py-2 text-center ">Batch</th>
                      <th className="px-4 py-2 text-left">Gmail</th>
                      <th className="px-4 py-2 text-left">Status</th>
                      <th className="px-4 py-2 text-left">Active</th>
                      <th className="px-4 py-2 text-center">Tickets</th>
                      <th className="px-4 py-2 text-left"></th>
                    </tr>
                  </thead>
                  <tbody className="text-green-950">
                    {filterUsers.filter((user: any) => user?.role === "STUDENT")

                      .filter((user: any) => {
                        if (filter === "inKrr" && !user.inKRR) return false;
                        if (filter === "outsideKrr" && user.inKRR) return false;
                        return true;
                      })
                      .slice((currentPage - 1) * rowsPerPage, currentPage * rowsPerPage)
                      .map((user: any, index: number) => {
                        const indexTable = (currentPage - 1) * rowsPerPage + index + 1;
                        const department =
                          user?.studentInfo?.batch?.department === "SOFTWAREENGINEERING"
                            ? "SE"
                            : user?.studentInfo?.batch?.department === "TOURISMANDMANAGEMENT"
                              ? "TM"
                              : user?.studentInfo?.batch?.department === "ARCHITECTURE"
                                ? "ARC"
                                : user?.studentInfo?.batch?.department;

                        const isFilteredInKrr = filter === "inKrr" && user.inKRR;
                        const isFilteredOutsideKrr = filter === "outsideKrr" && !user.inKRR;

                        if ((filter === "inKrr" && !user.inKRR) || (filter === "outsideKrr" && user.inKRR)) {
                          return null; // Skip the item if it doesn't match the filter criteria
                        }

                        return (
                          <tr
                            key={user.id}
                            className={user.enable ? "" : "text-gray-400 bg-opacity-60"}
                          >
                            {/* index */}
                            <td className="px-4 py-2">{indexTable}</td>
                            {/* username */}
                            <td
                              className="px-4 py-2"
                              onClick={() =>
                                handlePopupProfile(
                                  user.username,
                                  user.email,
                                  user?.studentInfo?.batch?.department,
                                  user?.studentInfo?.batch?.batchNum,
                                  user?.gender,
                                  user?.inKRR,
                                  user?.ticket?.remainTicket,
                                  user.role,
                                  true
                                )
                              }
                            >
                              {user.username}
                            </td>
                            {/* gender */}
                            <td className="py-2 text-center">
                              {user.gender === "MALE" ? "M" : user.gender === "FEMALE" ? "F" : ""}
                            </td>
                            {/* department */}
                            <td className="py-2 text-center">{department}</td>
                            {/* batch */}
                            <td className="px-4 py-2 text-center">
                              {user.studentInfo.batch.batchNum}
                            </td>
                            {/* email */}
                            <td
                              className="px-4 py-2"
                              onClick={() =>
                                handlePopupProfile(
                                  user.username,
                                  user.email,
                                  user?.studentInfo?.batch?.department,
                                  user?.studentInfo?.batch?.batchNum,
                                  user?.gender,
                                  user?.inKRR,
                                  user?.ticket?.remainTicket,
                                  user.role,
                                  true
                                )
                              }
                            >
                              {user.email}
                            </td>
                            {/* Status */}
                            <td className="px-4 py-2">{user.inKRR ? "In KRR" : "Out KRR"}</td>
                            {/* active */}
                            <td className="px-4 py-2">
                              <button
                                className={`relative inline-block w-10 h-6 rounded-full shadow-inner transition duration-300 ease-in-out ${user.enable ? "bg-green-800" : "bg-gray-200"
                                  }`}
                                onClick={() =>
                                  handleUpdateStatus(
                                    user.id,
                                    user.role,
                                    user.email,
                                    user.username,
                                    null,
                                    user.studentInfo.batch.department,
                                    user.phone,
                                    user.studentInfo.batch.batchNum,
                                    user.inKRR,
                                    user.gender,
                                    toggleStatus(user.enable)
                                  )
                                }
                              >
                                <span
                                  className={`absolute inset-0 w-6 h-6 flex items-center justify-center ${user.enable ? "ml-4" : ""
                                    }`}
                                >
                                  <span
                                    className={`w-5 h-5 rounded-full shadow-md ${user.enable ? "bg-gray-300" : "bg-white"
                                      }`}
                                  ></span>
                                </span>
                              </button>
                            </td>
                            {/* ticket */}
                            <td className="px-4 py-2 text-center">
                              {user.ticket.remainTicket}/36
                            </td>
                            {/* edit button */}
                            <td className="flex flex-row">
                              <UserModel
                                isEdit={true}
                                iconOnly={true}
                                user_id={user.id}
                                user_username={user.username}
                                user_email={user.email}
                                user_gender={user.gender}
                                user_role={user.role}
                                user_department={user.studentInfo.batch.department}
                                user_batchNum={user.studentInfo.batch.batchNum}
                                userEnable={user.enable}
                                isCreate={true}
                                isImport={true}
                                isIcon={true}
                                isTitle={false}
                                className={``}
                              />

                              <DeleteUserModel item={user.username} id={user.id} className="" />
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>


                </table>
              </div>
              {filterUsers.filter((user: any) => user?.role === "STUDENT")
                .length == 0 ? (
                <div className="flex flex-col items-center justify-center ">
                  <MdSearchOff className="w-24 h-24" />
                  <h1 className="text-2xl">No data</h1>
                </div>
              ) : (
                <div className="flex justify-end mt-4">
                  <div className="flex items-center">
                    <span className="mr-2">Rows per page:</span>
                    <select
                      value={rowsPerPage}
                      onChange={(e) => {
                        if (
                          Number(e.target.value) * currentPage >
                          filterUsers.length
                        ) {
                          setRowsPerPage(Number(e.target.value));
                          setCurrentPage(1);
                        } else {
                          setRowsPerPage(Number(e.target.value));
                        }
                      }}
                      className="px-2 py-1 border border-gray-300 rounded-md"
                    >
                      <option value={5}>5</option>
                      <option value={10}>10</option>
                      <option value={15}>15</option>
                      <option value={25}>25</option>
                      <option value={50}>50</option>
                      <option value={100}>100</option>
                      <option value={200}>200</option>
                    </select>
                  </div>
                  <div className="flex items-center ml-4">
                    <span className="mr-2">Page:</span>
                    {
                      <button
                        key={">"}
                        onClick={() => handlePrev()}
                        className={`border border-gray-300  rounded-md px-5 py-1 ml-1 focus:outline-none ${currentPage == 1 ? "bg-gray-200" : ""
                          }`}
                      >
                        {"<"}
                      </button>
                    }
                    {Array.from(
                      {
                        length: Math.ceil(
                          filterUsers
                            .filter((user: any) => user.role === "STUDENT" && (
                              (filter === "" && user.inKRR) ||
                              (filter === "inKrr" && user.inKRR) ||
                              (filter === "outsideKrr" && !user.inKRR)
                            ))
                            .length / rowsPerPage
                        ),
                      },
                      (_, i) => i + 1
                    ).map((page) => (
                      <button
                        key={page}
                        onClick={() => handlePageChange(page)}
                        className={`border border-gray-300 rounded-md px-2 py-1 ml-1 focus:outline-none ${currentPage === page ? "bg-gray-300" : ""
                          }`}
                      >
                        {page}
                      </button>
                    ))}
                    {
                      <button
                        key={">"}
                        onClick={() => handleNext()}
                        className={`border border-gray-300 rounded-md px-5 py-1 ml-1 focus:outline-none ${currentPage === Math.ceil(
                          filterUsers
                            .filter((user: any) => user.role === "STUDENT" && (
                              (filter === "" && user.inKRR) ||
                              (filter === "inKrr" && user.inKRR) ||
                              (filter === "outsideKrr" && !user.inKRR)
                            ))
                            .length / rowsPerPage
                        )
                          ? "bg-gray-200"
                          : ""
                          }`}
                      >
                        {">"}
                      </button>
                    }

                  </div>
                </div>
              )}
            </TabPanel>
            <TabPanel>
              <div className="mt-8 overflow-x-auto">
                <table className="w-full border-0 border-collapse table-auto">
                  <thead>
                    <tr className="text-sm font-normal text-gray-500">
                      <th className="px-4 py-2 text-left">No</th>
                      <th className="py-2 text-left ">Username</th>
                      <th className="py-2 pr-20 text-center">Gender</th>
                      <th className="px-4 py-2 text-left">Gmail</th>
                      <th className="px-4 py-2 text-left">Status</th>
                      <th className="px-4 py-2 text-left">Active</th>
                      <th className="px-4 py-2 text-left"></th>
                    </tr>
                  </thead>
                  <tbody className="text-green-950">
                    {filterUsers
                      .filter((user: any) => user?.role === "ADMIN")
                      .filter((user: any) => {
                        if (filter === "inKrr" && !user.inKRR) return false;
                        if (filter === "outsideKrr" && user.inKRR) return false;
                        return true;
                      })
                      .slice((currentPage - 1) * rowsPerPage, currentPage * rowsPerPage) // Slice the data based on the current page and rows per page

                      .map((user: any, index: any) => (

                        <tr
                          key={index}
                          className={
                            user.enable ? "" : "text-gray-400 bg-opacity-60"
                          }
                        >
                          <td className="px-4 py-2">
                            {(currentPage - 1) * rowsPerPage + index + 1}
                          </td>
                          <td
                            className="py-2 pr-4"
                            onClick={() =>
                              handlePopupProfile(
                                user.username,
                                user.email,
                                "",
                                0,
                                user?.gender,
                                user?.inKRR,
                                user?.ticket?.remainTicket,
                                user.role,
                                true
                              )
                            }
                          >
                            {user.username}
                          </td>
                          <td className="py-2 pr-20 text-center">
                            {user.gender == "MALE"
                              ? "M"
                              : "" || user.gender == "FEMALE"
                                ? "F"
                                : ""}
                          </td>
                          <td
                            className="px-4 py-2"
                            onClick={() =>
                              handlePopupProfile(
                                user.username,
                                user.email,
                                "",
                                0,
                                user?.gender,
                                user?.inKRR,
                                user?.ticket?.remainTicket,
                                user.role,
                                true
                              )
                            }
                          >
                            {user.email}
                          </td>
                          <td className="px-4 py-2">
                            {user.inKRR ? "IN KRR" : "OUT KRR"}
                          </td>
                          <td className="px-4 py-2">
                            {/* {bus.enable ? "Yes" : "No"} */}
                            <button
                              className={`relative inline-block w-10 h-6 rounded-full shadow-inner transition duration-300 ease-in-out ${user.enable ? "bg-green-800" : "bg-gray-200"
                                }`}
                              onClick={() =>
                                handleUpdateStatus(
                                  user.id,
                                  user.role,
                                  user.email,
                                  user.username,
                                  null,
                                  user.studentInfo,
                                  user.phone,
                                  user.studentInfo,
                                  user.inKRR,
                                  user.gender,
                                  toggleStatus(user.enable)
                                )
                              }
                            >
                              <span
                                className={`absolute inset-0 w-6 h-6 flex items-center justify-center ${user.enable ? "ml-4" : ""
                                  }`}
                              >
                                <span
                                  className={`w-5 h-5 rounded-full shadow-md ${user.enable ? "bg-gray-300" : "bg-white"
                                    }`}
                                ></span>
                              </span>
                            </button>
                          </td>

                          <td className="flex flex-row">
                            {loggedInUserRole !== "SUPERADMIN" ? null : (
                              <>
                                <UserModel
                                  isEdit={true}
                                  iconOnly={true}
                                  user_id={user.id}
                                  user_username={user.username}
                                  user_email={user.email}
                                  user_gender={user.gender}
                                  user_role={user.role}
                                  user_department={null}
                                  user_batchNum={null}
                                  userEnable={user.enable}
                                  isCreate={true}
                                  isImport={true}
                                  isIcon={true}
                                  isTitle={false}
                                  className={``}
                                />
                                <DeleteUserModel item={user.username} id={user.id} />
                              </>
                            )}
                          </td>

                        </tr>
                      ))}
                  </tbody>
                </table>
              </div>
              {filterUsers.filter((user: any) => user?.role === "ADMIN")
                .length == 0 ? (
                <div className="flex flex-col items-center justify-center ">
                  <MdSearchOff className="w-24 h-24" />
                  <h1 className="text-2xl">No data</h1>
                </div>
              ) : (
                <div className="flex justify-end mt-4">
                  <div className="flex items-center">
                    <span className="mr-2">Rows per page:</span>
                    <select
                      value={rowsPerPage}
                      onChange={(e) => {
                        if (
                          Number(e.target.value) * currentPage >
                          filterUsers.length
                        ) {
                          setRowsPerPage(Number(e.target.value));
                          setCurrentPage(1);
                        } else {
                          setRowsPerPage(Number(e.target.value));
                        }
                      }}
                      className="px-2 py-1 border border-gray-300 rounded-md"
                    >
                      <option value={5}>5</option>
                      <option value={10}>10</option>
                      <option value={15}>15</option>
                      <option value={25}>25</option>
                      <option value={50}>50</option>
                      <option value={100}>100</option>
                      <option value={200}>200</option>
                    </select>
                  </div>
                  <div className="flex items-center ml-4">
                    <span className="mr-2">Page:</span>
                    {
                      <button
                        key={">"}
                        onClick={() => handlePrev()}
                        className={`border border-gray-300  rounded-md px-5 py-1 ml-1 focus:outline-none ${currentPage == 1 ? "bg-gray-200" : ""
                          }`}
                      >
                        {"<"}
                      </button>
                    }
                    {Array.from(
                      {
                        length: Math.ceil(
                          filterUsers.filter(
                            (user: any) => user.role === "ADMIN"
                          ).length / rowsPerPage
                        ),
                      },
                      (_, i) => i + 1
                    ).map((page) => (
                      <button
                        key={page}
                        onClick={() => handlePageChange(page)}
                        className={`border border-gray-300 rounded-md px-2 py-1 ml-1 focus:outline-none ${currentPage === page ? "bg-gray-300" : ""
                          }`}
                      >
                        {page}
                      </button>
                    ))}
                    {
                      <button
                        key={">"}
                        onClick={() => handleNext()}
                        className={`border border-gray-300 rounded-md px-5 py-1 ml-1 focus:outline-none ${currentPage == Math.ceil(data.length / rowsPerPage)
                          ? "bg-gray-200"
                          : ""
                          }`}
                      >
                        {">"}
                      </button>
                    }
                  </div>
                </div>
              )}
            </TabPanel>
            <TabPanel>
              <div className="mt-8 overflow-x-auto">
                <table className="w-full border-0 border-collapse table-auto">
                  <thead>
                    <tr className="text-sm font-normal text-gray-500">
                      <th className="px-4 py-2 text-left">No</th>
                      <th className="px-4 py-2 text-left">Username</th>
                      <th className="py-2 pr-20 text-center">Gender</th>
                      <th className="px-4 py-2 text-left">Gmail</th>
                      <th className="px-4 py-2 text-left">Status</th>
                      <th className="px-4 py-2 text-left">Active</th>
                      <th className="px-4 py-2 text-center">Tickets</th>
                      <th className="px-4 py-2 text-left"></th>
                    </tr>
                  </thead>
                  <tbody className="text-green-950">
                    {filterUsers.filter((user: any) => user?.role === "STAFF")
                      .length == 0
                      ? null
                      : filterUsers.filter(
                        (user: any) => user?.role === "STAFF"
                      ).length == 0
                        ? null
                        : filterUsers
                          .filter((user: any) => user.role === "STAFF").filter((user: any) => {
                            if (filter === "inKrr" && !user.inKRR) return false;
                            if (filter === "outsideKrr" && user.inKRR) return false;
                            return true;
                          })
                          .slice(
                            (currentPage - 1) * rowsPerPage,
                            currentPage * rowsPerPage
                          )
                          .map((user: any, index: any) => (
                            <tr
                              key={index}
                              className={
                                user.enable ? "" : "text-gray-400 bg-opacity-60"
                              }
                            >
                              <td className="px-4 py-2">
                                {(currentPage - 1) * rowsPerPage + index + 1}
                              </td>
                              <td
                                className="px-4 py-2"
                                onClick={() =>
                                  handlePopupProfile(
                                    user.username,
                                    user.email,
                                    "",
                                    0,
                                    user?.gender,
                                    user?.inKRR === true
                                      ? "In Kirirom"
                                      : "Out of Kirirom",
                                    user?.ticket?.remainTicket,
                                    user.role,
                                    true
                                  )
                                }
                              >
                                {user.username}
                              </td>
                              <td className="py-2 pr-20 text-center">
                                {" "}
                                {user.gender == "MALE"
                                  ? "M"
                                  : "" || user.gender == "FEMALE"
                                    ? "F"
                                    : ""}
                              </td>
                              <td
                                className="px-4 py-2"
                                onClick={() =>
                                  handlePopupProfile(
                                    user.username,
                                    user.email,
                                    "",
                                    0,
                                    user?.gender,
                                    user?.inKRR === true
                                      ? "In Kirirom"
                                      : "Out of Kirirom",
                                    user?.ticket?.remainTicket,
                                    user.role,
                                    true
                                  )
                                }
                              >
                                {user.email}
                              </td>
                              <td className="px-4 py-2">
                                {user.inKRR ? "IN KRR" : "OUT KRR"}
                              </td>
                              <td className="px-4 py-2">
                                <button
                                  className={`relative inline-block w-10 h-6 rounded-full shadow-inner transition duration-300 ease-in-out ${user.enable ? "bg-green-800" : "bg-gray-200"
                                    }`}
                                  onClick={() =>
                                    handleUpdateStatus(
                                      user.id,
                                      user.role,
                                      user.email,
                                      user.username,
                                      null,
                                      user.studentInfo,
                                      user.phone,
                                      user.studentInfo,
                                      user.inKRR,
                                      user.gender,
                                      toggleStatus(user.enable)
                                    )
                                  }
                                >
                                  <span
                                    className={`absolute inset-0 w-6 h-6 flex items-center justify-center ${user.enable ? "ml-4" : ""
                                      }`}
                                  >
                                    <span
                                      className={`w-5 h-5 rounded-full shadow-md ${user.enable ? "bg-gray-300" : "bg-white"
                                        }`}
                                    ></span>
                                  </span>
                                </button>
                              </td>
                              <td className="px-4 py-2 text-center">
                                {user.ticket.remainTicket}/36
                              </td>
                              <td className="flex flex-row">
                                <button className="bg-transparent">
                                  <UserModel
                                    isEdit={true}
                                    iconOnly={true}
                                    user_id={user.id}
                                    user_username={user.username}
                                    user_email={user.email}
                                    // user_password={user.password}
                                    user_gender={user.gender}
                                    user_role={user.role}
                                    user_department={null}
                                    user_batchNum={null}
                                    userEnable={user.enable}
                                    isCreate={true}
                                    isImport={true}
                                    isIcon={true}
                                    isTitle={false}
                                    className={``}
                                  />
                                </button>
                                <DeleteUserModel
                                  item={user.username}
                                  id={user.id}
                                />
                              </td>
                            </tr>
                          ))}
                  </tbody>
                </table>
              </div>
              {filterUsers.filter((user: any) => user?.role === "STAFF")
                .length == 0 ? (
                <div className="flex flex-col items-center justify-center ">
                  <MdSearchOff className="w-24 h-24" />
                  <h1 className="text-2xl">No data</h1>
                </div>
              ) : (
                <div className="flex justify-end mt-4">
                  <div className="flex items-center">
                    <span className="mr-2">Rows per page:</span>
                    <select
                      value={rowsPerPage}
                      onChange={(e) => {
                        if (
                          Number(e.target.value) * currentPage >
                          filterUsers.length
                        ) {
                          setRowsPerPage(Number(e.target.value));
                          setCurrentPage(1);
                        } else {
                          setRowsPerPage(Number(e.target.value));
                        }
                      }}
                      className="px-2 py-1 border border-gray-300 rounded-md"
                    >
                      <option value={5}>5</option>
                      <option value={10}>10</option>
                      <option value={15}>15</option>
                      <option value={25}>25</option>
                      <option value={50}>50</option>
                      <option value={100}>100</option>
                      <option value={200}>200</option>
                    </select>
                  </div>
                  <div className="flex items-center ml-4">
                    <span className="mr-2">Page:</span>
                    {
                      <button
                        key={">"}
                        onClick={() => handlePrev()}
                        className={`border border-gray-300  rounded-md px-5 py-1 ml-1 focus:outline-none ${currentPage == 1 ? "bg-gray-200" : ""
                          }`}
                      >
                        {"<"}
                      </button>
                    }
                    {Array.from(
                      {
                        length: Math.ceil(
                          filterUsers
                            .filter((user: any) => user.role === "STAFF" && (
                              (filter === "" && user.inKRR) ||
                              (filter === "inKrr" && user.inKRR) ||
                              (filter === "outsideKrr" && !user.inKRR)
                            ))
                            .length / rowsPerPage
                        ),
                      },
                      (_, i) => i + 1
                    ).map((page) => (
                      <button
                        key={page}
                        onClick={() => handlePageChange(page)}
                        className={`border border-gray-300 rounded-md px-2 py-1 ml-1 focus:outline-none ${currentPage === page ? "bg-gray-300" : ""
                          }`}
                      >
                        {page}
                      </button>
                    ))}
                    {
                      <button
                        key={">"}
                        onClick={() => handleNext()}
                        className={`border border-gray-300 rounded-md px-5 py-1 ml-1 focus:outline-none ${currentPage === Math.ceil(
                          filterUsers
                            .filter((user: any) => user.role === "STAFF" && (
                              (filter === "" && user.inKRR) ||
                              (filter === "inKrr" && user.inKRR) ||
                              (filter === "outsideKrr" && !user.inKRR)
                            ))
                            .length / rowsPerPage
                        )
                          ? "bg-gray-200"
                          : ""
                          }`}
                      >
                        {">"}
                      </button>
                    }
                  </div>
                </div>
              )}
            </TabPanel>
          </Tabs>
        </div>
      </div>
    </div>
  );
};

export default withAuth(UserManagement);
