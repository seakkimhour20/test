import { catch_token_error } from "@/helper/catch_token_error";

export const getAllUserByDepartment = async (department: any) => {
  const res = await fetch(`${process.env.base}/dashboard/${department}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  catch_token_error(data.error);
  return data;
};

export const getAllUserByDepartmentAndBatch = async (department: string, batchNum: number) => {
  const res = await fetch(`${process.env.base}/dashboard/${department}/${batchNum}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  catch_token_error(data.error);

  return data;
};
