import { catch_token_error } from "@/helper/catch_token_error";

export const getAllBus = async () => {
  const res = await fetch(`${process.env.base}/bus`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
  });
  const data = await res.json();
  catch_token_error(data.error);
  return data;
};

export const createBus = async (busData: any) => {
  const res = await fetch(`${process.env.base}/bus`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
    body: JSON.stringify(busData),
  });
  return res;
};

export const deleteBus = async (id: any) => {
  const res = await fetch(`${process.env.base}/bus/${id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
  });
  return res;
};

export const getBus = async (id: string) => {
  const res = await fetch(`${process.env.base}/bus/${id}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
  });
  if (res.status !== 200) {
    console.error("Failed to get the bus.");
  }
  const response = await res.json();
  return response
};

export const updateBus = async (
  bus_id: string,
  bus_model: string,
  bus_plateNumber: string,
  bus_numOfSeat: number,
  bus_driverName: string,
  bus_driverContact: string,
  bus_enable: boolean | undefined
) => {
  const res = await fetch(`${process.env.base}/bus/${bus_id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
    body: JSON.stringify({
      model: bus_model,
      plateNumber: bus_plateNumber,
      numOfSeat: Number(bus_numOfSeat),
      driverName: bus_driverName,
      driverContact: bus_driverContact,
      enable: bus_enable,
    }),
  });

  return res;
};
