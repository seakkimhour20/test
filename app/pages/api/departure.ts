import { catch_token_error } from "@/helper/catch_token_error";

export const getAllDeparture = async () => {
  const res = await fetch(`${process.env.base}/departure`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
  });
  const response = await res.json();
  catch_token_error(response.error);
  return response
};

export const getDeparture = async (id: string) => {
  const res = await fetch(`${process.env.base}/departure/${id}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
  });
  if (res.status !== 200) {
    console.error("Failed to get the bus.");
  }
  const response = await res.json();
  catch_token_error(response.error);
  return response
};