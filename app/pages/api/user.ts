import { catch_token_error } from "@/helper/catch_token_error";

export const getAllUser = async () => {
  const res = await fetch(`${process.env.base}/user`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
  });
  const data = await res.json();
  catch_token_error(data.error);
  return data;
};

export const getOneUser = async (id: string) => {
  const res = await fetch(`${process.env.base}/user/${id}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
  });
  const response = await res.json();
  catch_token_error(response.error);
  return res;
}

export const addNewUser = async (userData: any) => {
  const res = await fetch(`${process.env.base}/user`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
    body: JSON.stringify(userData),
  });
  return res;
};

export const deleteUser = async (id: any) => {
  const res = await fetch(`${process.env.base}/user/${id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
  });
  return res;
};

export const updateUser = async (
  user_id: any,
  role: string,
  email: string,
  username: string,
  password: string | null,
  department: string,
  phone: string,
  batchNum: number,
  inKrr: boolean,
  gender: string,
  enable: boolean,
) => {

  const res = await fetch(`${process.env.base}/user/${user_id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
    body: JSON.stringify({
      role: role,
      email: email,
      username: username,
      password: password || null,
      department: department || null,
      phone: phone || null,
      batchNum: Number(batchNum) || null,
      inKrr: inKrr,
      gender: gender,
      enable: enable,
    }),

  });
  return res;
};


