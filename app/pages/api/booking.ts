import { SwapBookingDto } from "@/types/booking";

export const swapBooking = async (scheduleId: string, swapDto: SwapBookingDto) => {
  const res = await fetch(`${process.env.base}/booking/swap/${scheduleId}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify(swapDto),
  });
  
  return res
};


export const exportBooking = async (scheduleId: string) => {
  const res = await fetch(`${process.env.base}/booking/exportBooking/schedule?id=${scheduleId}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  if (res.status !== 200) {
    console.error("Failed to export booking.");
  }
  if (res.status == 200 || res.status == 201) {
    console.error("Export success.");
  }
  window.location.reload();
  return res;
};
