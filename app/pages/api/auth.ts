import { LoginInput } from "@/types";

export const logIn = async (loginBody: LoginInput) => {
  const res = await fetch(`${process.env.base}/user/login/admin`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(loginBody),
  });

  return res;
};
export const forgetPassword = async (email: string) => {
  const endpoint = `${process.env.base}/user/request/reset-password/admin`;
  const res = await fetch(endpoint, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ email: email })
  });

  return res;
};


export const logOut = async () => {
  localStorage.removeItem("token");
}



