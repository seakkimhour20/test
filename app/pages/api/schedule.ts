import { catch_token_error } from "@/helper/catch_token_error";
import { CreateScheduleDto, UpdateScheduleDto } from "@/types/schedule";


export const getAllSchedule = async () => {
  const res = await fetch(`${process.env.base}/schedule`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
  });
  const data = await res.json();
  catch_token_error(data.error);

  return data;
};

export const addNewSchedule = async (schedule: CreateScheduleDto) => {
  const res = await fetch(`${process.env.base}/schedule`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
    body: JSON.stringify(schedule),
  })
  return res
}


export const updateSchedule = async (scheduleId: string,
  newSchedule: UpdateScheduleDto
) => {
  const res = await fetch(`${process.env.base}/schedule/${scheduleId}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
    body: JSON.stringify(newSchedule),
  });
  return res
};


export const getSchedule = async (id: string) => {
  const res = await fetch(`${process.env.base}/schedule/${id}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
  });
  if (res.status !== 200) {
    console.error("Failed to get the schedule.");
  }
  const response = await res.json();
  catch_token_error(response.error);
  return response
};



export const importSchedule = async (file: File) => {
  let formData = new FormData();
  formData.append("file", file);
  const res = await fetch(`${process.env.base}/schedule/import`, {
    method: "POST",
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
    body: formData,
  });


  if (res.status === 200 || res.status === 201) {
    window.location.reload();
    const data = await res.json();
    return data;
  } else {
    return
  };
}


export const confirmBooking = async (scheduleId: string, status: boolean) => {
  const res = await fetch(`${process.env.base}/schedule/confirm/${scheduleId}?confirm=${status}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
  });
  return res

}

export const deleteSchedule = async (scheduleId: string) => {
  const res = await fetch(`${process.env.base}/schedule/${scheduleId}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
  });
  return res
}
