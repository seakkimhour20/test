import { catch_token_error } from "@/helper/catch_token_error";

export const getAllBatch = async () => {
  const res = await fetch(`${process.env.base}/batch`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
  },);
  const data = await res.json();
  catch_token_error(data.error);
  return data;
};

export const getBatchByDepartment = async (department: string) => {
  const res = await fetch(`${process.env.base}/batch/department/${department}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
  },);
  const data = await res.json();
  catch_token_error(data.error);
  return data;
}