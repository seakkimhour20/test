import { catch_token_error } from '@/helper/catch_token_error';
import { cache } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export async function delete_sublocation(id: string) {
  const res = await fetch(`${process.env.base}/subLocation/${id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
  });

  return res
}

export const get_all = cache(async () => {
  const data = await fetch(
    `${process.env.base}/mainlocation`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      next: { tags: ['collection'] }
    },)
  const response = await data.json();
  catch_token_error(response.error);

  return response;
})

export async function create_sublocation(sub_name: string, main_location_id: string) {
  const res = await fetch(`${process.env.base}/subLocation`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
    body: JSON.stringify({
      mainLocationId: main_location_id,
      subLocationName: sub_name,
    }),
  });

  return res
}

export async function edit_sublocation(
  sub_location_id: string,
  sub_name: string,
  main_location_id: string
) {
  const res = await fetch(
    `${process.env.base}/subLocation/${sub_location_id}`,
    {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify({
        mainLocationId: main_location_id,
        subLocationName: sub_name,
      }),
    }
  );

  return res

}


export function create_mainlocation(location: string) {
  const res = fetch(`${process.env.base}/mainLocation`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
    body: JSON.stringify({ mainLocationName: location }),
  });
  res.then((res) => {
    toast.success('Main Location created successfully!', {
      position: toast.POSITION.TOP_RIGHT,
      autoClose: 1000,
    });
    setTimeout(() => {
      window.location.reload();
    }, 1000);
  });
}
export function edit_mainlocation(location: string, id: string) {
  const res = fetch(`${process.env.base}/mainlocation/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
    body: JSON.stringify({ mainLocationName: location }),
  });
  res.then((res) => {
    toast.success('Main Location edited successfully!', {
      position: toast.POSITION.TOP_RIGHT,
      autoClose: 1000,
    });
    setTimeout(() => {
      window.location.reload();
    }, 1000);
  });
}