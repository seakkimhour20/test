"use client";
import { LocationModel } from "../(components)/location_model";
import { TableComponent } from "../(components)/table_location";
import withAuth from "../../services/withAuth";

const LocationManagement = () => {
  return (
    <div className="flex flex-col w-[100%] p-[50px] min-h-screen bg-light-normal">
      <div className="ml-[13%]">
        <div className="flex justify-between">
          <h4 className=" font-bold text-[36px] ">Location management</h4>
          <div className="flex justify-end">
            <LocationModel
              isEdit={false}
              isIcon={true}
              isTitle={true}
              isDisabled={false}
              className={` text-white-normal right-0 bg-primary-normal hover:bg-primary-hover`}
            />
          </div>
        </div>
        <TableComponent />
      </div>
    </div>
  );
};

export default withAuth(LocationManagement);
