"use client";
import React, { useEffect, useState } from "react";
import withAuth from "../services/withAuth";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { logIn } from "./pages/api/auth";
import Image from "next/image";
import ForgetPasswordModal from './(components)/ForgetPasswordModal';
import { ToastContainer } from "react-toastify";


const Login = () => {
  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token) {
      router.push("/schedule");
    }
  }, []);
  const [status, setStatus] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const router = useRouter();

  // for terminal
  const [showTerminal, setShowTerminal] = useState(false);
  const [description, setDiscription] = useState("");
  const [content, setContent] = useState("");

  const [type, setType] = useState("password");
  const [icon, setIcon] = useState("/hide_password.svg");

  // habdleSet vissible
  const handleToggle = () => {
    if (type === "password") {
      setIcon("/hide_password.svg");
      setType("text");
    } else {
      setIcon("/un_hide_password.svg");
      setType("password");
    }
  };


  const handleLogin = async () => {
    const resData = await logIn({ email, password });
    setEmail(email);
    setPassword(password);
    const data = await resData.json();
    if (resData.status == 200) {
      localStorage.setItem("token", data.data.accessToken);
      // setStatus(true);
      setIsAuthenticated(true);
      setShowTerminal(false);
      router.push("/schedule");
      return resData;
    } else if (resData.status == 401) {
      console.log("Response:", data.error);
      setIsAuthenticated(false);
      setShowTerminal(true);
      setContent("Error Logging In");
      setDiscription(`Error Login: ${data.error}`);
      return resData;
    } else if (resData.status == 400) {
      console.log("Response:", data.message);
      setIsAuthenticated(false);
      setShowTerminal(true);
      setContent("Error Logging In");
      setDiscription(`Error Login: ${data.message}`);
      return resData;
    } else {
      console.log("Response:", data.error);
      setIsAuthenticated(false);
      setShowTerminal(true);
      setContent("Invalide Credentails");
      setDiscription(`Error Login: ${data.error}`);
      return resData;
    }
  };

  return (
    <div className="flex flex-col items-center justify-center w-full h-full min-h-screen overflow-hidden bg-no-repeat bg-cover bg-my_bg_image">
      <div
        className="w-full p-6 rounded-md shadow-md lg:max-w-xl backdrop-blur-sm bg-white-normal/30 "
        onClick={() => {
          setShowTerminal(false);
        }}
      >
        <h1 className="text-3xl font-bold text-center text-gray-700">
          Login Your Account
        </h1>
        {!isAuthenticated && showTerminal ? (
          <div className="flex flex-col items-center justify-center w-full px-4 pt-8 pb-4">
            <div className="relative w-full max-w-lg p-4 mx-auto bg-black bg-red-500 rounded-md shadow-2xl backdrop-blur-sm text-white-normal">
              <div className="text-left">
                <p className="">{description}</p>
              </div>
            </div>
          </div>
        ) : // <ShowTerminal content={content} description={description} show={true} success={success} onClick={() => { setShowTerminal(false) }} />
        null}
        <div className="mb-4">
          <label
            htmlFor="email"
            className="block text-sm font-semibold text-gray-800"
          >
            Email
          </label>
          <input
            type="email"
            value={email}
            placeholder="example@gmail.com"
            onChange={(e) => setEmail(e.target.value)}
            className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
          />
        </div>
        <div className="mb-2">
          <label
            htmlFor="password"
            className="block text-sm font-semibold text-gray-800"
          >
            Password
          </label>
          <div className="flex flex-row mt-2">
            <input
              type={type}
              value={password}
              placeholder="password"
              onChange={(e) => setPassword(e.target.value)}
              className="block w-full px-4 py-2 text-gray-700 bg-white border rounded-md mt- focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
            />
            <span
              className="flex items-center justify-around "
              onClick={handleToggle}
            >
              <Image
                className="absolute flex items-center mr-10"
                src={icon}
                alt=""
                width={16}
                height={16}
              />
            </span>
            
          </div>
          <div className="flex mt-2 ">
            <ForgetPasswordModal />
          </div>
          
        </div>
  
        <div className="mt-3">
          <button
            onClick={handleLogin}
            className={
              !email || !password
                ? `w-full px-4 py-2 tracking-wide transition-colors duration-200 transform  rounded-md text-white-normal   bg-gray-500 `
                : `w-full px-4 py-2 tracking-wide transition-colors duration-200 transform bg-green-800 rounded-md text-white-normal hover:bg-green-900  `
            }
            disabled={!email || !password}
          >
            {status ? (
              <Link href="/schedule">Login</Link>
            ) : (
              <Link href="/">Login</Link>
            )}
          </button>
        </div>

      </div>
      <ToastContainer />
    </div>
    
  );
};

export default withAuth(Login);
