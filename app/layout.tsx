"use client";
import "./globals.css";
import Sidebar from "@/app/(components)/sideBar";
import { usePathname } from "next/navigation";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const currentRoute = usePathname();
  const showHeader = currentRoute === "/" ? false : true;

  return (
    <html lang="en">
      <body>
        <main>
          <div className="top-0 bottom-0 left-0 right-0 flex flex-row sm:flex-row">
            <div className="w-auto">{showHeader && <Sidebar />}</div>
            <div className="w-full">{children}</div>
          </div>
        </main>
      </body>
    </html>
  );
}
