"use client";
import * as React from "react";
import { FC } from "react";
import { SearchButton } from "@/app/(components)/search_botton";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { useEffect, useState } from "react";
import { confirmBooking, getSchedule } from "@/app/pages/api/schedule";

import {
  BookingResponse,
  CancelResponse,
  WaitingResponse,
} from "@/types/booking";
import { Card } from "@/app/(components)/card";
import SwapTicketModel from "@/app/(components)/swap_model";
import fileDownload from "js-file-download";
import Image from "next/image";
import Axios from "axios";
import { IconButton } from "@/app/(components)/Icon_botton";
import { MdSearchOff } from "react-icons/md";
import { SubmitButton } from "@/app/(components)/submit_botton";
import "react-tabs/style/react-tabs.css";
import { ScheduleResponseData } from "@/types/schedule";
import { formatTime } from "@/helper/format_time";

interface pageProps {
  params: {
    scheduleId: string;
  };
}

const ScheduleDetail: FC<pageProps> = ({ params }) => {
  const bookingUsers = [
    {
      id: "",
      value: " ",
    },
  ];
  const waitingUsers = [
    {
      id: "",
      value: " ",
    },
  ];

  const [booking, setBooking] = useState([]);
  const [waiting, setWaiting] = useState([]);
  const [cancel, setCancel] = useState([]);
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [isEnable, setIsEnable] = useState<boolean>(true);
  const [displayBooking, setDisplayBooking] = useState([]);
  const [displayWaiting, setDisplayWaiting] = useState([]);
  const [displayCancelling, setDisplayCanceling] = useState([]);

  const [currentPage, setCurrentPage] = useState(1);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  // Calculate pagination
  const indexOfLastRow = currentPage * rowsPerPage;
  const indexOfFirstRow = indexOfLastRow - rowsPerPage;
  const currentRowsBooking = displayBooking.slice(
    indexOfFirstRow,
    indexOfLastRow
  );
  const currentRowWaiting = displayWaiting.slice(
    indexOfFirstRow,
    indexOfLastRow
  );
  const currentRowCancel = displayCancelling.slice(
    indexOfFirstRow,
    indexOfLastRow
  );

  const [showModal, setShowModal] = useState(false);
  const changeTheme = () => {
    return setShowModal(!showModal);
  };
  const handlePageChange = (pageNumber: any) => {
    setCurrentPage(pageNumber);
  };

  useEffect(() => {
    const fetchData = async () => {
      const responseData = await getSchedule(params.scheduleId);
      setFrom(await responseData?.data?.departure?.from?.mainLocationName);
      setTo(await responseData?.data?.departure?.destination?.mainLocationName);
      setDate(
        await new Date(responseData?.data?.date).toLocaleDateString([], {
          day: "2-digit",
          month: "2-digit",
          year: "numeric",
        })
      );      
      setTime(formatTime( responseData?.data?.departure?.departureTime));
      setBooking(await responseData?.data?.booking);
      setWaiting(await responseData?.data?.Waitting);
      setCancel(await responseData?.data?.Cancel);
      setIsEnable(await responseData?.data?.enable);
      setDisplayBooking(await responseData?.data?.booking);
      setDisplayWaiting(await responseData?.data?.Waitting);
      setDisplayCanceling(await responseData?.data?.Cancel);
      
    };
    fetchData();
  }, [params.scheduleId]);

  const handleValidateSchedule = async () => {
    const res = await confirmBooking(params?.scheduleId, isEnable);
    window.location.reload();
  };

  booking.map((item: BookingResponse, index: number) => {
    bookingUsers.push({
      id: item.id,
      value: `${index + 1} ${item?.user?.username}`,
    });
  });

  waiting.map((item: WaitingResponse, index: number) => {
    waitingUsers.push({
      id: item.id,
      value: `${index + 1} ${item?.user?.username}`,
    });
  });

  const searchBooking = (search: string) => {
    const bookingResult = booking.filter((booking: any) => {
      return booking?.user?.username
        ?.toLowerCase()
        .includes(search?.toLowerCase());
    });
    const waitingResult = waiting.filter((booking: any) => {
      return booking?.user?.username
        ?.toLowerCase()
        .includes(search?.toLowerCase());
    });
    const cancelResult = cancel?.filter((booking: any) => {
      return booking?.user?.username
        ?.toLowerCase()
        .includes(search?.toLowerCase());
    });
    setDisplayBooking(bookingResult);
    setDisplayWaiting(waitingResult);
    setDisplayCanceling(cancelResult);
  };

  const handleExport = async (e: any) => {
    e.preventDefault();
    Axios({
      url: `${process.env.base}/booking/exportBooking/schedule?id=${params?.scheduleId}`,
      method: "GET",
      responseType: "blob", // important
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    }).then((response) => {
      const fileName = `ShuttleBus Request-${new Date(date)
        .toISOString()
        .substring(0, 10)} (${from}-to-${to}) ${params?.scheduleId}.xlsx`;
      fileDownload(response?.data, fileName);
    });
  };
  const handleNext = (tabName: string) => {
    if (
      tabName === "booking" &&
      currentPage < Math.ceil(booking.length / rowsPerPage)
    ) {
      setCurrentPage(currentPage + 1);
    } else if (
      tabName === "waiting" &&
      currentPage < Math.ceil(waiting.length / rowsPerPage)
    ) {
      setCurrentPage(currentPage + 1);
    } else if (
      tabName === "cancel" &&
      currentPage < Math.ceil(cancel.length / rowsPerPage)
    ) {
      setCurrentPage(currentPage + 1);
    }
  };
  const handlePrev = (tabName: string) => {
    if (tabName === "booking" && currentPage > 1) {
      setCurrentPage(currentPage - 1);
    } else if (tabName === "waiting" && currentPage > 1) {
      setCurrentPage(currentPage - 1);
    } else if (tabName === "cancel" && currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };
  return (
    <div className="ml-[13%]  h-full ">
      <div className="flex flex-col w-[100%] p-[50px] ">
        <div className="flex flex-col justify-between w-full align-middle">
          <div className="flex flex-row justify-between">
            <h4 className="font-bold text-[30px]">SCHEDULE DETAIL</h4>
            {/* <UserModel /> */}
            <div className="flex flex-row justify-between">
              <div>
                <SwapTicketModel
                  bookingUserOptions={bookingUsers}
                  waitingUserOptions={waitingUsers}
                  scheduleId={params?.scheduleId}
                />
              </div>
              <div className="ml-5">
                <IconButton
                  buttonWidth="w-40"
                  isDisabled={false}
                  title={`${isEnable ? "Validate" : "Unlock"}`}
                  onClick={() => handleValidateSchedule()}
                  className={`align-middle rounded-md cursor-pointer text-white-normal h-11 ${
                    isEnable
                      ? "bg-primary-normal hover:bg-primary-hover"
                      : "bg-gray-500 hover:bg-gray-600"
                  } `}
                  isIconOnly={true}
                  isTitleOnly={true}
                  img={!isEnable ? "/confirm.svg" : "/decline.svg"}
                  w="25"
                  h="25"
                />
              </div>
            </div>
          </div>
          <div className="flex flex-col items-center">
            <div className="flex flex-row mt-5 mr-28 space-x-7">
              <Card
                icon="/booking.svg"
                number={booking?.length}
                title="Booking"
              />
              <Card
                icon="/waiting.svg"
                number={waiting?.length}
                title="Waiting"
              />
              <Card icon="/cancel.svg" number={cancel?.length} title="Cancel" />
            </div>
          </div>
        </div>

        <div className="w-full mr-5 ">
          <Tabs className="mt-7">
            <TabList className="flex justify-between">
              <div>
                <Tab
                  onClick={() => {
                    setDisplayBooking(booking);
                    setCurrentPage(1);
                  }}
                >
                  BOOKING
                </Tab>
                <Tab
                  onClick={() => {
                    setDisplayWaiting(waiting);
                    setCurrentPage(1);
                  }}
                >
                  WAITING
                </Tab>
                <Tab
                  onClick={() => {
                    setDisplayCanceling(cancel);
                    setCurrentPage(1);
                  }}
                >
                  CANCEL
                </Tab>
              </div>
              <SearchButton
                searchFunction={searchBooking}
                placeholder={"Search by username and direction"}
              />
            </TabList>
            <TabPanel>
              {booking?.length == 0 ? (
                <div className="flex flex-col items-center justify-center ">
                  <MdSearchOff className="w-24 h-24" />
                  <h1 className="text-2xl">No data</h1>
                </div>
              ) : (
                <div>
                  <div className="mt-8 first-letter:overflow-x-auto">
                    <table className="w-full border-0 border-collapse table-auto">
                      <thead>
                        <tr className="text-sm font-normal text-gray-500">
                          <th className="px-4 py-2 text-left">No</th>
                          <th className="px-4 py-2 text-left">Username</th>
                          <th className="px-4 py-2 text-left">From</th>
                          <th className="px-4 py-2 text-left">To</th>
                          <th className="px-4 py-2 text-left">
                            Departure Date
                          </th>
                          <th className="px-4 py-2 text-left">
                            Departure Time
                          </th>
                          <th className="px-4 py-2 text-left">Ticket Status</th>
                        </tr>
                      </thead>
                      <tbody className="text-green-950">
                        {booking?.length === 0 ? (
                          <tr>
                            <td className="flex px-4 py-2 text-gray-400 item-center">
                              No Data
                            </td>
                          </tr>
                        ) : (
                          currentRowsBooking?.map(
                            (bookingD: BookingResponse, index: number) => (
                              // eslint-disable-next-line react/jsx-key
                              <tr>
                                <td className="px-4 py-2">{index + 1}</td>
                                <td className="px-4 py-2" key={index}>
                                  {bookingD?.user?.username}
                                </td>
                                <td className="px-4 py-2">{from}</td>
                                <td className="px-4 py-2">{to}</td>
                                <td className="px-4 py-2">{date}</td>
                                <td className="px-4 py-2">
                                  {time}
                                </td>
                                <td className="px-4 py-2">Booking</td>
                              </tr>
                            )
                          )
                        )}
                      </tbody>
                    </table>
                  </div>
                  <div className="flex justify-end mt-4">
                    <div className="flex items-center">
                      <span className="mr-2">Rows per page:</span>
                      <select
                        value={rowsPerPage}
                        onChange={(e) => {
                          if (
                            Number(e.target.value) * currentPage >
                            booking.length
                          ) {
                            setCurrentPage(1);
                            setRowsPerPage(Number(e.target.value));
                          } else {
                            setRowsPerPage(Number(e.target.value));
                          }
                        }}
                        className="px-2 py-1 border border-gray-300 rounded-md"
                      >
                        <option value={5}>5</option>
                        <option value={10}>10</option>
                        <option value={15}>15</option>
                        <option value={25}>25</option>
                        <option value={50}>50</option>
                        <option value={100}>100</option>
                        <option value={200}>200</option>
                      </select>
                    </div>
                    <div className="flex items-center ml-4">
                      <span className="mr-2">Page:</span>
                      {
                        <button
                          key={">"}
                          onClick={() => handlePrev("booking")}
                          className={`border border-gray-300  rounded-md px-5 py-1 ml-1 focus:outline-none ${
                            currentPage == 1 ? "bg-gray-200" : ""
                          }`}
                        >
                          {"<"}
                        </button>
                      }
                      {Array.from(
                        { length: Math.ceil(booking.length / rowsPerPage) },
                        (_, i) => i + 1
                      ).map((page) => (
                        <button
                          key={page}
                          onClick={() => handlePageChange(page)}
                          className={`border border-gray-300 rounded-md px-2 py-1 ml-1 focus:outline-none ${
                            currentPage === page ? "bg-gray-300" : ""
                          }`}
                        >
                          {page}
                        </button>
                      ))}
                      {
                        <button
                          key={">"}
                          onClick={() => handleNext("booking")}
                          className={`border border-gray-300 rounded-md px-5 py-1 ml-1 focus:outline-none ${
                            currentPage ==
                            Math.ceil(booking.length / rowsPerPage)
                              ? "bg-gray-200"
                              : ""
                          }`}
                        >
                          {">"}
                        </button>
                      }
                    </div>
                  </div>
                </div>
              )}
            </TabPanel>
            <TabPanel>
              {waiting?.length == 0 ? (
                <div className="flex flex-col items-center justify-center ">
                  <MdSearchOff className="w-24 h-24" />
                  <h1 className="text-2xl">No data</h1>
                </div>
              ) : (
                <div>
                  <div className="mt-8 overflow-x-auto">
                    <table className="w-full border-0 border-collapse table-auto">
                      <thead>
                        <tr className="text-sm font-normal text-gray-500">
                          <th className="px-4 py-2 text-left">No</th>
                          <th className="px-4 py-2 text-left">Username</th>
                          <th className="px-4 py-2 text-left">From</th>
                          <th className="px-4 py-2 text-left">To</th>
                          <th className="px-4 py-2 text-left">
                            Departure Date
                          </th>
                          <th className="px-4 py-2 text-left">
                            Departure Time
                          </th>
                          <th className="px-4 py-2 text-left">Ticket Status</th>
                        </tr>
                      </thead>
                      <tbody className="text-green-950">
                        {currentRowWaiting?.length === 0 ? (
                          <tr>
                            <td className="flex px-4 py-2 text-gray-400 item-center">
                              No Data
                            </td>
                          </tr>
                        ) : (
                          currentRowWaiting?.map(
                            (waitingD: WaitingResponse, index: number) => (
                              // eslint-disable-next-line react/jsx-key
                              <tr>
                                <td className="px-4 py-2">{index + 1}</td>
                                <td className="px-4 py-2" key={index}>
                                  {waitingD?.user?.username}
                                </td>
                                <td className="px-4 py-2">{from}</td>
                                <td className="px-4 py-2">{to}</td>
                                <td className="px-4 py-2">{date}</td>
                                <td className="px-4 py-2">
                                  {time}
                                </td>
                                <td className="px-4 py-2">Waiting</td>
                              </tr>
                            )
                          )
                        )}
                      </tbody>
                    </table>
                  </div>
                  <div className="flex justify-end mt-4">
                    <div className="flex items-center">
                      <span className="mr-2">Rows per page:</span>
                      <select
                        value={rowsPerPage}
                        onChange={(e) => {
                          if (
                            Number(e.target.value) * currentPage >
                            waiting.length
                          ) {
                            setCurrentPage(1);
                            setRowsPerPage(Number(e.target.value));
                          } else {
                            setRowsPerPage(Number(e.target.value));
                          }
                        }}
                        className="px-2 py-1 border border-gray-300 rounded-md"
                      >
                        <option value={5}>5</option>
                        <option value={10}>10</option>
                        <option value={15}>15</option>
                        <option value={25}>25</option>
                        <option value={50}>50</option>
                        <option value={100}>100</option>
                        <option value={200}>200</option>
                      </select>
                    </div>
                    <div className="flex items-center ml-4">
                      <span className="mr-2">Page:</span>
                      {
                        <button
                          key={">"}
                          onClick={() => handlePrev("waiting")}
                          className={`border border-gray-300  rounded-md px-5 py-1 ml-1 focus:outline-none ${
                            currentPage == 1 ? "bg-gray-200" : ""
                          }`}
                        >
                          {"<"}
                        </button>
                      }
                      {Array.from(
                        { length: Math.ceil(waiting?.length / rowsPerPage) },
                        (_, i) => i + 1
                      ).map((page) => (
                        <button
                          key={page}
                          onClick={() => handlePageChange(page)}
                          className={`border border-gray-300 rounded-md px-2 py-1 ml-1 focus:outline-none ${
                            currentPage === page ? "bg-gray-300" : ""
                          }`}
                        >
                          {page}
                        </button>
                      ))}
                      {
                        <button
                          key={">"}
                          onClick={() => handleNext("waiting")}
                          className={`border border-gray-300 rounded-md px-5 py-1 ml-1 focus:outline-none ${
                            currentPage ==
                            Math.ceil(waiting.length / rowsPerPage)
                              ? "bg-gray-200"
                              : ""
                          }`}
                        >
                          {">"}
                        </button>
                      }
                    </div>
                  </div>
                </div>
              )}
            </TabPanel>
            <TabPanel>
              {cancel?.length == 0 ? (
                <div className="flex flex-col items-center justify-center ">
                  <MdSearchOff className="w-24 h-24" />
                  <h1 className="text-2xl">No data</h1>
                </div>
              ) : (
                <div>
                  <div className="mt-8 overflow-x-auto">
                    <table className="w-full border-0 border-collapse table-auto">
                      <thead>
                        <tr className="text-sm font-normal text-gray-500">
                          <th className="px-4 py-2 text-left">No</th>
                          <th className="px-4 py-2 text-left">Username</th>
                          <th className="px-4 py-2 text-left">From</th>
                          <th className="px-4 py-2 text-left">To</th>
                          <th className="px-4 py-2 text-left">
                            Departure Date
                          </th>
                          <th className="px-4 py-2 text-left">
                            Departure Time
                          </th>
                          <th className="px-4 py-2 text-left">Ticket Status</th>
                        </tr>
                      </thead>
                      <tbody className="text-green-950">
                        {currentRowCancel?.length === 0 ? (
                          <tr>
                            <td className="flex px-4 py-2 text-gray-400 item-center">
                              No Data
                            </td>
                          </tr>
                        ) : (
                          currentRowCancel?.map(
                            (cancelD: CancelResponse, index: number) => (
                              // eslint-disable-next-line react/jsx-key
                              <tr>
                                <td className="px-4 py-2">{index + 1}</td>
                                <td className="px-4 py-2" key={index}>
                                  {cancelD?.user?.username}
                                </td>
                                <td className="px-4 py-2">{from}</td>
                                <td className="px-4 py-2">{to}</td>
                                <td className="px-4 py-2">{date}</td>
                                <td className="px-4 py-2">{time}</td>
                                <td className="px-4 py-2">Cancel</td>
                              </tr>
                            )
                          )
                        )}
                      </tbody>
                    </table>
                  </div>
                  <div className="flex justify-end mt-4">
                    <div className="flex items-center">
                      <span className="mr-2">Rows per page:</span>
                      <select
                        value={rowsPerPage}
                        onChange={(e) => {
                          if (
                            Number(e.target.value) * currentPage >
                            cancel.length
                          ) {
                            setCurrentPage(1);
                            setRowsPerPage(Number(e.target.value));
                          } else {
                            setRowsPerPage(Number(e.target.value));
                          }
                        }}
                        className="px-2 py-1 border border-gray-300 rounded-md"
                      >
                        <option value={5}>5</option>
                        <option value={10}>10</option>
                        <option value={15}>15</option>
                        <option value={25}>25</option>
                        <option value={50}>50</option>
                        <option value={100}>100</option>
                        <option value={200}>200</option>
                      </select>
                    </div>
                    <div className="flex items-center ml-4">
                      <span className="mr-2">Page:</span>
                      {
                        <button
                          key={">"}
                          onClick={() => handlePrev("cancel")}
                          className={`border border-gray-300  rounded-md px-5 py-1 ml-1 focus:outline-none ${
                            currentPage == 1 ? "bg-gray-200" : ""
                          }`}
                        >
                          {"<"}
                        </button>
                      }
                      {Array.from(
                        { length: Math.ceil(cancel.length / rowsPerPage) },
                        (_, i) => i + 1
                      ).map((page) => (
                        <button
                          key={page}
                          onClick={() => handlePageChange(page)}
                          className={`border border-gray-300 rounded-md px-2 py-1 ml-1 focus:outline-none ${
                            currentPage === page ? "bg-gray-300" : ""
                          }`}
                        >
                          {page}
                        </button>
                      ))}
                      {
                        <button
                          key={">"}
                          onClick={() => handleNext("cancel")}
                          className={`border border-gray-300 rounded-md px-5 py-1 ml-1 focus:outline-none ${
                            currentPage ==
                            Math.ceil(cancel.length / rowsPerPage)
                              ? "bg-gray-200"
                              : ""
                          }`}
                        >
                          {">"}
                        </button>
                      }
                    </div>
                  </div>
                </div>
              )}
            </TabPanel>
          </Tabs>
        </div>
      </div>
      <div className="flex flex-col items-start justify-end my-5 ml-[4%]">
        {booking?.length !== 0 ? (
          <button
            disabled={booking?.length === 0}
            className={
              booking.length === 0
                ? ""
                : "px-4 py-2 font-medium text-green-950 bg-gray-300 hover:bg-gray-400 rounded inline-flex items-center"
            }
            onClick={(e) => {
              handleExport(e);
            }}
          >
            <div className="flex flex-row">
              <p className="">Export Report</p>
              <Image
                className="ml-2"
                src="/download.svg"
                alt=""
                width={16}
                height={16}
              />
            </div>
          </button>
        ) : null}
      </div>
    </div>
  );
};

export default ScheduleDetail;
