"use client";
import * as React from "react";
import { useEffect, useState } from "react";
import { getAllSchedule } from "../pages/api/schedule";
import { ScheduleResponseData } from "@/types/schedule";
import { ScheduleModel } from "../(components)/schedule_model";
import withAuth from "../../services/withAuth";
import Link from "next/link";
import Image from "next/image";
import DeleteScheduleModal from "../(components)/delete_schedule_model";
import { MdSearchOff } from "react-icons/md";
import { formatTime } from "@/helper/format_time";
const ScheduleManagement = () => {
  const [data, setData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [rowsPerPage, setRowsPerPage] = useState(100);

  // Calculate pagination
  const indexOfLastRow = currentPage * rowsPerPage;
  const indexOfFirstRow = indexOfLastRow - rowsPerPage;
  const currentRows = data.slice(indexOfFirstRow, indexOfLastRow);

  const handlePageChange = (pageNumber: any) => {
    setCurrentPage(pageNumber);
  };
  useEffect(() => {
    const fetchData = async () => {
      const responseData = await getAllSchedule();
      const data = responseData?.data;
      await data?.sort((a: any, b: any) =>
        b.date > a.date ? 1 : b.date < a.date ? -1 : 0
      );
      setData(data);
      // setData(await responseData.data);
    };
    fetchData();
  }, []);

  function formatDate(dateString: string): string {
    const date = new Date(dateString);

    const day = date.getDate();
    const month = date.toLocaleString("default", { month: "short" });
    const year = date.getFullYear();
    return `${day}/${month}/${year}`;
  }

  const handleNext = () => {
    if (currentPage < Math.ceil(data.length / rowsPerPage)) {
      setCurrentPage(currentPage + 1);
    }
  };
  const handlePrev = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };
  return (
    <div className="flex flex-col w-full p-[50px] min-h-screen bg-light-normal">
      <div className="ml-[13%]">
        <div className="flex flex-row items-center justify-between w-full align-middle">
          <h4 className="font-bold text-[36px]">SCHEDULE</h4>
          <div className="flex flex-row space-x-4">
            <ScheduleModel
              isCreate={true}
              isImport={true}
              isEdit={false}
              isIcon={true}
              isTitle={true}
              isDisabled={false}
              className={`text-white-normal h-11 bg-primary-normal hover:bg-primary-hover`}
            />
            <ScheduleModel
              isCreate={true}
              isImport={false}
              isEdit={false}
              isIcon={true}
              isTitle={true}
              isDisabled={false}
              className={`text-white-normal h-11 bg-primary-normal hover:bg-primary-hover`}
            />
          </div>
        </div>
        {data.length == 0 || !data ? (
          <div className="flex flex-col items-center justify-center ">
            <MdSearchOff className="w-24 h-24" />
            <h1 className="text-2xl">No data</h1>
          </div>
        ) : (
          <div className="w-full mt-16">
            <div className="m-20 overflow-x-auto">
              {/* <TableSchedule columnList={columns} rowList={rows} /> */}
              <table className="w-full border-0 border-collapse table-auto">
                <thead>
                  <tr className={`text-sm font-normal text-gray-500`}>
                    <th className="px-4 py-2 text-left">No</th>
                    <th className="px-4 py-2 text-left">Driver Name</th>
                    <th className="px-4 py-2 text-left">Driver Number</th>
                    <th className="px-4 py-2 text-left">From</th>
                    <th className="px-4 py-2 text-left">To</th>
                    <th className="px-4 py-2 text-left">Departure Date</th>
                    <th className="px-4 py-2 text-left">Departure Time</th>
                    <th className="px-4 py-2 text-left">Seat</th>
                    <th className="px-4 py-2 text-left">Booked</th>
                    <th className="px-4 py-2 text-left">Waiting</th>
                    <th className="px-4 py-2 text-left">Operation</th>
                    <th className="px-4 py-2 text-left"></th>
                  </tr>
                </thead>
                <tbody className={``}>
                  {/* Loop over the scheduleData array and generate a table row for each object */}
                  {currentRows?.map(
                    (scheduleDt: ScheduleResponseData, index: number) => (
                      <tr
                        key={index}
                        className={
                          scheduleDt.enable ? "text-green-950" : "text-gray-400"
                        }
                      >
                        <td className="px-4 py-2 text-green-950">
                          <Link href={`/schedule/${scheduleDt?.id}`}>
                            {(currentPage - 1) * rowsPerPage + index + 1}
                          </Link>
                        </td>
                        <td className="px-4 py-2">
                          <Link href={`/schedule/${scheduleDt?.id}`}>
                            {" "}
                            {scheduleDt?.bus === null
                              ? "N/A"
                              : scheduleDt?.bus?.driverName}
                          </Link>
                        </td>
                        <td className="px-4 py-2">
                          <Link href={`/schedule/${scheduleDt?.id}`}>
                            {" "}
                            {scheduleDt?.bus === null
                              ? "N/A"
                              : scheduleDt?.bus?.driverContact}
                          </Link>
                        </td>
                        <td className="px-4 py-2">
                          <Link href={`/schedule/${scheduleDt?.id}`}>
                            {" "}
                            {scheduleDt?.departure?.from?.mainLocationName}
                          </Link>
                        </td>
                        <td className="px-4 py-2">
                          <Link href={`/schedule/${scheduleDt?.id}`}>
                            {
                              scheduleDt?.departure?.destination
                                ?.mainLocationName
                            }
                          </Link>
                        </td>
                        <td className="px-4 py-2">
                          <Link href={`/schedule/${scheduleDt?.id}`}>
                            {formatDate(scheduleDt?.date?.toLocaleString())}
                          </Link>
                        </td>
                        <td className="px-4 py-2">
                          <Link href={`/schedule/${scheduleDt?.id}`}>
                            {formatTime(scheduleDt.departure.departureTime)}
                          </Link>
                        </td>
                        <td className="px-4 py-2 text-center">
                          <Link href={`/schedule/${scheduleDt?.id}`}>
                            {scheduleDt?.availableSeat}
                          </Link>
                        </td>
                        <td className="px-4 py-2 text-center">
                          <Link href={`/schedule/${scheduleDt?.id}`}>
                            {" "}
                            {scheduleDt?.booking?.length}
                          </Link>
                        </td>
                        <td className="px-4 py-2 text-center">
                          <Link href={`/schedule/${scheduleDt?.id}`}>
                            {scheduleDt?.Waitting?.length}
                          </Link>
                        </td>
                        <td className="flex flex-row justify-end py-2 ">
                          <button className="text-white rounded">
                            <ScheduleModel
                              isCreate={false}
                              isImport={false}
                              isEdit={true}
                              schedule_id={scheduleDt?.id}
                              isIcon={true}
                              isDisabled={scheduleDt?.enable ? false : true}
                              isTitle={false}
                              bus={scheduleDt?.bus}
                              departure={scheduleDt?.departure}
                              departure_date={scheduleDt?.date}
                              className={
                                scheduleDt?.enable ? `hover:bg-gray-200` : ""
                              }
                            />
                          </button>
                          <DeleteScheduleModal
                            item={`${scheduleDt?.departure?.from?.mainLocationName} - ${scheduleDt?.departure?.destination?.mainLocationName}`}
                            id={scheduleDt?.id}
                            enable={
                              scheduleDt?.booking?.length +
                                scheduleDt?.Waitting?.length +
                                scheduleDt?.Cancel?.length !==
                                0 || !scheduleDt?.enable
                                ? true
                                : false
                            }
                            className={``}
                          />
                        </td>
                        <td className="justify-center px-4 py-2 text-center">
                          <div>
                            <Image
                              src={
                                scheduleDt?.enable
                                  ? "unvalidated.svg"
                                  : "/validated.svg"
                              }
                              alt=""
                              width={20}
                              height={20}
                            />
                          </div>
                        </td>
                      </tr>
                    )
                  )}
                </tbody>
              </table>
            </div>
            {!data || data?.length == 0 ? null : (
              <div className="flex justify-end mt-4">
                <div className="flex items-center">
                  <span className="mr-2">Rows per page:</span>
                  <select
                    value={rowsPerPage}
                    onChange={(e) => {
                      if (Number(e.target.value) * currentPage > data.length) {
                        setCurrentPage(1);
                        setRowsPerPage(Number(e.target.value));
                      } else {
                        setRowsPerPage(Number(e.target.value));
                      }
                    }}
                    className="px-2 py-1 border border-gray-300 rounded-md"
                  >
                    <option value={5}>5</option>
                    <option value={10}>10</option>
                    <option value={15}>15</option>
                    <option value={25}>25</option>
                    <option value={50}>50</option>
                    <option value={100}>100</option>
                    <option value={200}>200</option>
                  </select>
                </div>
                <div className="flex items-center ml-4">
                  <span className="mr-2">Page:</span>
                  {
                    <button
                      key={"schedule backward"}
                      onClick={() => handlePrev()}
                      className={`border border-gray-300  rounded-md px-5 py-1 ml-1 focus:outline-none ${
                        currentPage == 1 ? "bg-gray-200" : ""
                      }`}
                    >
                      {"<"}
                    </button>
                  }
                  {Array.from(
                    { length: Math.ceil(data.length / rowsPerPage) },
                    (_, i) => i + 1
                  ).map((page) => (
                    <button
                      key={page}
                      onClick={() => handlePageChange(page)}
                      className={`border border-gray-300 rounded-md px-2 py-1 ml-1 focus:outline-none ${
                        currentPage === page ? "bg-gray-300" : ""
                      }`}
                    >
                      {page}
                    </button>
                  ))}
                  {
                    <button
                      key={"schedule forward"}
                      onClick={() => handleNext()}
                      className={`border border-gray-300 rounded-md px-5 py-1 ml-1 focus:outline-none ${
                        currentPage == Math.ceil(data.length / rowsPerPage)
                          ? "bg-gray-200"
                          : ""
                      }`}
                    >
                      {">"}
                    </button>
                  }
                </div>
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default withAuth(ScheduleManagement);
