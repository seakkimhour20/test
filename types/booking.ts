export interface BookingResponse {
  id: string;
  user: {
    username: string;
  };
}


export interface CancelResponse {
  id: string;
  user: {
    username: string
  }
}


export interface WaitingResponse {
  id: string;
  user: {
    username: string
  }
}

export interface SwapBookingDto {
  fromBookedId: string,
  withWaitingId: string
}