export interface BusResponse {
    status: string
    id: string,
    driverName: string,
    driverContact: string
    model: string,
    enable: boolean,
    numOfSeat: number,
    plateNumber: string
    createAt: Date,
    updatedAt: Date
  }