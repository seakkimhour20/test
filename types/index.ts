export interface LoginInput {
    email: string | null;
    password: string | null;
  }
  