export interface UserResponse {
  id: string;
  email: string;
  username: string;
  role: string;
  gender: string;
  inKRR: boolean;
  password: string;
  phone: string;
  enable: boolean;
  ticket: {
    id: string;
    remainTicket: number;
    ticketLimitInhand: number;
    updatedAt: string;
  };
  studentInfo: {
    id: string;
    batch: {
        id: string;
        department: string;
        batchNum: number;
    }
  };
  staffInfo: any; // Update the type if applicable
  adminInfo: any; // Update the type if applicable
  superAdminInfo: any; // Update the type if applicable
}

