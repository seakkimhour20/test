interface FromLocation {
    id: string;
    mainLocationName: string;
    createAt: Date;
    updatedAt: Date;
  }
  interface DestinationLocation {
    id: string;
    mainLocationName: string;
    createAt: Date;
    updatedAt: Date;
}
  
interface PickupLocation {
    id: string,
    mainLocationName: string,
    subLocationName: string,
    createAt: Date,
    updatedAt: Date
  }
  interface DropLocation {
    id: string,
    mainLocationName: string,
    subLocationName: string,
    createAt: Date,
    updatedAt: Date
  }

  
//   ------------------------------
  export interface DepartureResponse {
    id: string;
    departureTime: string | Date ,
    from: FromLocation
    destination: DestinationLocation,
    pickupLocation: PickupLocation,
    dropLocation: DropLocation,
}