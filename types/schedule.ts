export interface ScheduleRowTable {
  key: string;
  driverName: string;
  driverNumber: string;
  from: string;
  to: string;
  departureDate: string | Date;
  departureTime: string;
  seat: number;
  booked: number;
  waiting: number;
  operation: string;
}

interface FromLocation {
  id: string;
  mainLocationName: string;
  createAt: Date;
  updatedAt: Date;
}
interface DestinationLocation {
  id: string;
  mainLocationName: string;
  createAt: Date;
  updatedAt: Date;
}
interface PickupLocation {
  id: string,
  mainLocationName: string,
  subLocationName: string,
  createAt: Date,
  updatedAt: Date
}
interface DropLocation {
  id: string,
  mainLocationName: string,
  subLocationName: string,
  createAt: Date,
  updatedAt: Date
}

interface BusResponse {
  plateNumber: any;
  id: string;
  model: string;
  busNumber: string;
  numOfSeat: number;
  driverName: string;
  driverContact: string;
  enable: boolean;
  createAt: Date;
  updatedAt: Date;
}

interface BookingResponse {
  id: string;
  user: {
    username: string
  }
}

interface WaittingResponse {
  id: string;
  user: {
    username: string
  }
}

interface CancelResponse {
  id: string;
  user: {
    username: string
  }
}

export interface ScheduleResponseData {
  id: string | string,
  departureId: string,
  date: Date | string,
  busId: string,
  availableSeat: number,
  enable: boolean,
  departure: {
    id: string,
    departureTime: string,
    from: FromLocation,
    destination: DestinationLocation,
    pickupLocation: PickupLocation
    dropLocation: DropLocation
  },
  booking: BookingResponse[],
  Waitting: WaittingResponse[],
  Cancel: CancelResponse[],
  bus: BusResponse
}


export interface ScheduleDropDownDto {
  id: string;
    departure: string;
    departureTime: string | Date
}

export interface CreateScheduleDto{
  departureId: string;
  busId: string | null
  date: string | Date
}

export interface UpdateScheduleDto{
  departureId: string;
  busId: string | null
  date: string | Date
}