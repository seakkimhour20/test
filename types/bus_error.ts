export interface BusError {
    phoneNumber: {
        isError: boolean;
        message: string;
    }
    plateNumber: {
        isError: boolean;
        message: string;
    }

}