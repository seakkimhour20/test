export interface UserError {
    username: {
        isError: boolean;
        message: string;
    }
    email: {
        isError: boolean;
        message: string;
    }
    password: {
        isError: boolean;
        message: string;
    }
    confirmPassword: {
        isError: boolean;
        message: string;
    }
}