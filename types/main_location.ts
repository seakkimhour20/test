import { SubLocationModel } from "./sub_location";
export interface MainLocation {
    id: string;
    createdAt: Date;
    updatedAt: Date | null;
    mainLocationName: string;
    SubLocation: SubLocationModel[];
}