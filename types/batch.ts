import { EnumType } from "typescript";

export interface BatchResponse {
    id: string;
    department: EnumType | string;
    batchNum: number;
    createdAt: Date;
    updatedAt: Date
}