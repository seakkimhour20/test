import { useRouter, usePathname } from 'next/navigation';
const withAuth = (WrappedComponent) => {
  // eslint-disable-next-line react/display-name
  return (props) => {
    if (typeof window !== 'undefined') {
      const Router = useRouter();
      const accessToken = localStorage.getItem('token');
      const currentPath = usePathname();

      if (currentPath == '/') {
        if (accessToken) {
          Router.push('/schedule');
          return null;
        } else {
          Router.push('/');
        }
      } else {
        if (!accessToken) {
          Router.push('/');
          return null;
        }
      }

      return <WrappedComponent {...props} />;
    }
    return null;
  };
};
export default withAuth;
