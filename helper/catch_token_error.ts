export function catch_token_error(error: string): void {
    if (error == "Invalid token or expired" || error == "Invalid token" || error == "No token provided") {
        localStorage.removeItem("token");
        window.location.href = "/"
    }
}