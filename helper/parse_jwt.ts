export function parse_jwt_token(token: string | null): string | null {
    if (!token) {
      return null;
    }
  
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('_', '/');
    const decodedToken = JSON.parse(window.atob(base64));
    const role = decodedToken.role;
    return role;
  }
  